package com.acidwater.saher.main;

import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.acidwater.saher.Places.details.view.PlaceDetailsFragment;
import com.acidwater.saher.Places.moreplaces.view.MorePlacesFragment;
import com.acidwater.saher.changePassword.view.ChangePasswordFragment;
import com.acidwater.saher.data.PushData;
import com.acidwater.saher.profile.view.ProfileFragment;
import com.acidwater.saher.profile.model.ProfileInteractor;
import com.acidwater.saher.profile.model.User;
import com.acidwater.saher.R;
import com.acidwater.saher.calendar.view.CalendarFragment;
import com.acidwater.saher.data.DataEntityWrapper;
import com.acidwater.saher.events.details.view.EventsDetailsFragment;
import com.acidwater.saher.exceptions.SaherException;
import com.acidwater.saher.favorites.view.FavoritesFragment;
import com.acidwater.saher.home.view.HomeFragment;
import com.acidwater.saher.musicians.details.view.MusicianDetailsFragment;
import com.acidwater.saher.musicians.moremusicians.view.MoreArtistsFragment;
import com.acidwater.saher.musicians.moremusicians.view.MoreBandsFragment;
import com.acidwater.saher.search.view.SearchFragment;
import com.acidwater.saher.services.ServiceObserver;
import com.acidwater.saher.settings.view.SettingsFragment;
import com.acidwater.saher.utils.BasePagerAdapter;
import com.acidwater.saher.utils.ConstantStrings;
import com.acidwater.saher.utils.DateUtils;
import com.acidwater.saher.utils.GeneralUtils;
import com.acidwater.saher.utils.NonSwipeableViewPager;
import com.acidwater.saher.utils.RxUtils;
import com.acidwater.saher.utils.bus.MainBus;
import com.acidwater.saher.utils.bus.events.PushOpened;
import com.onesignal.OneSignal;

import io.branch.referral.Branch;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;


public class MainActivity extends AppCompatActivity implements MainActivityListener {


    private Toolbar toolbar;
    private TabLayout bottomTabLayout;
    private NonSwipeableViewPager pager;
    boolean doubleBackToExitPressedOnce = false;
    private Disposable disposable;
    private CompositeDisposable compositeDisposable;
    private TextView toolbarTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbarTitle = (TextView) findViewById(R.id.toolbar_title);
        bottomTabLayout = (TabLayout) findViewById(R.id.bottomTabLayout);
        pager = (NonSwipeableViewPager) findViewById(R.id.pager);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        compositeDisposable = new CompositeDisposable();
        registerToBus();

        String date = "2017-07-21T10:05:00+0000";
        int day_of_month = DateUtils.getDay(date);
        int month_int = DateUtils.getMonth(date);
        int year = DateUtils.getYear(date);
        String month = DateUtils.getMonthName(date);

        BasePagerAdapter adapter = new BasePagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new HomeFragment(), null);
        adapter.addFragment(new CalendarFragment(), null);
        adapter.addFragment(new FavoritesFragment(), null);
        adapter.addFragment(new SettingsFragment(), null);

        pager.setAdapter(adapter);
        bottomTabLayout.setupWithViewPager(pager);

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                toolbarTitle.setVisibility(View.VISIBLE);
                if (getSupportFragmentManager().getBackStackEntryCount() > 0) {

                    onBackPressed();
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        setupTabIcons();


        new Handler().post(new Runnable() {
            @Override
            public void run() {
                getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
                    @Override
                    public void onBackStackChanged() {
                        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                            // show back button


                            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    onBackPressed();
                                }
                            });
                        } else {
                            //show hamburger
                            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
//                            toolbar.setTitle(getString(R.string.app_name));

                        }
                    }
                });

            }
        });

        if (GeneralUtils.getSharedPref(MainActivity.this, ConstantStrings.USER_SESSION).equals(ConstantStrings.user_is_logged_in)) {
            getProfile();
        }


        parsePush();
    }

    private void parsePush() {
        if (GeneralUtils.getPush(this) != null) {

            PushData pushData = GeneralUtils.getPush(this);

            switch (pushData.getType()) {
                case ConstantStrings.FAVORITE_EVENT:
                    navigateToEventDetails(pushData.getId());
                    break;

                case ConstantStrings.FAVORITE_PUB:
                    navigateToPlaceDetails(pushData.getId());
                    break;

                case ConstantStrings.FAVORITE_MUSICIAN:
                    navigateToMusicianDetails(pushData.getId());
                    break;
            }
            GeneralUtils.savePush(this, null);

        }
    }

    private void setupTabIcons() {
        bottomTabLayout.getTabAt(0).setIcon(R.drawable.icon_home);
//        bottomTabLayout.getTabAt(0).setText(getString(R.string.home));
        bottomTabLayout.getTabAt(1).setIcon(R.drawable.icon_calendar);
//        bottomTabLayout.getTabAt(1).setText(getString(R.string.calendar));
        bottomTabLayout.getTabAt(2).setIcon(R.drawable.icon_favorite);
//        bottomTabLayout.getTabAt(2).setText(getString(R.string.favorites));
        bottomTabLayout.getTabAt(3).setIcon(R.drawable.icon_settings);
//        bottomTabLayout.getTabAt(3).setText(getString(R.string.settings));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);

        final MenuItem mSearch = menu.findItem(R.id.action_search);
        final SearchView mSearchView = (SearchView) mSearch.getActionView();
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // open searchFragment
                mSearch.collapseActionView();
                navigateToSearch(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        mSearch.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem menuItem) {
                toolbarTitle.setVisibility(View.GONE);
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem menuItem) {
                toolbarTitle.setVisibility(View.VISIBLE);
                return true;
            }
        });

        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            super.onBackPressed();
        } else {

            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, getString(R.string.press_back_to_exit), Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);

        }
    }

    @Override
    public void navigateToMoreBands(String categoryName, int categoryID) {
        MoreBandsFragment moreBandsFragment = MoreBandsFragment.newInstance(categoryName, categoryID);
        navigateToFragment(moreBandsFragment);
//        toolbar.setTitle(getString(R.string.Bands));
    }

    @Override
    public void navigateToMoreArtists(String categoryName, int categoryID) {
        MoreArtistsFragment moreArtistsFragment = MoreArtistsFragment.newInstance(categoryName, categoryID);
        navigateToFragment(moreArtistsFragment);
//        toolbar.setTitle(getString(R.string.Artists));
    }

    @Override
    public void navigateToEventDetails(int eventID) {
        EventsDetailsFragment eventsDetailsFragment = EventsDetailsFragment.newInstance(eventID);
        navigateToFragment(eventsDetailsFragment);
//        toolbar.setTitle(getString(R.string.Event));
    }

    @Override
    public void navigateToPlaceDetails(int placeID) {
        PlaceDetailsFragment placeDetailsFragment = PlaceDetailsFragment.newInstance(placeID);
        navigateToFragment(placeDetailsFragment);
//        toolbar.setTitle(getString(R.string.Place));
    }

    @Override
    public void navigateToMusicianDetails(int musicianID) {
        MusicianDetailsFragment musicianDetailsFragment = MusicianDetailsFragment.newInstance(musicianID);
        navigateToFragment(musicianDetailsFragment);
    }

    @Override
    public void navigateToMorePlaces(String categoryName, int categoryID) {
        MorePlacesFragment morePlacesFragment = MorePlacesFragment.newInstance(categoryName, categoryID);
        navigateToFragment(morePlacesFragment);
//        toolbar.setTitle(getString(R.string.Places));
    }

    @Override
    public void navigateToProfile() {
        ProfileFragment profileFragment = ProfileFragment.newInstance();
        navigateToFragment(profileFragment);
    }

    @Override
    public void navigateToSearch(String query) {
        destroySearchFragment();
        SearchFragment searchFragment = SearchFragment.newInstance(query);
        navigateToFragment(searchFragment);
    }

    @Override
    public void navigateToChangePassword() {
        ChangePasswordFragment changePasswordFragment = ChangePasswordFragment.newInstance();
        navigateToFragment(changePasswordFragment);
    }

    private void navigateToFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction =
                fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.container, fragment, fragment.getClass().getSimpleName()).addToBackStack(fragment.getClass().getSimpleName());
        fragmentTransaction.commit();
    }

    private void destroySearchFragment() {

        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment f = fragmentManager.findFragmentByTag(SearchFragment.class.getSimpleName());
        if (f != null) {
            onBackPressed();
        }
    }

    private void getProfile() {
        ProfileInteractor profileInteractor = new ProfileInteractor(this);


        disposable = profileInteractor.getProfile().subscribeWith(new ServiceObserver<DataEntityWrapper<User>>() {
            @Override
            public void onStart() {

            }

            @Override
            public void onComplete() {

            }

            @Override
            public void onNext(DataEntityWrapper<User> userDataEntityWrapper) {
                if (userDataEntityWrapper != null && userDataEntityWrapper.getData() != null) {
                    GeneralUtils.saveUserProfile(MainActivity.this, userDataEntityWrapper.getData());
                    OneSignal.sendTag(ConstantStrings.USER_ID, String.valueOf(userDataEntityWrapper.getData().getId()));
                }
            }

            @Override
            public void onError(SaherException e) {
                Toast.makeText(getBaseContext(), "error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        RxUtils.disposeIfNotNull(disposable);
        RxUtils.disposeIfNotNull(compositeDisposable);
    }

    private void registerToBus() {
        compositeDisposable.add(MainBus.getInstance().getBusObservable().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<Object>() {
            @Override
            public void accept(Object object) throws Exception {

                if (object instanceof PushOpened) {
                    parsePush();
                }
            }
        }));
    }
}
