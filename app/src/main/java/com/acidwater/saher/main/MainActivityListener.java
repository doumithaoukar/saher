package com.acidwater.saher.main;

/**
 * Created by Doumith on 7/23/2017.
 */
public interface MainActivityListener {


    void navigateToMoreBands(String categoryName, int categoryID);

    void navigateToMoreArtists(String categoryName, int categoryID);

    void navigateToEventDetails(int eventID);

    void navigateToPlaceDetails(int placeID);

    void navigateToMusicianDetails(int musicianID);

    void navigateToMorePlaces(String categoryName, int categoryID);

    void navigateToProfile();

    void navigateToSearch(String query);

    void navigateToChangePassword();

}
