package com.acidwater.saher.utils;

import android.content.Context;
import android.content.Intent;

import com.acidwater.saher.data.PushData;
import com.acidwater.saher.main.MainActivity;
import com.acidwater.saher.utils.bus.MainBus;
import com.acidwater.saher.utils.bus.events.PushOpened;
import com.google.gson.Gson;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;

/**
 * Created by Doumith on 3/4/2018.
 */
public class SaherNotificationOpenedHandler implements OneSignal.NotificationOpenedHandler {

    Context context;

    public SaherNotificationOpenedHandler(Context context) {
        this.context = context;
    }

    @Override
    public void notificationOpened(OSNotificationOpenResult result) {

        String jsonString = result.notification.payload.additionalData.toString();
        Gson gson = new Gson();
        PushData pushData = gson.fromJson(jsonString, PushData.class);
        GeneralUtils.savePush(context, pushData);

        Intent it = new Intent(context, MainActivity.class);
        context.startActivity(it);
        MainBus.getInstance().send(new PushOpened());

    }
}
