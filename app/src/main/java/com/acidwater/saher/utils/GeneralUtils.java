package com.acidwater.saher.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.net.Uri;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Display;
import android.view.inputmethod.InputMethodManager;

import com.acidwater.saher.IntroActivity;
import com.acidwater.saher.R;
import com.acidwater.saher.data.PushData;
import com.acidwater.saher.exceptions.SaherException;
import com.acidwater.saher.profile.model.User;
import com.acidwater.saher.utils.bus.MainBus;
import com.acidwater.saher.utils.bus.events.UpdateProfile;
import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.MediaType;
import okhttp3.RequestBody;

/**
 * Created by Doumith on 8/12/2017.
 */
public class GeneralUtils {

    public static final String SAHER_SHARED_PREF = "saher_shared_pref";

    public static int getDpValue(int dp) {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, displaymetrics);
    }

    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static int pxToDp(int px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }


    public static void setSharedPref(Context context, String key, String value) {
        SharedPreferences mPrefs = context.getSharedPreferences(SAHER_SHARED_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        prefsEditor.putString(key, value);
        prefsEditor.commit();

    }

    public static String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd'T'HH:mm:ss";
        String outputPattern = "dd/MM/yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static void logout(Context context) {
        clearUserProfile(context);
        saveCookie(context, new HashSet<String>());
        setSharedPref(context, ConstantStrings.user_is_logged_in, "");
        setSharedPref(context, ConstantStrings.USER_SESSION, "");
        Intent it = new Intent(context, IntroActivity.class);
        it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(it);
    }


    public static void openIntent(Context context, String url) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        context.startActivity(i);
    }

    public static String getSharedPref(Context context, String key) {
        SharedPreferences mPrefs = context.getSharedPreferences(SAHER_SHARED_PREF, Context.MODE_PRIVATE);
        String pref = mPrefs.getString(key, "");
        return pref;
    }

    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    public static int getScreenWidth(Activity context) {
        Display display = context.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.x;
    }

    public static final String md5(final String s) {
        final String MD5 = "MD5";
        try {
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static void saveUserProfile(Context context, User user) {
        SharedPreferences mPrefs = context.getSharedPreferences(SAHER_SHARED_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(user);
        prefsEditor.putString("user", json);
        prefsEditor.commit();
        MainBus.getInstance().send(new UpdateProfile());

    }

    public static void clearUserProfile(Context context) {
        SharedPreferences mPrefs = context.getSharedPreferences(SAHER_SHARED_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        Gson gson = new Gson();
        prefsEditor.putString("user", "");
        prefsEditor.commit();
        MainBus.getInstance().send(new UpdateProfile());
    }

    public static void saveCookie(Context context, HashSet<String> cookies) {

        SharedPreferences mPrefs = context.getSharedPreferences(SAHER_SHARED_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        prefsEditor.putStringSet("COOKIE", cookies);
        prefsEditor.commit();

    }

    public static User getUserProfile(Context context) {
        SharedPreferences mPrefs = context.getSharedPreferences(SAHER_SHARED_PREF, Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = mPrefs.getString("user", "");
        User obj = gson.fromJson(json, User.class);
        return obj;
    }

    public static HashSet<String> getCookies(Context context) {
        SharedPreferences mPrefs = context.getSharedPreferences(SAHER_SHARED_PREF, Context.MODE_PRIVATE);
        return (HashSet<String>) mPrefs.getStringSet("COOKIE", new HashSet<String>());

    }

    public static void savePush(Context context, PushData push) {
        SharedPreferences mPrefs = context.getSharedPreferences(SAHER_SHARED_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        String json = "";
        if (push != null) {
            Gson gson = new Gson();
            json = gson.toJson(push);
        } else {
        }
        prefsEditor.putString("push", json);
        prefsEditor.commit();
    }

    public static PushData getPush(Context context) {
        SharedPreferences mPrefs = context.getSharedPreferences(SAHER_SHARED_PREF, Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = mPrefs.getString("push", "");
        PushData obj = gson.fromJson(json, PushData.class);
        return obj;
    }


    public static String resolveError(SaherException error, Context context) {
        int errorMessage = R.string.SERVICE_ERROR;
        String errorMessageStr = null;
        switch (error.reason) {
            case CONNECTION_ERROR:
                errorMessage = R.string.IO_EXCEPTION;
                break;
            case SERVICE_ERROR:
                errorMessage = R.string.SERVICE_ERROR;
                break;
            case NOT_FOUND:

            case BAD_REQUEST:
                errorMessage = R.string.BAD_REQUEST;
                break;
            case NOT_LATEST_VERSION:

            default:
                errorMessage = R.string.SERVICE_ERROR;
                break;

        }

        if (!TextUtils.isEmpty(errorMessageStr)) {
            return errorMessageStr;
        } else {
            return context.getResources().getString(errorMessage);
        }
    }


    public static void closeKeyboard(Context context) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
    }


    public static File convertBitmapToFile(Context context, Bitmap bitmap, String filename) {
        File f = new File(context.getCacheDir(), filename);
        try {
            f.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 0 /*ignored for PNG*/, bos);
        byte[] bitmapdata = bos.toByteArray();

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(f);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return f;
    }

    public static RequestBody toRequestBody(String value) {
        RequestBody body = RequestBody.create(MediaType.parse("text/plain"), value);
        return body;
    }

    public static void callNumber(Context context, String number) {
        if (!TextUtils.isEmpty(number)) {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:" + number));
            context.startActivity(intent);
        }
    }

    public static String getShareURl(Context context) {
        return "https://play.google.com/store/apps/details?id=" + context.getPackageName();
    }

}
