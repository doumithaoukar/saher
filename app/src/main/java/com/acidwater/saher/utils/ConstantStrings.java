package com.acidwater.saher.utils;

/**
 * Created by Doumith on 6/18/2017.
 */
public class ConstantStrings {


    public static final String BASE_URL = "https://saher.info/app_dev.php/api/v1/";
    public static final String FAQ_URL = "https://www.saher.info/faq";
    public static final String ABOUT_URL = "https://www.saher.info/about";
    public static final String TERMS_URL = "https://www.saher.info/terms";
    public static final String CONTACT_URL = "https://www.saher.info/contact";
    public static final String WEBSITE_URL = "https://www.saher.info";


    public static final String USER_ID = "user_id";


    public static final String SLIDER_HOME = "slider/home";
    public static final String SERVICE_EVENTS = "events";
    public static final String SERVICE_PLACES = "pubsByCategory";
    public static final String SERVICE_SEARCH = "search";

    public static final String SERVICE_MUSICIANS_CATEGORY = "musiciansByCategory";
    public static final String SERVICE_MUSICIANS = "musicians";
    public static final String SERVICE_PLACES_CATEGORY = "pubs";


    public static final String SERVICE_EVENT_DETAILS = "event";
    public static final String SERVICE_PLACE_DETAILS = "pub";
    public static final String SERVICE_MUSICIAN_DETAILS = "musician";


    public static final String SERVICE_CALENDAR = "calendar";


    public static final String SERVICE_REGISTER_PROFILE = "user/register";
    public static final String SERVICE_FORGOT_PASSWORD = "user/resetPassword";
    public static final String SERVICE_LOGIN = "user/login";
    public static final String SERVICE_FACEBOOK_LOGIN = "user/facebookLogin";
    public static final String SERVICE_GOOGLE_LOGIN = "user/googleLogin";
    public static final String SERVICE_USER_PROFILE = "user/profile";
    public static final String SERVICE_ADD_FAVORITE = "user/favorite";
    public static final String SERVICE_REMOVE_FAVORITE = "user/unfavorite";
    public static final String SERVICE_PROFILE_UPDATE = "user/update";


    public static final String ONE_SIGNAL_APP_ID = "54f7a29f-9c13-4e26-8815-2b670e8555a9";
    public static final int FACEBOOK_REQUEST_CODE = 102;

    public static final int PASSWORD_LENGTH = 8;


    public static final String TYPE_ARTIST = "artist";
    public static final String TYPE_BAND = "band";

    //ENTITY TYPE FAVORITE
    public static final String FAVORITE_MUSICIAN = "artist";
    public static final String FAVORITE_PUB = "pub";
    public static final String FAVORITE_EVENT = "event";


    //paging default values
    public static final int EVENTS_PAGING = 25;
    public static final int MUSICIANS_PAGING = 25;
    public static final int PLACES_PAGING = 25;
    public static final int SEARCH_PAGING = 25;


    public static final String USER_SESSION = "user_session";
    public static final String user_is_logged_in = "is_logged_in";
    public static final String user_need_verification = "user_need_verification";

    public static final String SETTINGS_PROFILE = "settings_profile";
    public static final String SETTINGS_CHANGE_PASSWORD = "settings_change_password";

    public static final String SETTINGS_NOTIFICATIONS = "settings_notifications";
    public static final String SETTINGS_LOGOUT = "settings_logout";
    public static final String SETTINGS_ABOUT = "settings_about";
    public static final String SETTINGS_FAQ = "settings_faq";
    public static final String SETTINGS_TERMS = "settings_terms";
    public static final String SETTINGS_CONTACT_US = "settings_contact_us";
    public static final String SETTINGS_WEBSITE = "settings_website";
    public static final String SETTINGS_SHARE_APP = "settings_share_app";

    public static final String LOGGED_ACCOUNT_TYPE = "Logged_account_type";
    public static final String FACEBOOK_ACCOUNT = "FACEBOOK_ACCOUNT";
    public static final String GOOGLE_PLUS_ACCOUNT = "GOOGLE_PLUS_ACCOUNT";
    public static final String EMAIL_ACCOUNT = "EMAIL_ACCOUNT";

}
