package com.acidwater.saher.utils.mvp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.acidwater.saher.ApplicationContext;
import com.acidwater.saher.R;
import com.acidwater.saher.exceptions.SaherException;
import com.acidwater.saher.utils.GeneralUtils;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceFragment;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

/**
 * Created by chadia on 4/19/2017.
 */

public abstract class LceRetryFragment<CV extends View, M, V extends MvpLceView<M>, P extends MvpPresenter<V>> extends MvpLceFragment<CV, M, V, P> {

    protected ApplicationContext application;
    private Button errorButton;
    private View loadingView;
    private TextView errorTextView;

    public LceRetryFragment() {
        super();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        errorButton = (Button) view.findViewById(R.id.errorButton);
        errorTextView = (TextView) view.findViewById(R.id.errorView);

        //Progress bar customization
        loadingView = view.findViewById(R.id.loadingView);
        customizeLoadingView(loadingView);
    }

    protected abstract void customizeLoadingView(View loadingView);

    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return "error";
    }

    @Override
    public void setData(M data) {

    }

    @Override
    public void loadData(boolean pullToRefresh) {

    }

    @Override
    public P createPresenter() {
        return null;
    }

    public abstract void retry();

    @Override
    public void showError(Throwable e, boolean pullToRefresh) {
        super.showError(e, pullToRefresh);
        SaherException exception = (SaherException) (e);
        errorTextView.setVisibility(View.VISIBLE);
        errorButton.setVisibility(View.VISIBLE);

        errorTextView.setText(GeneralUtils.resolveError(exception, getContext()));
        errorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                retry();
            }
        });
    }


    @Override
    public void showLoading(boolean pullToRefresh) {
        super.showLoading(pullToRefresh);
        errorButton.setVisibility(View.GONE);
    }

    @Override
    public void showContent() {
        super.showContent();
        errorButton.setVisibility(View.GONE);
    }
}
