package com.acidwater.saher.utils;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.acidwater.saher.home.view.HomeFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Doumith on 6/9/2017.
 */
public class BasePagerAdapter extends FragmentStatePagerAdapter {

    private final List<Fragment> mFragmentList = new ArrayList();
    private final List<String> mFragmentTitleNames = new ArrayList();

    public BasePagerAdapter(FragmentManager fm) {
        super(fm);
    }

    public void addFragment(Fragment fragment, String title) {
        mFragmentList.add(fragment);
        mFragmentTitleNames.add(title);
    }

    public void addFragmentAtPosition(Fragment fragment, String title, int position) {
        mFragmentList.add(position, fragment);
        mFragmentTitleNames.add(position, title);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitleNames.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }
}