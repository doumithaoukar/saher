package com.acidwater.saher.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Doumith on 7/23/2017.
 */
public class DateUtils {

    private static final String SERVER_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
    private static final String LOCAL_FORMAT = "dd-mm-yyyy";

    public static int getDay(String dateString) {
        DateFormat df = new SimpleDateFormat(SERVER_DATE_FORMAT);
        Calendar cal = Calendar.getInstance();
        try {
            cal.setTime(df.parse(dateString));
            return cal.get(Calendar.DAY_OF_MONTH);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static int getMonth(String dateString) {
        DateFormat df = new SimpleDateFormat(SERVER_DATE_FORMAT);
        Calendar cal = Calendar.getInstance();
        try {
            cal.setTime(df.parse(dateString));
            return cal.get(Calendar.MONTH) + 1;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static int getYear(String dateString) {
        DateFormat df = new SimpleDateFormat(SERVER_DATE_FORMAT);
        Calendar cal = Calendar.getInstance();
        try {
            cal.setTime(df.parse(dateString));
            return cal.get(Calendar.YEAR);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static String getMonthName(String dateString) {
        DateFormat df = new SimpleDateFormat(SERVER_DATE_FORMAT);
        Calendar cal = Calendar.getInstance();
        try {
            cal.setTime(df.parse(dateString));
            return cal.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getTime(String dateString) {
        DateFormat df = new SimpleDateFormat(SERVER_DATE_FORMAT);
        Calendar cal = Calendar.getInstance();
        try {
            cal.setTime(df.parse(dateString));
            return String.format("%02d", cal.get(Calendar.HOUR_OF_DAY)) + ":" + String.format("%02d", cal.get(Calendar.MINUTE));

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getAmPM(String dateString) {
        DateFormat df = new SimpleDateFormat(SERVER_DATE_FORMAT);
        Calendar cal = Calendar.getInstance();

        if (cal.get(Calendar.AM_PM) == Calendar.AM) {
            return "AM";
        }
        return "PM";

    }
}
