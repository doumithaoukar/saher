package com.acidwater.saher.intro.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.acidwater.saher.R;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.squareup.picasso.Picasso;


public class TutorialFollow extends BaseSliderView {
    private Context context;

    public TutorialFollow(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    public View getView() {
        View v = LayoutInflater.from(getContext()).inflate(R.layout.tutorial_item, null);
        ImageView target = (ImageView) v.findViewById(R.id.daimajia_slider_image);
        TextView title = (TextView) v.findViewById(R.id.slide_title);
        Picasso.with(context).load(R.drawable.tutorial_follow).into(target);
        title.setText(context.getString(R.string.tutorial_follow));
        bindEventAndShow(v, target);
        setScaleType(ScaleType.CenterInside);
        return v;
    }
}
