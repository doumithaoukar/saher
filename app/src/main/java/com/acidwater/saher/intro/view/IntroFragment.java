package com.acidwater.saher.intro.view;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.acidwater.saher.BaseFragment;
import com.acidwater.saher.IntroActivity;
import com.acidwater.saher.IntroActivityListener;
import com.acidwater.saher.R;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.Transformers.BaseTransformer;


/**
 * Created by Doumith on 7/6/2017.
 */
public class IntroFragment extends BaseFragment implements View.OnClickListener {


    private Button btnSkip;
    private Button btnLogin;
    private IntroActivityListener mListener;


    private final long SLIDER_DURATION = 5000;
    private SliderLayout slider;
    private Handler sliderHandler;
    private Runnable slideRunnable;
    private boolean isSliderStopped;

    public static IntroFragment newInstance() {
        IntroFragment introFragment = new IntroFragment();
        return introFragment;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof IntroActivity) {
            mListener = (IntroActivity) activity;
        } else {
            throw new RuntimeException("you should implemented MainActivityListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(
                R.layout.fragment_intro, container, false);

        btnLogin = (Button) mView.findViewById(R.id.btnLogin);
        btnSkip = (Button) mView.findViewById(R.id.btnSkip);
        slider = (SliderLayout) mView.findViewById(R.id.slider);

        btnLogin.setOnClickListener(this);
        btnSkip.setOnClickListener(this);

        buildTutorialSlideShow();
        return mView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLogin:
                mListener.navigateToSection(IntroActivity.LOGIN);
                break;
            case R.id.btnSkip:
                mListener.navigateToSection(IntroActivity.MAIN_ACTIVITY);
                break;
        }
    }

    private void destroyThis() {
        getFragmentManager().beginTransaction().remove(this).commit();
        getFragmentManager().popBackStack();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (sliderHandler != null && slideRunnable != null) {
            sliderHandler.removeCallbacks(slideRunnable);
            slider.stopAutoCycle();
        }
    }

    @Override
    public void onStop() {
        super.onStop();

        if (slider != null) {
            isSliderStopped = true;
            slider.stopAutoCycle();
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        if (isSliderStopped == true) {
            slider.startAutoCycle();
        }
    }

    private void buildTutorialSlideShow() {
        slider.removeAllSliders();

        slider.addSlider(new TutorialEvents(getContext()));
        slider.addSlider(new TutorialCalendar(getContext()));
        slider.addSlider(new TutorialFollow(getContext()));
        slider.addSlider(new TutorialProfile(getContext()));

        slider.setPagerTransformer(false, new BaseTransformer() {
            @Override
            protected void onTransform(View view, float position) {

                View description_layout = view.findViewById(R.id.description_layout);
                View content_layout = view.findViewById(R.id.content_layout);

                if (position <= -1.0F || position >= 1.0F) {
                    content_layout.setTranslationX(view.getWidth() * position);
                    content_layout.setAlpha(0.0F);
                    description_layout.setAlpha(0);
                } else if (position == 0.0F) {
                    content_layout.setTranslationX(view.getWidth() * -position);
                    content_layout.setAlpha(1.0F - Math.abs(position));
                    description_layout.setAlpha(1.0F);
                } else {
                    // position is between -1.0F & 0.0F OR 0.0F & 1.0F
                    content_layout.setTranslationX(view.getWidth() * position);
                    content_layout.setAlpha(1.0F);
                    description_layout.setAlpha(1.0F - Math.abs(position));
                }
            }
        });
        slider.setDuration(SLIDER_DURATION);

        // there is a bug in the library :Once initiated, the slider immediately slides to the 2nd slide
        // we are using handler to fix this issue
        slider.stopAutoCycle();
        sliderHandler = new Handler();
        slideRunnable = new Runnable() {
            @Override
            public void run() {
                slider.startAutoCycle();
            }
        };
        sliderHandler.postDelayed(slideRunnable, SLIDER_DURATION);

    }

}
