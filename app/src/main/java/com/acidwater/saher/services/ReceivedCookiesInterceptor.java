package com.acidwater.saher.services;

import android.content.Context;
import android.util.Log;

import com.acidwater.saher.utils.ConstantStrings;
import com.acidwater.saher.utils.GeneralUtils;

import java.io.IOException;
import java.util.HashSet;

import okhttp3.Interceptor;
import okhttp3.Response;

/**
 * Created by Doumith on 10/21/2017.
 */
public class ReceivedCookiesInterceptor implements Interceptor {
    private Context context;

    public ReceivedCookiesInterceptor(Context context) {
        this.context = context;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Response originalResponse = chain.proceed(chain.request());

        if (!originalResponse.headers("Set-Cookie").isEmpty()) {
            Log.i("cookie","Recevie \n"+originalResponse.headers("Set-Cookie").toString());

            HashSet<String> cookies = new HashSet<>();

            for (String header : originalResponse.headers("Set-Cookie")) {
                cookies.add(header);
            }
            GeneralUtils.saveCookie(context, cookies);
        }

        return originalResponse;
    }
}