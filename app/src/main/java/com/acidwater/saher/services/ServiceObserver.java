package com.acidwater.saher.services;


import com.acidwater.saher.data.ServerResponse;
import com.acidwater.saher.data.gsondeserializer.GSONDeserializer;
import com.acidwater.saher.data.gsondeserializer.GSONDeserializerImpl;
import com.acidwater.saher.exceptions.SaherException;
import com.acidwater.saher.utils.GeneralUtils;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;


import java.io.IOException;

import io.reactivex.observers.DisposableObserver;
import retrofit2.HttpException;

/**
 * Created by KamiX on 4/27/16.
 */
public abstract class ServiceObserver<T> extends DisposableObserver<T> {

    @Override
    public abstract void onStart();

    @Override
    public abstract void onComplete();

    @Override
    public void onError(Throwable e) {
        onError(createApplicationException(e));
    }

    @Override
    public abstract void onNext(T t);

    public abstract void onError(SaherException e);

    private SaherException createApplicationException(Throwable e) {
        SaherException exception = null;

        if (e instanceof HttpException) {
            HttpException httpException = (HttpException) e;

            switch (httpException.code()) {
                case 400:
                case 401:
                case 402:
                case 403:
                case 404:
                case 405:
                case 417:
                    GSONDeserializer<ServerResponse> deserializer = new GSONDeserializerImpl<ServerResponse>();
                    Gson gson = new Gson();
                    ServerResponse errorResponse = null;
                    try {
                        errorResponse = deserializer.deserialize(httpException.response().errorBody().string(), ServerResponse.class, gson);
                        String errorMessage = errorResponse.getStatusDescription() == null ? "" : errorResponse.getStatusDescription();
                        exception = new SaherException(SaherException.Reason.BAD_REQUEST, errorMessage);


                    } catch (IOException e1) {
                        e1.printStackTrace();
                        exception = new SaherException(SaherException.Reason.NOT_FOUND, "Server Error");
                    } catch (JsonSyntaxException js) {
                        exception = new SaherException(SaherException.Reason.SERVICE_ERROR, "Server Error");
                    }
                    break;

                default:
                    exception = new SaherException(SaherException.Reason.SERVICE_ERROR, "Server Error");
                    break;
            }
        } else {
            exception = new SaherException(SaherException.Reason.CONNECTION_ERROR, "Server Error");
        }
        return exception;
    }
}