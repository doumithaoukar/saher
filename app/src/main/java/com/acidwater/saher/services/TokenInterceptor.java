package com.acidwater.saher.services;

import android.content.Context;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;


public class TokenInterceptor implements Interceptor {

    public TokenInterceptor(Context context) {

    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request initialRequest = chain.request();
        Request modifiedRequest = initialRequest;
        //Add headers
        Request.Builder headerBuilder = initialRequest.newBuilder();
        headerBuilder.build();

        Response response = chain.proceed(modifiedRequest);
        boolean unauthorized = response.code() == 401;
        if (unauthorized) {


        }
        return response;
    }

}
