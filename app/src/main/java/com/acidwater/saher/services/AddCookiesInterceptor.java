package com.acidwater.saher.services;

import android.content.Context;
import android.util.Log;

import com.acidwater.saher.utils.GeneralUtils;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Doumith on 10/21/2017.
 */
public class AddCookiesInterceptor implements Interceptor {

    private Context context;

    public AddCookiesInterceptor(Context context) {
        this.context = context;
    }

    @Override
    public Response intercept(Interceptor.Chain chain) throws IOException {

        Request.Builder builder = chain.request().newBuilder();


        for (String cookie : GeneralUtils.getCookies(context)) {
            builder.addHeader("Cookie", cookie);
         }

        Log.i("cookie","ADD : \n"+GeneralUtils.getCookies(context).toString());

        Response response = chain.proceed(builder.build());

        return response;
    }
}