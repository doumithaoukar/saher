package com.acidwater.saher.forgotpassword;
import com.acidwater.saher.data.ServerResponse;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;

import io.reactivex.Observable;

/**
 * Created by Doumith on 7/6/2017.
 */
public interface ForgotPasswordMvp {

    interface Interactor {
        Observable<ServerResponse> submitEmail(String email);
    }

    interface Presenter extends MvpPresenter<View> {

        void submitEmail(String email);

        void validateEmail(String email);
    }

    interface View extends MvpView {


        void showErrorMessage(String message);

        void showLoadingView();

        void HideLoadingView();

        void setResultSuccessful(String message);
    }
}
