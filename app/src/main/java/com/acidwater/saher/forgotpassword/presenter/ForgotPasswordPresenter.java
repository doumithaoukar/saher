package com.acidwater.saher.forgotpassword.presenter;

import android.content.Context;
import android.text.TextUtils;


import com.acidwater.saher.R;
import com.acidwater.saher.data.ServerResponse;
import com.acidwater.saher.exceptions.SaherException;
import com.acidwater.saher.forgotpassword.ForgotPasswordMvp;
import com.acidwater.saher.forgotpassword.model.ForgotPasswordInteractor;
import com.acidwater.saher.services.ServiceObserver;
import com.acidwater.saher.utils.GeneralUtils;
import com.acidwater.saher.utils.RxUtils;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Doumith on 7/13/2017.
 */
public class ForgotPasswordPresenter extends MvpBasePresenter<ForgotPasswordMvp.View> implements ForgotPasswordMvp.Presenter {

    private Context context;
    private ForgotPasswordInteractor forgotPasswordInteractor;
    private CompositeDisposable compositeDisposable;

    public ForgotPasswordPresenter(Context context) {
        this.context = context;
        forgotPasswordInteractor = new ForgotPasswordInteractor(context);
        compositeDisposable = new CompositeDisposable();

    }

    @Override
    public void submitEmail(String email) {

        compositeDisposable.add(forgotPasswordInteractor.submitEmail(email).subscribeWith(new ServiceObserver<ServerResponse>() {
            @Override
            public void onStart() {
                if (isViewAttached()) {
                    getView().showLoadingView();
                }
            }

            @Override
            public void onComplete() {

                if (isViewAttached()) {

                    getView().HideLoadingView();
                }
            }

            @Override
            public void onNext(ServerResponse serverResponse) {

                if (isViewAttached()) {
                    getView().setResultSuccessful(serverResponse.getStatusDescription());
                }
            }

            @Override
            public void onError(SaherException e) {

                if (isViewAttached()) {

                    getView().showErrorMessage(e.errorMessage);
                    getView().HideLoadingView();
                }
            }
        }));
    }

    @Override
    public void validateEmail(String email) {

        if (TextUtils.isEmpty(email)) {

            if (isViewAttached()) {
                getView().showErrorMessage(context.getString(R.string.enter_email));
                return;
            }

        } else if (!GeneralUtils.isEmailValid(email)) {
            getView().showErrorMessage(context.getString(R.string.invalid_email));
            return;
        }

        submitEmail(email);
    }

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        if (!retainInstance) {
            RxUtils.disposeIfNotNull(compositeDisposable);
        }
    }
}
