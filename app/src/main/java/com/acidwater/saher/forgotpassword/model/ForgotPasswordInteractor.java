package com.acidwater.saher.forgotpassword.model;

import android.content.Context;

import com.acidwater.saher.data.ServerResponse;
import com.acidwater.saher.forgotpassword.ForgotPasswordMvp;
import com.acidwater.saher.services.TokenInterceptor;
import com.acidwater.saher.utils.ConstantStrings;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by Doumith on 7/13/2017.
 */
public class ForgotPasswordInteractor implements ForgotPasswordMvp.Interactor {


    private ForgotPasswordApi forgotPasswordApi;

    public ForgotPasswordInteractor(Context context) {
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder().addInterceptor(new TokenInterceptor(context)).build();
        String baseURL = ConstantStrings.BASE_URL;
        Retrofit.Builder retrofitBuilder = new Retrofit.Builder().baseUrl(baseURL.endsWith("/") ? baseURL : baseURL.concat("/"))
                .addConverterFactory(GsonConverterFactory.create()).addCallAdapterFactory(RxJava2CallAdapterFactory.create());
        retrofitBuilder.client(okHttpClient);
        Retrofit retrofit = retrofitBuilder.build();
        forgotPasswordApi = retrofit.create(ForgotPasswordApi.class);
    }


    @Override
    public Observable<ServerResponse> submitEmail(String email) {

        Observable<ServerResponse> observable = null;
        observable =
                forgotPasswordApi.submitEmail(email).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
        return observable;
    }


    public interface ForgotPasswordApi {
        @FormUrlEncoded
        @POST(ConstantStrings.SERVICE_FORGOT_PASSWORD)
        Observable<ServerResponse> submitEmail(@Field("EMAIL") String email);
    }


}
