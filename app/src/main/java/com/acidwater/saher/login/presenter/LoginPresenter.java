package com.acidwater.saher.login.presenter;

import android.content.Context;
import android.text.TextUtils;

import com.acidwater.saher.R;
import com.acidwater.saher.data.ServerResponse;
import com.acidwater.saher.exceptions.SaherException;
import com.acidwater.saher.forgotpassword.ForgotPasswordMvp;
import com.acidwater.saher.forgotpassword.model.ForgotPasswordInteractor;
import com.acidwater.saher.login.LoginMvp;
import com.acidwater.saher.login.model.LoginInteractor;
import com.acidwater.saher.services.ServiceObserver;
import com.acidwater.saher.utils.ConstantStrings;
import com.acidwater.saher.utils.GeneralUtils;
import com.acidwater.saher.utils.RxUtils;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;
import com.onesignal.OneSignal;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Doumith on 7/13/2017.
 */
public class LoginPresenter extends MvpBasePresenter<LoginMvp.View> implements LoginMvp.Presenter {

    private Context context;
    private LoginInteractor loginInteractor;
    private CompositeDisposable compositeDisposable;

    public LoginPresenter(Context context) {
        this.context = context;
        loginInteractor = new LoginInteractor(context);
        compositeDisposable = new CompositeDisposable();

    }

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        if (!retainInstance) {
            RxUtils.disposeIfNotNull(compositeDisposable);
        }
    }

    @Override
    public void submitLogin(String email, String password) {
        compositeDisposable.add(loginInteractor.submitLogin(email, password).subscribeWith(new ServiceObserver<ServerResponse>() {
            @Override
            public void onStart() {
                if (isViewAttached()) {
                    getView().showLoadingView();
                }
            }

            @Override
            public void onComplete() {

                if (isViewAttached()) {

                    getView().HideLoadingView();
                }
            }

            @Override
            public void onNext(ServerResponse serverResponse) {

                if (isViewAttached()) {
                    GeneralUtils.setSharedPref(context, ConstantStrings.USER_SESSION, ConstantStrings.user_is_logged_in);
                    GeneralUtils.setSharedPref(context, ConstantStrings.LOGGED_ACCOUNT_TYPE, ConstantStrings.EMAIL_ACCOUNT);
                    getView().setResultSuccessful(serverResponse.getStatusDescription());
                }
            }

            @Override
            public void onError(SaherException e) {

                if (isViewAttached()) {

                    getView().showErrorMessage(e.errorMessage);
                    getView().HideLoadingView();
                }
            }
        }));
    }

    @Override
    public void validateLogin(String email, String password) {
        if (TextUtils.isEmpty(email)) {

            if (isViewAttached()) {
                getView().showErrorMessage(context.getString(R.string.please_enter_username_email));
                return;
            }

        } else if (TextUtils.isEmpty(password) || password.length() < ConstantStrings.PASSWORD_LENGTH) {
            getView().showErrorMessage(context.getString(R.string.error_password_length));
            return;
        }

        submitLogin(email, password);
    }

    @Override
    public void facebookLogin(String token) {
        compositeDisposable.add(loginInteractor.facebookLogin(token).subscribeWith(new ServiceObserver<ServerResponse>() {
            @Override
            public void onStart() {
                if (isViewAttached()) {
                    getView().showLoadingView();
                }
            }

            @Override
            public void onComplete() {

                if (isViewAttached()) {

                    getView().HideLoadingView();
                }
            }

            @Override
            public void onNext(ServerResponse serverResponse) {

                if (isViewAttached()) {
                    GeneralUtils.setSharedPref(context, ConstantStrings.USER_SESSION, ConstantStrings.user_is_logged_in);
                    GeneralUtils.setSharedPref(context, ConstantStrings.LOGGED_ACCOUNT_TYPE, ConstantStrings.FACEBOOK_ACCOUNT);
                    getView().setResultSuccessful(serverResponse.getStatusDescription());
                }
            }

            @Override
            public void onError(SaherException e) {

                if (isViewAttached()) {

                    getView().showErrorMessage(e.errorMessage);
                    getView().HideLoadingView();
                }
            }
        }));
    }

    @Override
    public void googlePlusLogin(String token) {
        compositeDisposable.add(loginInteractor.googlePlusLogin(token).subscribeWith(new ServiceObserver<ServerResponse>() {
            @Override
            public void onStart() {
                if (isViewAttached()) {
                    getView().showLoadingView();
                }
            }

            @Override
            public void onComplete() {

                if (isViewAttached()) {

                    getView().HideLoadingView();
                }
            }

            @Override
            public void onNext(ServerResponse serverResponse) {

                if (isViewAttached()) {
                    GeneralUtils.setSharedPref(context, ConstantStrings.USER_SESSION, ConstantStrings.user_is_logged_in);
                    GeneralUtils.setSharedPref(context, ConstantStrings.LOGGED_ACCOUNT_TYPE, ConstantStrings.GOOGLE_PLUS_ACCOUNT);

                    getView().setResultSuccessful(serverResponse.getStatusDescription());
                }
            }

            @Override
            public void onError(SaherException e) {

                if (isViewAttached()) {

                    getView().showErrorMessage(e.errorMessage);
                    getView().HideLoadingView();
                }
            }
        }));
    }
}
