package com.acidwater.saher.login;

import com.acidwater.saher.data.ServerResponse;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;

import io.reactivex.Observable;

/**
 * Created by Doumith on 7/6/2017.
 */
public interface LoginMvp {

    interface Interactor {
        Observable<ServerResponse> submitLogin(String email, String password);

        Observable<ServerResponse> facebookLogin(String token);

        Observable<ServerResponse> googlePlusLogin(String token);
    }

    interface Presenter extends MvpPresenter<View> {

        void submitLogin(String email, String password);

        void validateLogin(String email, String password);

        void facebookLogin(String token);

        void googlePlusLogin(String token);


    }

    interface View extends MvpView {

        void showErrorMessage(String message);

        void showLoadingView();

        void HideLoadingView();

        void setResultSuccessful(String message);
    }
}
