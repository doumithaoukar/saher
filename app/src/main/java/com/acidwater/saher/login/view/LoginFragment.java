package com.acidwater.saher.login.view;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.acidwater.saher.IntroActivity;
import com.acidwater.saher.IntroActivityListener;
import com.acidwater.saher.R;
import com.acidwater.saher.forgotpassword.ForgotPasswordMvp;
import com.acidwater.saher.forgotpassword.presenter.ForgotPasswordPresenter;
import com.acidwater.saher.login.LoginMvp;
import com.acidwater.saher.login.presenter.LoginPresenter;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.hannesdorfmann.mosby3.mvp.MvpFragment;

import org.json.JSONObject;

import java.util.Arrays;

/**
 * Created by Doumith on 7/6/2017.
 */
public class LoginFragment extends MvpFragment<LoginMvp.View, LoginMvp.Presenter> implements GoogleApiClient.OnConnectionFailedListener, View.OnClickListener, LoginMvp.View {


    private Button btnContinue;
    private EditText email;
    private EditText password;
    private RelativeLayout progressBar;
    private IntroActivityListener mListener;
    private TextView btnForgotPassword;
    private ImageButton btnBack;
    private TextView title;
    private Button btnFacebook;
    private Button btnGoogle;
    private TextView btnSkip;
    private Button btnCreateAccount;

    public static final int GOOGLE_PLUS_SIGN_IN = 100;

    private GoogleApiClient mGoogleApiClient;
    private CallbackManager callbackManager;


    public static LoginFragment newInstance() {
        LoginFragment forgotPasswordFragment = new LoginFragment();
        return forgotPasswordFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(
                R.layout.fragment_login, container, false);

        btnContinue = (Button) mView.findViewById(R.id.btn_continue);
        progressBar = (RelativeLayout) mView.findViewById(R.id.main_progress);
        email = (EditText) mView.findViewById(R.id.email);
        password = (EditText) mView.findViewById(R.id.password);
        btnContinue.setOnClickListener(this);
        btnForgotPassword = (TextView) mView.findViewById(R.id.btnForgotPassword);
        btnForgotPassword.setOnClickListener(this);
        SpannableString content = new SpannableString(getString(R.string.forgot_password));
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        btnForgotPassword.setText(content);
        btnBack = (ImageButton) mView.findViewById(R.id.btnBack);
        btnBack.setOnClickListener(this);
        title = (TextView) mView.findViewById(R.id.title);

        btnFacebook = (Button) mView.findViewById(R.id.btn_facebook);
        btnGoogle = (Button) mView.findViewById(R.id.btn_google_plus);
        btnCreateAccount = (Button) mView.findViewById(R.id.btn_create_account);
        btnSkip = (TextView) mView.findViewById(R.id.btnSkip);

        btnFacebook.setOnClickListener(this);
        btnGoogle.setOnClickListener(this);
        btnCreateAccount.setOnClickListener(this);
        btnSkip.setOnClickListener(this);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.webclient_id))
                .requestEmail()
                .requestId()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .enableAutoManage(getActivity() /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        callbackManager = CallbackManager.Factory.create();
        handleFacebookSignInResult();

        return mView;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof IntroActivity) {
            mListener = (IntroActivity) activity;
        } else {
            throw new RuntimeException("you should implemented MainActivityListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_continue:
                presenter.validateLogin(email.getText().toString(), password.getText().toString());
                break;

            case R.id.btnForgotPassword:
                mListener.navigateToSection(IntroActivity.FORGOT_PASSWORD);
                break;

            case R.id.btnBack:
                mListener.onNavigationBackPressed();
                break;

            case R.id.btn_facebook:
                signInByFacebook();
                break;

            case R.id.btn_google_plus:
                signInByGooglePlus();
                break;

            case R.id.btn_create_account:
                destroyThis();
                mListener.navigateToSection(IntroActivity.SIGN_UP);
                break;

            case R.id.btnSkip:
                mListener.navigateToSection(IntroActivity.MAIN_ACTIVITY);
                break;
        }
    }

    @Override
    public LoginMvp.Presenter createPresenter() {
        return new LoginPresenter(getContext());
    }

    @Override
    public void showErrorMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void showLoadingView() {
        progressBar.setVisibility(View.VISIBLE);

    }

    @Override
    public void HideLoadingView() {
        progressBar.setVisibility(View.GONE);

    }

    @Override
    public void setResultSuccessful(String message) {
        mListener.navigateToSection(IntroActivity.MAIN_ACTIVITY);
        destroyThis();
    }

    private void destroyThis() {
        getFragmentManager().beginTransaction().remove(this).commit();
        getFragmentManager().popBackStack();
    }

    private void signInByGooglePlus() {

        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, GOOGLE_PLUS_SIGN_IN);
    }


    private void signInByFacebook() {
        LoginManager.getInstance().logOut();
        LoginManager.getInstance().logInWithReadPermissions(getActivity(),
                Arrays.asList("public_profile"));

    }

    private void handleGooglePlusSignInResult(GoogleSignInResult result) {
        Log.d("Google", "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {

            GoogleSignInAccount acct = result.getSignInAccount();
            String token = acct.getIdToken();
            presenter.googlePlusLogin(token);
            signOut();
            revokeAccess();
        } else {
            signOut();
        }
    }

    private void handleFacebookSignInResult() {
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        String token = loginResult.getAccessToken().getToken();
                        presenter.facebookLogin(token);
                    }

                    @Override
                    public void onCancel() {
                        Log.d("TEST", "On cancel");
                    }

                    @Override
                    public void onError(FacebookException error) {
                        Toast.makeText(getContext(), error.toString(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.stopAutoManage(getActivity());
            mGoogleApiClient.disconnect();
        }
    }

    private void signOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {

                    }
                });

    }

    private void revokeAccess() {
        Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                    }
                });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.stopAutoManage(getActivity());
            mGoogleApiClient.disconnect();
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (FacebookSdk.isFacebookRequestCode(requestCode)) {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        } else if (requestCode == GOOGLE_PLUS_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleGooglePlusSignInResult(result);
        }
    }

}