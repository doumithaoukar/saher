package com.acidwater.saher.login.model;

import android.content.Context;

import com.acidwater.saher.data.ServerResponse;
import com.acidwater.saher.forgotpassword.ForgotPasswordMvp;
import com.acidwater.saher.login.LoginMvp;
import com.acidwater.saher.services.ReceivedCookiesInterceptor;
import com.acidwater.saher.services.TokenInterceptor;
import com.acidwater.saher.utils.ConstantStrings;
import com.acidwater.saher.utils.GeneralUtils;

import com.franmontiel.persistentcookiejar.ClearableCookieJar;
import com.franmontiel.persistentcookiejar.PersistentCookieJar;
import com.franmontiel.persistentcookiejar.cache.SetCookieCache;
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Doumith on 7/13/2017.
 */
public class LoginInteractor implements LoginMvp.Interactor {


    private LoginAPI loginAPI;

    public LoginInteractor(Context context) {
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder().addInterceptor(new ReceivedCookiesInterceptor(context)).build();

        String baseURL = ConstantStrings.BASE_URL;
        Retrofit.Builder retrofitBuilder = new Retrofit.Builder().baseUrl(baseURL.endsWith("/") ? baseURL : baseURL.concat("/"))
                .addConverterFactory(GsonConverterFactory.create()).addCallAdapterFactory(RxJava2CallAdapterFactory.create());
        retrofitBuilder.client(okHttpClient);
        Retrofit retrofit = retrofitBuilder.build();
        loginAPI = retrofit.create(LoginAPI.class);
    }

    public Observable<ServerResponse> submitLogin(String email, String password) {
        Observable<ServerResponse> observable = null;
        observable =
                loginAPI.submitEmail(email, GeneralUtils.md5(password)).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
        return observable;
    }

    @Override
    public Observable<ServerResponse> facebookLogin(String token) {
        Observable<ServerResponse> observable = null;
        observable =
                loginAPI.facebookLogin(token).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
        return observable;
    }

    @Override
    public Observable<ServerResponse> googlePlusLogin(String token) {
        Observable<ServerResponse> observable = null;
        observable =
                loginAPI.googleLogin(token).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
        return observable;
    }


    public interface LoginAPI {
        @FormUrlEncoded
        @POST(ConstantStrings.SERVICE_LOGIN)
        Observable<ServerResponse> submitEmail(@Field("EMAIL") String email,
                                               @Field("PASSWORD") String password);

        @FormUrlEncoded
        @POST(ConstantStrings.SERVICE_FACEBOOK_LOGIN)
        Observable<ServerResponse> facebookLogin(@Field("TOKEN") String token);


        @FormUrlEncoded
        @POST(ConstantStrings.SERVICE_GOOGLE_LOGIN)
        Observable<ServerResponse> googleLogin(@Field("TOKEN") String token);

    }

}
