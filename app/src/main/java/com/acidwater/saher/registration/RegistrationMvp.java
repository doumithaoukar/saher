package com.acidwater.saher.registration;

import com.acidwater.saher.data.ServerResponse;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;

import io.reactivex.Observable;

/**
 * Created by Doumith on 7/6/2017.
 */
public interface RegistrationMvp {

    interface Interactor {
        Observable<ServerResponse> register(String username,String firstName, String lastName, String email, String password);
    }

    interface Presenter extends MvpPresenter<View> {

        void register(String username,String firstName, String lastName, String email, String password);

        void validate(String username,String firstName, String lastName, String email, String password, String retypePassword);

    }

    interface View extends MvpView {

        void showErrorMessage(String message);

        void showLoadingView();

        void HideLoadingView();

        void openLoginScreen();

    }
}
