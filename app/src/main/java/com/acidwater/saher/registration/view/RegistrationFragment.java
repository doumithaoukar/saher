package com.acidwater.saher.registration.view;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.acidwater.saher.IntroActivity;
import com.acidwater.saher.IntroActivityListener;
import com.acidwater.saher.R;
import com.acidwater.saher.registration.RegistrationMvp;
import com.acidwater.saher.registration.presenter.RegistrationPresenter;
import com.acidwater.saher.utils.ConstantStrings;
import com.hannesdorfmann.mosby3.mvp.MvpFragment;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Doumith on 7/6/2017.
 */
public class RegistrationFragment extends MvpFragment<RegistrationMvp.View, RegistrationMvp.Presenter> implements View.OnClickListener, RegistrationMvp.View {


    private Button btnContinue;
    private EditText firstName;
    private EditText lastName;
    private EditText email;
    private EditText password;
    private EditText confirmPassword;
    private RelativeLayout progressBar;

    private ImageButton btnBack;
    private TextView title;
    private EditText username;
    private CircleImageView profileImage;

    private IntroActivityListener mListener;

    public static RegistrationFragment newInstance() {
        RegistrationFragment registrationFragment = new RegistrationFragment();
        return registrationFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(
                R.layout.fragment_registration, container, false);

        firstName = (EditText) mView.findViewById(R.id.firstname);
        username = (EditText) mView.findViewById(R.id.username);
        lastName = (EditText) mView.findViewById(R.id.lastname);
        btnContinue = (Button) mView.findViewById(R.id.btn_continue);
        progressBar = (RelativeLayout) mView.findViewById(R.id.main_progress);
        email = (EditText) mView.findViewById(R.id.email);
        password = (EditText) mView.findViewById(R.id.password);
        confirmPassword = (EditText) mView.findViewById(R.id.confirmPassword);
        btnBack = (ImageButton) mView.findViewById(R.id.btnBack);
        btnBack.setOnClickListener(this);
        title = (TextView) mView.findViewById(R.id.title);
        btnContinue.setOnClickListener(this);
        profileImage = (CircleImageView) mView.findViewById(R.id.profile_image);
        profileImage.setOnClickListener(this);


        return mView;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_continue:
                presenter.validate(username.getText().toString(), firstName.getText().toString(), lastName.getText().toString(), email.getText().toString(),
                        password.getText().toString(), confirmPassword.getText().toString());
                break;

            case R.id.btnBack:
                mListener.onNavigationBackPressed();
                break;
        }
    }

    @Override
    public RegistrationMvp.Presenter createPresenter() {
        return new RegistrationPresenter(getContext());
    }

    @Override
    public void showErrorMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void showLoadingView() {
        progressBar.setVisibility(View.VISIBLE);

    }

    @Override
    public void HideLoadingView() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void openLoginScreen() {
        destroyThis();
        mListener.navigateToSection(IntroActivity.LOGIN);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof IntroActivity) {
            mListener = (IntroActivity) activity;
        } else {
            throw new RuntimeException("you should implemented MainActivityListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void destroyThis() {
        getFragmentManager().beginTransaction().remove(this).commit();
        getFragmentManager().popBackStack();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        mListener = null;

    }


}