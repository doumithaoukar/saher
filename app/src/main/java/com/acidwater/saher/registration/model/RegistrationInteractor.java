package com.acidwater.saher.registration.model;

import android.content.Context;

import com.acidwater.saher.data.ServerResponse;
import com.acidwater.saher.registration.RegistrationMvp;
import com.acidwater.saher.services.TokenInterceptor;
import com.acidwater.saher.utils.ConstantStrings;
import com.acidwater.saher.utils.GeneralUtils;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Doumith on 7/13/2017.
 */
public class RegistrationInteractor implements RegistrationMvp.Interactor {


    private RegistrationApi registrationApi;


    public RegistrationInteractor(Context context) {

        OkHttpClient okHttpClient = new OkHttpClient().newBuilder().addInterceptor(new TokenInterceptor(context)).build();
        String baseURL = ConstantStrings.BASE_URL;
        Retrofit.Builder retrofitBuilder = new Retrofit.Builder().baseUrl(baseURL.endsWith("/") ? baseURL : baseURL.concat("/"))
                .addConverterFactory(GsonConverterFactory.create()).addCallAdapterFactory(RxJava2CallAdapterFactory.create());
        retrofitBuilder.client(okHttpClient);
        Retrofit retrofit = retrofitBuilder.build();
        registrationApi = retrofit.create(RegistrationApi.class);
    }


    @Override
    public Observable<ServerResponse> register(String username, String firstName, String lastName, String email, String password) {
        Observable<ServerResponse> observable =
                registrationApi.register(
                        username, firstName + " " + lastName, email, GeneralUtils.md5(password)).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
        return observable;
    }


    public interface RegistrationApi {
        @FormUrlEncoded
        @POST(ConstantStrings.SERVICE_REGISTER_PROFILE)
        Observable<ServerResponse> register(@Field("USERNAME") String username,
                                            @Field("FULL_NAME") String fullName,
                                            @Field("EMAIL") String email,
                                            @Field("PASSWORD") String password);
    }

}
