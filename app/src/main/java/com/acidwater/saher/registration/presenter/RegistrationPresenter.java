package com.acidwater.saher.registration.presenter;

import android.content.Context;
import android.text.TextUtils;
import android.widget.Toast;


import com.acidwater.saher.R;
import com.acidwater.saher.data.ServerResponse;
import com.acidwater.saher.exceptions.SaherException;
import com.acidwater.saher.registration.RegistrationMvp;
import com.acidwater.saher.registration.model.RegistrationInteractor;
import com.acidwater.saher.services.ServiceObserver;
import com.acidwater.saher.utils.ConstantStrings;
import com.acidwater.saher.utils.GeneralUtils;
import com.acidwater.saher.utils.RxUtils;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Doumith on 7/13/2017.
 */
public class RegistrationPresenter extends MvpBasePresenter<RegistrationMvp.View> implements RegistrationMvp.Presenter {

    private Context context;
    private RegistrationInteractor registrationInteractor;
    private CompositeDisposable compositeDisposable;

    public RegistrationPresenter(Context context) {
        this.context = context;
        registrationInteractor = new RegistrationInteractor(context);
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        if (!retainInstance) {
            RxUtils.disposeIfNotNull(compositeDisposable);
        }
    }

    @Override
    public void register(String username,String firstName, String lastName, String email, String password) {

        compositeDisposable.add(registrationInteractor.register(username,firstName, lastName, email, password).subscribeWith(new ServiceObserver<ServerResponse>() {
            @Override
            public void onStart() {
                if (isViewAttached()) {
                    getView().showLoadingView();
                }
            }

            @Override
            public void onComplete() {

                if (isViewAttached()) {

                    getView().HideLoadingView();
                }
            }

            @Override
            public void onNext(ServerResponse serverResponse) {

                if (isViewAttached()) {
                    
                    getView().showErrorMessage(serverResponse.getStatusDescription());
                    getView().openLoginScreen();
                }
            }

            @Override
            public void onError(SaherException e) {

                if (isViewAttached()) {

                    getView().showErrorMessage(e.errorMessage);
                    getView().HideLoadingView();
                }
            }
        }));
    }

    @Override
    public void validate(String username,String firstName, String lastName, String email, String password, String confirmPassword) {
        if (TextUtils.isEmpty(username) ||TextUtils.isEmpty(firstName) || TextUtils.isEmpty(lastName) || TextUtils.isEmpty(email)
                || TextUtils.isEmpty(password) || TextUtils.isEmpty(confirmPassword)) {

            if (isViewAttached()) {
                getView().showErrorMessage(context.getString(R.string.enter_required_fields));
                return;
            }
        } else if (!GeneralUtils.isEmailValid(email)) {
            getView().showErrorMessage(context.getString(R.string.invalid_email));
            return;
        } else if (!confirmPassword.equals(password)) {
            getView().showErrorMessage(context.getString(R.string.password_dont_match));
            return;
        } else if (password.length() < ConstantStrings.PASSWORD_LENGTH) {
            getView().showErrorMessage(context.getString(R.string.error_password_length));
            return;
        }

        register(username,firstName, lastName, email, password);
    }
}
