package com.acidwater.saher.widgets;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by D on 7/20/2015.
 */
public class SpacesItemDecorationNoTopMargin extends RecyclerView.ItemDecoration {
    private int space;

    public SpacesItemDecorationNoTopMargin(int space) {
        this.space = space;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        outRect.left = space;
        outRect.right = space;
        outRect.bottom = space;
        outRect.top = 0;
    }
}