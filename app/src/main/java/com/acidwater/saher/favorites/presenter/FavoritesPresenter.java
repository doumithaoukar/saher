package com.acidwater.saher.favorites.presenter;

import android.content.Context;

import com.acidwater.saher.calendar.CalendarMvp;
import com.acidwater.saher.calendar.model.CalendarInteractor;
import com.acidwater.saher.data.DataEntitiesWrapper;
import com.acidwater.saher.data.DataEntityWrapper;
import com.acidwater.saher.data.ServerResponse;
import com.acidwater.saher.events.model.Event;
import com.acidwater.saher.exceptions.SaherException;
import com.acidwater.saher.favorites.FavoritesMvp;
import com.acidwater.saher.favorites.model.FavoritesInteractor;
import com.acidwater.saher.profile.model.ProfileInteractor;
import com.acidwater.saher.profile.model.User;
import com.acidwater.saher.services.ServiceObserver;
import com.acidwater.saher.utils.GeneralUtils;
import com.acidwater.saher.utils.RxUtils;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import java.util.List;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Doumith on 7/13/2017.
 */
public class FavoritesPresenter extends MvpBasePresenter<FavoritesMvp.View> implements FavoritesMvp.Presenter {

    private Context context;
    private FavoritesInteractor favoritesInteractor;
    private ProfileInteractor profileInteractor;
    private CompositeDisposable compositeDisposable;

    public FavoritesPresenter(Context context) {
        this.context = context;
        favoritesInteractor = new FavoritesInteractor(context);
        compositeDisposable = new CompositeDisposable();
        profileInteractor = new ProfileInteractor(context);

    }

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        if (!retainInstance) {
            RxUtils.disposeIfNotNull(compositeDisposable);
        }
    }

    @Override
    public void getFavorites() {

        if (favoritesInteractor.getFavorites() != null) {
            if (isViewAttached()) {
                getView().setData(favoritesInteractor.getFavorites());
            }
        } else {
            if (isViewAttached()) {
                getView().showEmptyView();
            }
        }

    }

    @Override
    public void addToFavorite(int id, String entityType) {
        compositeDisposable.add(favoritesInteractor.addToFavorite(id, entityType).subscribeWith(new ServiceObserver<ServerResponse>() {
            @Override
            public void onStart() {

            }

            @Override
            public void onComplete() {

            }

            @Override
            public void onNext(ServerResponse serverResponse) {
                compositeDisposable.add(profileInteractor.getProfile().subscribeWith(new ServiceObserver<DataEntityWrapper<User>>() {
                    @Override
                    public void onStart() {

                    }

                    @Override
                    public void onComplete() {

                    }

                    @Override
                    public void onNext(DataEntityWrapper<User> userDataEntityWrapper) {
                        if (userDataEntityWrapper != null) {
                            GeneralUtils.saveUserProfile(context, userDataEntityWrapper.getData());
                        }
                    }

                    @Override
                    public void onError(SaherException e) {

                    }
                }));
            }

            @Override
            public void onError(SaherException e) {

            }
        }));
    }

    @Override
    public void removeFromFavorite(int id, String entityType) {
        compositeDisposable.add(favoritesInteractor.removeFromFavorite(id, entityType).subscribeWith(new ServiceObserver<ServerResponse>() {
            @Override
            public void onStart() {

            }

            @Override
            public void onComplete() {

            }

            @Override
            public void onNext(ServerResponse serverResponse) {
                compositeDisposable.add(profileInteractor.getProfile().subscribeWith(new ServiceObserver<DataEntityWrapper<User>>() {
                    @Override
                    public void onStart() {

                    }

                    @Override
                    public void onComplete() {

                    }

                    @Override
                    public void onNext(DataEntityWrapper<User> userDataEntityWrapper) {
                        if (userDataEntityWrapper != null) {
                            GeneralUtils.saveUserProfile(context, userDataEntityWrapper.getData());
                        }
                    }

                    @Override
                    public void onError(SaherException e) {

                    }
                }));
            }

            @Override
            public void onError(SaherException e) {

            }
        }));
    }

    @Override
    public boolean isFavorite(int id, String entityType) {
        return favoritesInteractor.isFavorite(id, entityType);
    }
}
