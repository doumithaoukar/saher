package com.acidwater.saher.favorites.model;

import android.content.Context;

import com.acidwater.saher.data.DataEntityWrapper;
import com.acidwater.saher.data.ServerResponse;
import com.acidwater.saher.favorites.FavoritesMvp;
import com.acidwater.saher.profile.model.User;
import com.acidwater.saher.services.AddCookiesInterceptor;
import com.acidwater.saher.utils.ConstantStrings;
import com.acidwater.saher.utils.GeneralUtils;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Doumith on 7/13/2017.
 */
public class FavoritesInteractor implements FavoritesMvp.Interactor {


    private Context context;
    private FavoritesApi favoritesApi;

    public FavoritesInteractor(Context context) {
        this.context = context;
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder().addInterceptor(new AddCookiesInterceptor(context)).build();

        String baseURL = ConstantStrings.BASE_URL;
        Retrofit.Builder retrofitBuilder = new Retrofit.Builder().baseUrl(baseURL.endsWith("/") ? baseURL : baseURL.concat("/"))
                .addConverterFactory(GsonConverterFactory.create()).addCallAdapterFactory(RxJava2CallAdapterFactory.create());
        retrofitBuilder.client(okHttpClient);
        Retrofit retrofit = retrofitBuilder.build();
        favoritesApi = retrofit.create(FavoritesApi.class);
    }

    @Override
    public ArrayList<Favorite> getFavorites() {

        if (GeneralUtils.getUserProfile(context) != null && GeneralUtils.getUserProfile(context).getFavorites() != null) {

            return GeneralUtils.getUserProfile(context).getFavorites();
        }

        return null;
    }

    @Override
    public Observable<ServerResponse> addToFavorite(int id, String entityType) {
        Observable<ServerResponse> observable = null;
        observable =
                favoritesApi.addToFavorite(id, entityType).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
        return observable;
    }

    @Override
    public Observable<ServerResponse> removeFromFavorite(int id, String entityType) {
        Observable<ServerResponse> observable = null;
        observable =
                favoritesApi.removeFromFavorite(id, entityType).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
        return observable;
    }

    @Override
    public boolean isFavorite(int id, String entityType) {
        if (GeneralUtils.getUserProfile(context) != null) {
            List<Favorite> favorites = GeneralUtils.getUserProfile(context).getFavorites();
            for (Favorite favorite : favorites) {
                if (favorite.getId() == id && favorite.getEntityType().equals(entityType))
                    return true;
            }
        }
        return false;
    }

    public interface FavoritesApi {
        @FormUrlEncoded
        @POST(ConstantStrings.SERVICE_ADD_FAVORITE)
        Observable<ServerResponse> addToFavorite(@Field("ENTITY_ID") int id, @Field("ENTITY_TYPE") String entityType);

        @FormUrlEncoded
        @POST(ConstantStrings.SERVICE_REMOVE_FAVORITE)
        Observable<ServerResponse> removeFromFavorite(@Field("ENTITY_ID") int id, @Field("ENTITY_TYPE") String entityType);
    }

}
