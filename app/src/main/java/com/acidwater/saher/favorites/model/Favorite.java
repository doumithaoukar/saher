package com.acidwater.saher.favorites.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Doumith on 10/21/2017.
 */
public class Favorite implements Parcelable {

    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String name;

    @SerializedName("teaser")
    private String teaser;

    @SerializedName("entity_type")
    private String entityType;

    @SerializedName("event_date")
    private String event_date;

    @SerializedName("average_rating")
    private int rating;

    @SerializedName("thumbnail")
    private String thumbnail;

    @SerializedName("city")
    private String city;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTeaser() {
        return teaser;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTeaser(String teaser) {
        this.teaser = teaser;
    }



    public String getEvent_date() {
        return event_date;
    }

    public void setEvent_date(String event_date) {
        this.event_date = event_date;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }


    public Favorite() {
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String image) {
        this.thumbnail = image;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeString(this.teaser);
        dest.writeString(this.entityType);
        dest.writeString(this.event_date);
        dest.writeInt(this.rating);
        dest.writeString(this.thumbnail);
        dest.writeString(this.city);
    }

    protected Favorite(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.teaser = in.readString();
        this.entityType = in.readString();
        this.event_date = in.readString();
        this.rating = in.readInt();
        this.thumbnail = in.readString();
        this.city = in.readString();
    }

    public static final Creator<Favorite> CREATOR = new Creator<Favorite>() {
        @Override
        public Favorite createFromParcel(Parcel source) {
            return new Favorite(source);
        }

        @Override
        public Favorite[] newArray(int size) {
            return new Favorite[size];
        }
    };

    public String getEntityType() {
        return entityType;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }
}
