package com.acidwater.saher.favorites.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.acidwater.saher.R;
import com.acidwater.saher.events.model.Event;
import com.acidwater.saher.favorites.model.Favorite;
import com.acidwater.saher.utils.DateUtils;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by D on 7/20/2015.
 */

public class FavoritesAdapter extends RecyclerView.Adapter<FavoritesAdapter.ViewHolder> {

    private List<Favorite> list;
    private Context context;
    private onItemClickListener onItemClickListener;

    public FavoritesAdapter(Context context, List<Favorite> list) {
        this.list = list;
        this.context = context;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView title;
        public TextView teaser;
        public ImageView image;
        public TextView event_day;
        public TextView event_month;


        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            teaser = (TextView) itemLayoutView.findViewById(R.id.event_teaser);
            title = (TextView) itemLayoutView.findViewById(R.id.event_title);
            image = (ImageView) itemLayoutView.findViewById(R.id.event_image);
            event_day = (TextView) itemLayoutView.findViewById(R.id.event_day);
            event_month = (TextView) itemLayoutView.findViewById(R.id.event_month);
        }
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_event, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        Favorite favorite = list.get(position);
        if (favorite.getName() != null) {
            viewHolder.title.setText(favorite.getName());

        }
        viewHolder.teaser.setText(favorite.getTeaser());

        if (!TextUtils.isEmpty(favorite.getThumbnail())) {
            Picasso.with(context).load(favorite.getThumbnail()).into(viewHolder.image);

        }

        if (!TextUtils.isEmpty(favorite.getEvent_date())) {
            viewHolder.event_day.setText(String.valueOf(DateUtils.getDay(favorite.getEvent_date())));
            viewHolder.event_month.setText(DateUtils.getMonthName(favorite.getEvent_date()));

        }

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClickListener != null) {
                    onItemClickListener.onClick(list.get(position), position);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface onItemClickListener {
        void onClick(Favorite favorite, int position);
    }

    public void setOnItemClickListener(FavoritesAdapter.onItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }


}