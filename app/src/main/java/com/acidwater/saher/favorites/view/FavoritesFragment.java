package com.acidwater.saher.favorites.view;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.acidwater.saher.R;
import com.acidwater.saher.calendar.CalendarMvp;
import com.acidwater.saher.calendar.presenter.CalendarPresenter;
import com.acidwater.saher.data.DataEntitiesWrapper;
import com.acidwater.saher.events.model.Event;
import com.acidwater.saher.events.view.EventAdapter;
import com.acidwater.saher.favorites.FavoritesMvp;
import com.acidwater.saher.favorites.model.Favorite;
import com.acidwater.saher.favorites.presenter.FavoritesPresenter;
import com.acidwater.saher.main.MainActivityListener;
import com.acidwater.saher.utils.ConstantStrings;
import com.acidwater.saher.utils.RxUtils;
import com.acidwater.saher.utils.bus.MainBus;
import com.acidwater.saher.utils.bus.events.UpdateProfile;
import com.acidwater.saher.utils.mvp.LceRetryFragment;
import com.acidwater.saher.widgets.SpacesItemDecorationNoTopMargin;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceFragment;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;


/**
 * Created by Doumith on 7/6/2017.
 */
public class FavoritesFragment extends LceRetryFragment<View, ArrayList<Favorite>, FavoritesMvp.View, FavoritesMvp.Presenter> implements View.OnClickListener, FavoritesMvp.View {


    private RecyclerView recyclerView;
    private MainActivityListener mListener;
    private TextView emptyView;
    private CompositeDisposable compositeDisposable;
    private FavoritesAdapter favoritesAdapter;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MainActivityListener) {
            mListener = (MainActivityListener) activity;
        } else {
            throw new RuntimeException("you should implemented MainActivityListener");
        }
    }

    public static FavoritesFragment newInstance() {
        FavoritesFragment calendarFragment = new FavoritesFragment();
        return calendarFragment;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        RxUtils.disposeIfNotNull(compositeDisposable);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadData(false);

    }

    @Override
    protected void customizeLoadingView(View loadingView) {

    }

    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View containerView = inflater.inflate(R.layout.fragment_favorites, container, false);
        recyclerView = (RecyclerView) containerView.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.addItemDecoration(new SpacesItemDecorationNoTopMargin(35));
        compositeDisposable = new CompositeDisposable();
        emptyView = (TextView) containerView.findViewById(R.id.emptyView);
        registerToBus();
        return containerView;
    }


    private void registerToBus() {
        compositeDisposable.add(MainBus.getInstance().getBusObservable().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<Object>() {
            @Override
            public void accept(Object object) throws Exception {

                if (object instanceof UpdateProfile) {
                    if (favoritesAdapter != null) {
                        recyclerView.setAdapter(null);
                    }
                    loadData(false);
                }

            }

        }));
    }


    @Override
    public FavoritesMvp.Presenter createPresenter() {
        return new FavoritesPresenter(getContext());
    }

    @Override
    public void retry() {
        loadData(false);
    }

    @Override
    public void showErrorMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void showEmptyView() {
        emptyView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideEmptyView() {
        emptyView.setVisibility(View.GONE);

    }


    private void destroyThis() {
        getFragmentManager().beginTransaction().remove(this).commit();
        getFragmentManager().popBackStack();
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void setData(ArrayList<Favorite> data) {
        if (data == null || data.size() == 0) {
            showEmptyView();
        } else {
            hideEmptyView();
            favoritesAdapter = new FavoritesAdapter(getContext(), data);
            recyclerView.setAdapter(favoritesAdapter);
            favoritesAdapter.setOnItemClickListener(new FavoritesAdapter.onItemClickListener() {
                @Override
                public void onClick(Favorite favorite, int position) {
                    switch (favorite.getEntityType()) {
                        case "event":
                            mListener.navigateToEventDetails(favorite.getId());
                            break;

                        case "artist":
                            mListener.navigateToMusicianDetails(favorite.getId());
                            break;

                        case "band":
                            mListener.navigateToMusicianDetails(favorite.getId());
                            break;

                        case "pub":
                            mListener.navigateToPlaceDetails(favorite.getId());
                            break;
                    }
                }
            });
        }

    }

    @Override
    public void loadData(boolean pullToRefresh) {
        presenter.getFavorites();
    }
}