package com.acidwater.saher.favorites;

import com.acidwater.saher.data.DataEntitiesWrapper;
import com.acidwater.saher.data.ServerResponse;
import com.acidwater.saher.events.model.Event;
import com.acidwater.saher.favorites.model.Favorite;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

/**
 * Created by Doumith on 6/18/2017.
 */
public interface FavoritesMvp {

    interface Interactor {
        ArrayList<Favorite> getFavorites();

        Observable<ServerResponse> addToFavorite(int id, String entityType);

        Observable<ServerResponse> removeFromFavorite(int id, String entityType);

        boolean isFavorite(int id, String entityType);
    }

    interface Presenter extends MvpPresenter<View> {
        void getFavorites();

        void addToFavorite(int id, String entityType);

        void removeFromFavorite(int id, String entityType);

        boolean isFavorite(int id, String entityType);
    }

    interface View extends MvpLceView<ArrayList<Favorite>> {

        void showErrorMessage(String message);

        void showEmptyView();

        void hideEmptyView();

    }
}
