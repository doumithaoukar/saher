package com.acidwater.saher.search.model;

import android.content.Context;

import com.acidwater.saher.data.DataEntitiesWrapper;
import com.acidwater.saher.events.model.Event;
import com.acidwater.saher.search.SearchMvp;
import com.acidwater.saher.services.TokenInterceptor;
import com.acidwater.saher.utils.ConstantStrings;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

/**
 * Created by Doumith on 4/28/2018.
 */
public class SearchInteractor implements SearchMvp.Interactor {


    private SearchApi searchApi;

    public SearchInteractor(Context context) {
        String baseURL = ConstantStrings.BASE_URL;
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder().addInterceptor(new TokenInterceptor(context)).build();
        Retrofit.Builder retrofitBuilder = new Retrofit.Builder().baseUrl(baseURL.endsWith("/") ? baseURL : baseURL.concat("/"))
                .addConverterFactory(GsonConverterFactory.create()).addCallAdapterFactory(RxJava2CallAdapterFactory.create());
        retrofitBuilder.client(okHttpClient);
        Retrofit retrofit = retrofitBuilder.build();
        searchApi = retrofit.create(SearchApi.class);
    }


    @Override
    public Observable<DataEntitiesWrapper<List<SearchResult>>> search(String query, int page) {

        String URL = ConstantStrings.BASE_URL.endsWith("/") ? ConstantStrings.BASE_URL : ConstantStrings.BASE_URL.concat("/");
        URL += ConstantStrings.SERVICE_SEARCH + "/" + query;
        Map<String, String> params = new HashMap<String, String>();
        params.put("page", String.valueOf(page));
        params.put("limit", String.valueOf(ConstantStrings.SEARCH_PAGING));
        Observable<DataEntitiesWrapper<List<SearchResult>>> observable =
                searchApi.search(URL, params).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());

        return observable;
    }


    public interface SearchApi {
        @Headers("Content-Type: application/json")
        @GET
        Observable<DataEntitiesWrapper<List<SearchResult>>> search(@Url String url, @QueryMap Map<String, String> params);
    }
}
