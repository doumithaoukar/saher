package com.acidwater.saher.search.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.acidwater.saher.R;
import com.acidwater.saher.events.model.Event;
import com.acidwater.saher.search.model.SearchResult;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by D on 7/20/2015.
 */

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.ViewHolder> {

    private List<SearchResult> list;
    private Context context;
    private onItemClickListener onItemClickListener;

    public SearchAdapter(Context context, List<SearchResult> list) {
        this.list = list;
        this.context = context;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView title;
        public TextView teaser;
        public ImageView image;


        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            teaser = (TextView) itemLayoutView.findViewById(R.id.teaser);
            title = (TextView) itemLayoutView.findViewById(R.id.title);
            image = (ImageView) itemLayoutView.findViewById(R.id.image);
        }
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_search, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        SearchResult searchResult = list.get(position);
        if (searchResult.getName() != null) {
            viewHolder.title.setText(searchResult.getName());

        }
        viewHolder.teaser.setText(searchResult.getTeaser());

        if (searchResult.getCover() != null) {
            Picasso.with(context).load(searchResult.getCover()).into(viewHolder.image);

        }


        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClickListener != null) {
                    onItemClickListener.onClick(list.get(position), position);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface onItemClickListener {
        void onClick(SearchResult searchResult, int position);
    }

    public void setOnItemClickListener(SearchAdapter.onItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public List<SearchResult> getList() {
        return list;
    }

    public void setList(List<SearchResult> list) {
        this.list = list;
    }
}