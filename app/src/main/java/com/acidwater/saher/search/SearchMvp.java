package com.acidwater.saher.search;

import com.acidwater.saher.data.DataEntitiesWrapper;
import com.acidwater.saher.search.model.SearchResult;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by Doumith on 4/28/2018.
 */
public interface SearchMvp {

    interface Interactor {

        Observable<DataEntitiesWrapper<List<SearchResult>>> search(String query,int page);
    }

    interface Presenter extends MvpPresenter<View> {

        void search(String query, int page);

        void loadMoreSearch(String query, int page);


    }

    interface View extends MvpLceView<DataEntitiesWrapper<List<SearchResult>>> {

        void setError(String message);

        void hideLoadMore();

        void showEmptyView();

        void hideEmptyView();

    }


}



