package com.acidwater.saher.search.presenter;

import android.content.Context;

import com.acidwater.saher.data.DataEntitiesWrapper;
import com.acidwater.saher.events.model.Event;
import com.acidwater.saher.events.model.EventInteractor;
import com.acidwater.saher.exceptions.SaherException;
import com.acidwater.saher.search.SearchMvp;
import com.acidwater.saher.search.model.SearchInteractor;
import com.acidwater.saher.search.model.SearchResult;
import com.acidwater.saher.services.ServiceObserver;
import com.acidwater.saher.utils.RxUtils;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import java.util.List;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Doumith on 4/28/2018.
 */
public class SearchPresenter extends MvpBasePresenter<SearchMvp.View> implements SearchMvp.Presenter {

    private SearchInteractor searchInteractor;
    private CompositeDisposable compositeDisposable;


    public SearchPresenter(Context context) {
        searchInteractor = new SearchInteractor(context);
        compositeDisposable = new CompositeDisposable();
    }


    @Override
    public void search(String query, int page) {
        if (isViewAttached()) {
            getView().showLoading(false);
        }

        compositeDisposable.add(searchInteractor.search(query, page).subscribeWith(new ServiceObserver<DataEntitiesWrapper<List<SearchResult>>>() {
            @Override
            public void onStart() {

            }

            @Override
            public void onComplete() {

                if (isViewAttached()) {
                    getView().showContent();
                }
            }

            @Override
            public void onNext(DataEntitiesWrapper<List<SearchResult>> listDataEntitiesWrapper) {
                if (isViewAttached()) {
                    getView().setData(listDataEntitiesWrapper);
                }
            }

            @Override
            public void onError(SaherException e) {

                if (isViewAttached()) {
                    getView().showError(e, false);
                }
            }
        }));
    }

    @Override
    public void loadMoreSearch(String query, int page) {
        compositeDisposable.add(searchInteractor.search(query, page).subscribeWith(new ServiceObserver<DataEntitiesWrapper<List<SearchResult>>>() {
            @Override
            public void onStart() {

            }

            @Override
            public void onComplete() {
                if (isViewAttached()) {
                    getView().hideLoadMore();
                }

            }

            @Override
            public void onNext(DataEntitiesWrapper<List<SearchResult>> listDataEntitiesWrapper) {
                if (isViewAttached()) {
                    getView().setData(listDataEntitiesWrapper);
                }
            }

            @Override
            public void onError(SaherException e) {

                if (isViewAttached()) {
                    getView().hideLoadMore();
                }
            }
        }));
    }

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        RxUtils.disposeIfNotNull(compositeDisposable);
    }
}
