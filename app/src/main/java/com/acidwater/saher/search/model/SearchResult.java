package com.acidwater.saher.search.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Doumith on 4/28/2018.
 */
public class SearchResult implements Parcelable {

    private String id;
    private String name;
    private String teaser;
    private String position;
    private String cover;
    private String thumbnail;
    private String type;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTeaser() {
        return teaser;
    }

    public void setTeaser(String teaser) {
        this.teaser = teaser;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeString(this.teaser);
        dest.writeString(this.position);
        dest.writeString(this.cover);
        dest.writeString(this.thumbnail);
        dest.writeString(this.type);
    }

    public SearchResult() {
    }

    protected SearchResult(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.teaser = in.readString();
        this.position = in.readString();
        this.cover = in.readString();
        this.thumbnail = in.readString();
        this.type = in.readString();
    }

    public static final Parcelable.Creator<SearchResult> CREATOR = new Parcelable.Creator<SearchResult>() {
        @Override
        public SearchResult createFromParcel(Parcel source) {
            return new SearchResult(source);
        }

        @Override
        public SearchResult[] newArray(int size) {
            return new SearchResult[size];
        }
    };
}
