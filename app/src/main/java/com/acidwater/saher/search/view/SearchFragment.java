package com.acidwater.saher.search.view;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.acidwater.saher.R;
import com.acidwater.saher.data.DataEntitiesWrapper;
import com.acidwater.saher.events.EventMvp;
import com.acidwater.saher.events.model.Event;
import com.acidwater.saher.events.presenter.EventsPresenter;
import com.acidwater.saher.events.view.EventAdapter;
import com.acidwater.saher.main.MainActivityListener;
import com.acidwater.saher.search.SearchMvp;
import com.acidwater.saher.search.model.SearchResult;
import com.acidwater.saher.search.presenter.SearchPresenter;
import com.acidwater.saher.utils.ConstantStrings;
import com.acidwater.saher.utils.mvp.LceRetryFragment;
import com.acidwater.saher.widgets.SpacesItemDecoration;
import com.jcodecraeer.xrecyclerview.XRecyclerView;

import java.util.List;

/**
 * Created by Doumith on 6/9/2017.
 */
public class SearchFragment extends LceRetryFragment<View, DataEntitiesWrapper<List<SearchResult>>, SearchMvp.View, SearchMvp.Presenter> implements SearchMvp.View {

    private int page = 1;
    private XRecyclerView recyclerView;
    private MainActivityListener mListener;
    private LinearLayoutManager layoutManager;
    private SearchAdapter searchAdapter;
    private SwipeRefreshLayout swipeRefreshLayout;

    private static final String QUERY_KEY = "QUERY";
    private String queryString = "";
    private TextView emptyView;


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MainActivityListener) {
            mListener = (MainActivityListener) activity;
        } else {
            throw new RuntimeException("you should implemented MainActivityListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public static SearchFragment newInstance(String query) {
        SearchFragment searchFragment = new SearchFragment();
        Bundle bundle = new Bundle();
        bundle.putString(QUERY_KEY, query);
        searchFragment.setArguments(bundle);
        return searchFragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        loadData(false);
    }

    @Override
    protected void customizeLoadingView(View loadingView) {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View containerView = inflater.inflate(R.layout.fragment_search, container, false);
        // reset all data after fragment is destroyed
        page = 1;
        swipeRefreshLayout = (SwipeRefreshLayout) containerView.findViewById(R.id.swipe_refresh_layout);
        recyclerView = (XRecyclerView) containerView.findViewById(R.id.recyclerView);
        layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new SpacesItemDecoration(35));
        recyclerView.setPullRefreshEnabled(false);
        emptyView = (TextView) containerView.findViewById(R.id.emptyView);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                page = 1;
                presenter.search(queryString, page);
            }
        });

        if (savedInstanceState != null) {
            queryString = savedInstanceState.getString(QUERY_KEY);
        } else {
            if (getArguments() != null) {
                queryString = getArguments().getString(QUERY_KEY);
            }
        }
        return containerView;

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(QUERY_KEY, queryString);
    }

    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return null;
    }

    @Override
    public void setData(DataEntitiesWrapper<List<SearchResult>> data) {

        if (data == null || data.getData().isEmpty()) {
            showEmptyView();

        } else {
            hideEmptyView();
            if (page == 1) {
                searchAdapter = new SearchAdapter(getContext(), data.getData());
                searchAdapter.setOnItemClickListener(new SearchAdapter.onItemClickListener() {
                    @Override
                    public void onClick(SearchResult searchResult, int position) {
                        switch (searchResult.getType()) {

                            case "event":
                                try {
                                    int eventID = Integer.parseInt(searchResult.getId());
                                    mListener.navigateToEventDetails(eventID);

                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                                break;

                            case "artist":
                                try {
                                    int artistID = Integer.parseInt(searchResult.getId());
                                    mListener.navigateToMusicianDetails(artistID);

                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }

                                break;

                            case "band":
                                try {
                                    int bandID = Integer.parseInt(searchResult.getId());
                                    mListener.navigateToMusicianDetails(bandID);

                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                                break;

                            case "pub":
                                try {
                                    int placeID = Integer.parseInt(searchResult.getId());
                                    mListener.navigateToPlaceDetails(placeID);

                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                                break;
                        }
                    }
                });
                recyclerView.setAdapter(searchAdapter);
                recyclerView.setLoadingMoreEnabled(true);
                recyclerView.setLoadingListener(new XRecyclerView.LoadingListener() {
                    @Override
                    public void onRefresh() {
                        //refresh data here
                    }

                    @Override
                    public void onLoadMore() {
                        // load more data here
                        page++;
                        presenter.loadMoreSearch(queryString, page);
                    }
                });
            } else {
                searchAdapter.getList().addAll(data.getData());
                searchAdapter.notifyDataSetChanged();
                hideLoadMore();
                if (data.getData().size() < ConstantStrings.EVENTS_PAGING) {
                    recyclerView.setLoadingMoreEnabled(false);
                }
            }

            swipeRefreshLayout.setRefreshing(false);
        }


    }

    @Override
    public void loadData(boolean pullToRefresh) {
        presenter.search(queryString, page);
    }

    @Override
    public SearchMvp.Presenter createPresenter() {
        return new SearchPresenter(getContext());
    }

    @Override
    public void retry() {
        page = 1;
        loadData(false);
    }


    @Override
    public void setError(String message) {

    }

    @Override
    public void hideLoadMore() {
        recyclerView.loadMoreComplete();
    }

    @Override
    public void showEmptyView() {
        emptyView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideEmptyView() {
        emptyView.setVisibility(View.GONE);

    }

}
