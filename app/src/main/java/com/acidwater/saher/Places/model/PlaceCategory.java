package com.acidwater.saher.Places.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Doumith on 7/15/2017.
 */
public class PlaceCategory implements Parcelable {

    private int id;
    private String name;
    @SerializedName("pubs")
    private List<Place> places;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Place> getPlaces() {
        return places;
    }

    public void setPlaces(List<Place> places) {
        this.places = places;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeTypedList(this.places);
    }

    public PlaceCategory() {
    }

    protected PlaceCategory(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.places = in.createTypedArrayList(Place.CREATOR);
    }

    public static final Parcelable.Creator<PlaceCategory> CREATOR = new Parcelable.Creator<PlaceCategory>() {
        @Override
        public PlaceCategory createFromParcel(Parcel source) {
            return new PlaceCategory(source);
        }

        @Override
        public PlaceCategory[] newArray(int size) {
            return new PlaceCategory[size];
        }
    };
}
