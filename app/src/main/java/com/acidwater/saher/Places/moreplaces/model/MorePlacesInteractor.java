package com.acidwater.saher.Places.moreplaces.model;

import com.acidwater.saher.Places.model.Place;
import com.acidwater.saher.Places.moreplaces.MorePlacesMvp;
import com.acidwater.saher.data.DataEntitiesWrapper;
import com.acidwater.saher.musicians.model.Musician;
import com.acidwater.saher.musicians.moremusicians.MoreMusicianMvp;
import com.acidwater.saher.utils.ConstantStrings;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

/**
 * Created by Doumith on 6/18/2017.
 */
public class MorePlacesInteractor implements MorePlacesMvp.Interactor {


    private MorePlacesApi morePlacesApi;

    public MorePlacesInteractor() {
        String baseURL = ConstantStrings.BASE_URL;
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder().build();
        Retrofit.Builder retrofitBuilder = new Retrofit.Builder().baseUrl(baseURL.endsWith("/") ? baseURL : baseURL.concat("/"))
                .addConverterFactory(GsonConverterFactory.create()).addCallAdapterFactory(RxJava2CallAdapterFactory.create());
        retrofitBuilder.client(okHttpClient);
        Retrofit retrofit = retrofitBuilder.build();
        morePlacesApi = retrofit.create(MorePlacesApi.class);
    }


    @Override
    public Observable<DataEntitiesWrapper<List<Place>>> getPlacesByCategory(int page, int categoryID) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("categories[]", String.valueOf(categoryID));
        params.put("page", String.valueOf(page));
        params.put("limit", String.valueOf(ConstantStrings.EVENTS_PAGING));
        Observable<DataEntitiesWrapper<List<Place>>> observable =
                morePlacesApi.getPlacesByCategory(params).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());

        return observable;
    }


    public interface MorePlacesApi {

        @GET(ConstantStrings.SERVICE_PLACES_CATEGORY)
        Observable<DataEntitiesWrapper<List<Place>>> getPlacesByCategory(@QueryMap Map<String, String> params);
    }


}
