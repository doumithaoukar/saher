package com.acidwater.saher.Places.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.acidwater.saher.Places.model.Place;
import com.acidwater.saher.R;
import com.acidwater.saher.events.model.Event;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by D on 7/20/2015.
 */

public class PlaceAdapter extends RecyclerView.Adapter<PlaceAdapter.ViewHolder> {

    private List<Place> list;
    private Context context;
    private onItemClickListener onItemClickListener;

    public PlaceAdapter(Context context, List<Place> list) {
        this.list = list;
        this.context = context;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView title;
        public TextView teaser;
        public ImageView image;


        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            teaser = (TextView) itemLayoutView.findViewById(R.id.place_teaser);
            title = (TextView) itemLayoutView.findViewById(R.id.place_title);
            image = (ImageView) itemLayoutView.findViewById(R.id.place_image);

        }
    }

    public List<Place> getList() {
        return list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_place, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        Place event = list.get(position);
        if (event.getName() != null) {
            viewHolder.title.setText(event.getName());

        }
        if (event.getTeaser() != null) {
            viewHolder.teaser.setText(event.getAddress());

        }
        if (event.getThumbnail() != null) {
            Picasso.with(context).load(event.getThumbnail()).into(viewHolder.image);

        }

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClickListener != null) {
                    onItemClickListener.onClick(list.get(position), position);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface onItemClickListener {
        void onClick(Place place, int position);
    }

    public void setOnItemClickListener(PlaceAdapter.onItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }


}