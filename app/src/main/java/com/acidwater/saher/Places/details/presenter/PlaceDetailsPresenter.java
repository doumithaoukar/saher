package com.acidwater.saher.Places.details.presenter;

import android.content.Context;

import com.acidwater.saher.Places.details.PlaceDetailsMvp;
import com.acidwater.saher.Places.details.model.PlaceDetails;
import com.acidwater.saher.Places.details.model.PlaceDetailsInteractor;
import com.acidwater.saher.data.DataEntityWrapper;
import com.acidwater.saher.data.ServerResponse;
import com.acidwater.saher.events.details.EventDetailsMvp;
import com.acidwater.saher.events.details.model.EventDetails;
import com.acidwater.saher.events.details.model.EventDetailsInteractor;
import com.acidwater.saher.exceptions.SaherException;
import com.acidwater.saher.favorites.FavoritesMvp;
import com.acidwater.saher.favorites.model.FavoritesInteractor;
import com.acidwater.saher.profile.model.ProfileInteractor;
import com.acidwater.saher.profile.model.User;
import com.acidwater.saher.services.ServiceObserver;
import com.acidwater.saher.utils.ConstantStrings;
import com.acidwater.saher.utils.GeneralUtils;
import com.acidwater.saher.utils.RxUtils;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Doumith on 7/23/2017.
 */
public class PlaceDetailsPresenter extends MvpBasePresenter<PlaceDetailsMvp.View> implements PlaceDetailsMvp.Presenter {

    private PlaceDetailsInteractor placeDetailsInteractor;
    private CompositeDisposable compositeDisposable;
    private FavoritesMvp.Interactor favoritesInteractor;
    private ProfileInteractor profileInteractor;
    private Context context;

    public PlaceDetailsPresenter(Context context) {
        this.context = context;
        placeDetailsInteractor = new PlaceDetailsInteractor();
        compositeDisposable = new CompositeDisposable();
        this.favoritesInteractor = new FavoritesInteractor(context);
        this.profileInteractor = new ProfileInteractor(context);

    }


    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        if (!retainInstance) {
            RxUtils.disposeIfNotNull(compositeDisposable);
        }
    }

    @Override
    public void getPlaceDetails(int placeID) {
        if (isViewAttached()) {
            getView().showLoading(false);
        }

        compositeDisposable.add(placeDetailsInteractor.getPlaceDetails(placeID).subscribeWith(new ServiceObserver<DataEntityWrapper<PlaceDetails>>() {
            @Override
            public void onStart() {

            }

            @Override
            public void onComplete() {

                if (isViewAttached()) {
                    getView().showContent();
                }
            }

            @Override
            public void onNext(DataEntityWrapper<PlaceDetails> placeDetailsDataEntityWrapper) {
                if (isViewAttached()) {
                    getView().setData(placeDetailsDataEntityWrapper);
                }
            }

            @Override
            public void onError(SaherException e) {

                if (isViewAttached()) {
                    getView().showError(e, false);
                }
            }
        }));
    }

    @Override
    public void addToFavorite(int placeID) {
        compositeDisposable.add(favoritesInteractor.addToFavorite(placeID, ConstantStrings.FAVORITE_PUB).subscribeWith(new ServiceObserver<ServerResponse>() {
            @Override
            public void onStart() {
                if (isViewAttached()) {
                    getView().showLoadingView();
                }
            }

            @Override
            public void onComplete() {
                if (isViewAttached()) {
                    getView().hideLoadingView();

                }
            }

            @Override
            public void onNext(ServerResponse serverResponse) {
                compositeDisposable.add(profileInteractor.getProfile().subscribeWith(new ServiceObserver<DataEntityWrapper<User>>() {
                    @Override
                    public void onStart() {
                        if (isViewAttached()) {
                            getView().showLoadingView();
                        }
                    }

                    @Override
                    public void onComplete() {
                        if (isViewAttached()) {
                            getView().hideLoadingView();
                        }
                    }

                    @Override
                    public void onNext(DataEntityWrapper<User> userDataEntityWrapper) {
                        if (userDataEntityWrapper != null) {
                            GeneralUtils.saveUserProfile(context, userDataEntityWrapper.getData());
                        }
                        if (isViewAttached()) {
                            getView().refreshData();
                        }
                    }

                    @Override
                    public void onError(SaherException e) {
                        if (isViewAttached()) {
                            getView().hideLoadingView();
                            getView().showError(e, false);
                        }
                    }
                }));
            }

            @Override
            public void onError(SaherException e) {
                if (isViewAttached()) {
                    getView().hideLoadingView();
                    getView().showError(e, false);
                }
            }
        }));
    }

    @Override
    public void removeFromFavorite(int placeID) {
        compositeDisposable.add(favoritesInteractor.removeFromFavorite(placeID, ConstantStrings.FAVORITE_PUB).subscribeWith(new ServiceObserver<ServerResponse>() {
            @Override
            public void onStart() {

                if (isViewAttached()) {
                    getView().showLoadingView();
                }
            }

            @Override
            public void onComplete() {
                if (isViewAttached()) {
                    getView().hideLoadingView();

                }
            }

            @Override
            public void onNext(ServerResponse serverResponse) {
                compositeDisposable.add(profileInteractor.getProfile().subscribeWith(new ServiceObserver<DataEntityWrapper<User>>() {
                    @Override
                    public void onStart() {
                        if (isViewAttached()) {
                            getView().showLoadingView();
                        }
                    }

                    @Override
                    public void onComplete() {
                        if (isViewAttached()) {
                            getView().hideLoadingView();

                        }
                    }

                    @Override
                    public void onNext(DataEntityWrapper<User> userDataEntityWrapper) {
                        if (userDataEntityWrapper != null) {
                            GeneralUtils.saveUserProfile(context, userDataEntityWrapper.getData());
                        }
                        if (isViewAttached()) {
                            getView().refreshData();
                        }
                    }

                    @Override
                    public void onError(SaherException e) {
                        if (isViewAttached()) {
                            getView().hideLoadingView();
                            getView().showError(e, false);
                        }
                    }
                }));
            }

            @Override
            public void onError(SaherException e) {
                if (isViewAttached()) {
                    getView().hideLoadingView();
                    getView().showError(e, false);
                }
            }
        }));
    }

    @Override
    public void playVideo(String videoURL) {
        if (isViewAttached()) {
            getView().playVideo(videoURL);
        }
    }
}
