package com.acidwater.saher.Places.presenter;

import com.acidwater.saher.Places.PlacesMvp;
import com.acidwater.saher.Places.model.Place;
import com.acidwater.saher.Places.model.PlaceCategory;
import com.acidwater.saher.Places.model.PlacesInteractor;
import com.acidwater.saher.data.DataEntitiesWrapper;
import com.acidwater.saher.events.EventMvp;
import com.acidwater.saher.events.model.Event;
import com.acidwater.saher.events.model.EventInteractor;
import com.acidwater.saher.exceptions.SaherException;
import com.acidwater.saher.services.ServiceObserver;
import com.acidwater.saher.utils.RxUtils;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import java.util.List;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Doumith on 6/9/2017.
 */
public class PlacesPresenter extends MvpBasePresenter<PlacesMvp.View> implements PlacesMvp.Presenter {

    private PlacesInteractor placesInteractor;
    private CompositeDisposable compositeDisposable;


    public PlacesPresenter() {
        placesInteractor = new PlacesInteractor();
        compositeDisposable = new CompositeDisposable();
    }


    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        if (!retainInstance) {
            RxUtils.disposeIfNotNull(compositeDisposable);
        }
    }

    @Override
    public void getPlaces(int page) {
        if (isViewAttached()) {
            getView().showLoading(false);
        }

        compositeDisposable.add(placesInteractor.getPlaces(page).subscribeWith(new ServiceObserver<DataEntitiesWrapper<List<PlaceCategory>>>() {
            @Override
            public void onStart() {

            }

            @Override
            public void onComplete() {

                if (isViewAttached()) {
                    getView().showContent();
                }
            }

            @Override
            public void onNext(DataEntitiesWrapper<List<PlaceCategory>> listDataEntitiesWrapper) {
                if (isViewAttached()) {
                    getView().setData(listDataEntitiesWrapper);
                }
            }

            @Override
            public void onError(SaherException e) {

                if (isViewAttached()) {
                    getView().showError(e, false);
                }
            }
        }));
    }

    @Override
    public void openMorePlaces(String categoryName, int categoryID) {
        if (isViewAttached()) {

            getView().openMorePlaces(categoryName, categoryID);
        }
    }

    @Override
    public void loadMorePlaces(int page) {

        compositeDisposable.add(placesInteractor.getPlaces(page).subscribeWith(new ServiceObserver<DataEntitiesWrapper<List<PlaceCategory>>>() {
            @Override
            public void onStart() {

            }

            @Override
            public void onComplete() {

                if (isViewAttached()) {
                    getView().hideLoadMore();
                }
            }

            @Override
            public void onNext(DataEntitiesWrapper<List<PlaceCategory>> listDataEntitiesWrapper) {
                if (isViewAttached()) {
                    getView().setData(listDataEntitiesWrapper);
                }
            }

            @Override
            public void onError(SaherException e) {

                if (isViewAttached()) {
                    getView().hideLoadMore();
                }
            }
        }));
    }
}
