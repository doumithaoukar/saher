package com.acidwater.saher.Places.moreplaces;

import com.acidwater.saher.Places.model.Place;
import com.acidwater.saher.data.DataEntitiesWrapper;
import com.acidwater.saher.musicians.model.Musician;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by Doumith on 6/18/2017.
 */
public interface MorePlacesMvp {

    interface Interactor {


        Observable<DataEntitiesWrapper<List<Place>>> getPlacesByCategory(int page, int categoryID);

    }

    interface Presenter extends MvpPresenter<View> {


        void getPlacesByCategory(int page, int categoryID);
    }

    interface View extends MvpLceView<DataEntitiesWrapper<List<Place>>> {

        void setError(String message);
        void hideLoadMore();

    }
}
