package com.acidwater.saher.Places.moreplaces.view;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.acidwater.saher.Places.model.Place;
import com.acidwater.saher.Places.moreplaces.MorePlacesMvp;
import com.acidwater.saher.Places.moreplaces.presenter.MorePlacesPresenter;
import com.acidwater.saher.Places.view.PlaceAdapter;
import com.acidwater.saher.R;
import com.acidwater.saher.data.DataEntitiesWrapper;
import com.acidwater.saher.main.MainActivityListener;
import com.acidwater.saher.musicians.model.Musician;
import com.acidwater.saher.musicians.moremusicians.MoreMusicianMvp;
import com.acidwater.saher.musicians.moremusicians.presenter.MoreMusiciansPresenter;
import com.acidwater.saher.musicians.view.MusicianAdapter;
import com.acidwater.saher.utils.ConstantStrings;
import com.acidwater.saher.utils.mvp.LceRetryFragment;
import com.acidwater.saher.widgets.GridItemSpacesDecoration;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceFragment;
import com.jcodecraeer.xrecyclerview.XRecyclerView;

import java.util.List;

/**
 * Created by Doumith on 6/9/2017.
 */
public class MorePlacesFragment extends LceRetryFragment<View, DataEntitiesWrapper<List<Place>>, MorePlacesMvp.View, MorePlacesMvp.Presenter> implements MorePlacesMvp.View {

    private int page = 1;
    private XRecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private String categoryName;
    private int categoryID;
    private PlaceAdapter placeAdapter;
    private static final String CATEGORY_NAME = "categoryName";
    private static final String CATEGORY_ID = "categoryID";
    private MainActivityListener mListener;


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MainActivityListener) {
            mListener = (MainActivityListener) activity;
        } else {
            throw new RuntimeException("you should implemented MainActivityListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public static MorePlacesFragment newInstance(String categoryName, int categoryID) {
        MorePlacesFragment morePlacesFragment = new MorePlacesFragment();
        Bundle b = new Bundle();
        b.putString(CATEGORY_NAME, categoryName);
        b.putInt(CATEGORY_ID, categoryID);
        morePlacesFragment.setArguments(b);
        return morePlacesFragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (savedInstanceState != null) {
            categoryName = savedInstanceState.getString(CATEGORY_NAME);
            categoryID = savedInstanceState.getInt(CATEGORY_ID);
        } else {
            if (getArguments() != null) {
                categoryName = getArguments().getString(CATEGORY_NAME);
                categoryID = getArguments().getInt(CATEGORY_ID);
            }

        }
        if (categoryName != null && categoryID != 0) {
            loadData(false);
        }

    }

    @Override
    protected void customizeLoadingView(View loadingView) {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        page = 1;
        View containerView = inflater.inflate(R.layout.fragment_places, container, false);
        swipeRefreshLayout = (SwipeRefreshLayout) containerView.findViewById(R.id.swipe_refresh_layout);
        recyclerView = (XRecyclerView) containerView.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
        recyclerView.addItemDecoration(new GridItemSpacesDecoration(15));
        recyclerView.setPullRefreshEnabled(false);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                page = 1;
                presenter.getPlacesByCategory(page, categoryID);
            }
        });
        return containerView;

    }

    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return null;
    }


    @Override
    public void setData(DataEntitiesWrapper<List<Place>> data) {
        placeAdapter = new PlaceAdapter(getContext(), data.getData());
        recyclerView.setAdapter(placeAdapter);

        if (page == 1) {
            placeAdapter = new PlaceAdapter(getContext(), data.getData());
            recyclerView.setAdapter(placeAdapter);
            placeAdapter.setOnItemClickListener(new PlaceAdapter.onItemClickListener() {
                @Override
                public void onClick(Place place, int position) {
                    mListener.navigateToPlaceDetails(place.getId());
                }
            });
            recyclerView.setLoadingMoreEnabled(true);
            recyclerView.setLoadingListener(new XRecyclerView.LoadingListener() {
                @Override
                public void onRefresh() {
                    //refresh data here
                }

                @Override
                public void onLoadMore() {
                    // load more data here
                    page++;
                    presenter.getPlacesByCategory(page, categoryID);
                }
            });
        } else {
            placeAdapter.getList().addAll(data.getData());
            placeAdapter.notifyDataSetChanged();
            hideLoadMore();
            if (data.getData().size() < ConstantStrings.EVENTS_PAGING) {
                recyclerView.setLoadingMoreEnabled(false);
            }
        }

        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void loadData(boolean pullToRefresh) {
        presenter.getPlacesByCategory(page, categoryID);
    }

    @Override
    public MorePlacesMvp.Presenter createPresenter() {
        return new MorePlacesPresenter();
    }

    @Override
    public void retry() {
        page = 1;
        loadData(false);
    }


    @Override
    public void setError(String message) {

    }

    @Override
    public void hideLoadMore() {
        recyclerView.loadMoreComplete();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (categoryName != null) {
            outState.putString(CATEGORY_NAME, categoryName);
        }
        outState.putInt(CATEGORY_ID, categoryID);
    }
}
