package com.acidwater.saher.Places.details.model;

import android.os.Parcel;

import com.acidwater.saher.Places.model.Place;
import com.acidwater.saher.data.Category;
import com.acidwater.saher.data.Contact;
import com.acidwater.saher.events.model.Event;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Doumith on 7/23/2017.
 */
public class PlaceDetails extends Place {

    @SerializedName("categories")
    private ArrayList<Category> categories;

    @SerializedName("events")
    private ArrayList<Event> events;

    @SerializedName("longitude")
    private double longitude;

    @SerializedName("latitude")
    private double latitude;

    @SerializedName("contacts")
    private ArrayList<Contact> contacts;

    @SerializedName("youtube_url")
    private String videoURL;

    @SerializedName("facebook_url")
    private String facebookURL;

    @SerializedName("site_url")
    private String websiteURL;

    @SerializedName("description")
    private String description;

    @SerializedName("video")
    private String videoTrailer;

    @SerializedName("twitter_url")
    private String twitterURL;


    @SerializedName("youTubeChanel")
    private String youTubeChanel;


    @SerializedName("insta_url")
    private String instagramURL;


    public ArrayList<Category> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<Category> categories) {
        this.categories = categories;
    }

    public ArrayList<Event> getEvents() {
        return events;
    }

    public void setEvents(ArrayList<Event> events) {
        this.events = events;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }


    public ArrayList<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }

    public String getVideoURL() {
        return videoURL;
    }

    public void setVideoURL(String videoURL) {
        this.videoURL = videoURL;
    }

    public String getFacebookURL() {
        return facebookURL;
    }

    public void setFacebookURL(String facebookURL) {
        this.facebookURL = facebookURL;
    }

    public String getWebsiteURL() {
        return websiteURL;
    }

    public void setWebsiteURL(String websiteURL) {
        this.websiteURL = websiteURL;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVideoTrailer() {
        return videoTrailer;
    }

    public void setVideoTrailer(String videoTrailer) {
        this.videoTrailer = videoTrailer;
    }

    public String getTwitterURL() {
        return twitterURL;
    }

    public void setTwitterURL(String twitterURL) {
        this.twitterURL = twitterURL;
    }

    public String getYouTubeChanel() {
        return youTubeChanel;
    }

    public void setYouTubeChanel(String youTubeChanel) {
        this.youTubeChanel = youTubeChanel;
    }

    public String getInstagramURL() {
        return instagramURL;
    }

    public void setInstagramURL(String instagramURL) {
        this.instagramURL = instagramURL;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeTypedList(this.categories);
        dest.writeTypedList(this.events);
        dest.writeDouble(this.longitude);
        dest.writeDouble(this.latitude);
        dest.writeTypedList(this.contacts);
        dest.writeString(this.videoURL);
        dest.writeString(this.facebookURL);
        dest.writeString(this.websiteURL);
        dest.writeString(this.description);
        dest.writeString(this.videoTrailer);
        dest.writeString(this.twitterURL);
        dest.writeString(this.youTubeChanel);
        dest.writeString(this.instagramURL);
    }

    public PlaceDetails() {
    }

    protected PlaceDetails(Parcel in) {
        super(in);
        this.categories = in.createTypedArrayList(Category.CREATOR);
        this.events = in.createTypedArrayList(Event.CREATOR);
        this.longitude = in.readDouble();
        this.latitude = in.readDouble();
        this.contacts = in.createTypedArrayList(Contact.CREATOR);
        this.videoURL = in.readString();
        this.facebookURL = in.readString();
        this.websiteURL = in.readString();
        this.description = in.readString();
        this.videoTrailer = in.readString();
        this.twitterURL = in.readString();
        this.youTubeChanel = in.readString();
        this.instagramURL = in.readString();
    }

    public static final Creator<PlaceDetails> CREATOR = new Creator<PlaceDetails>() {
        @Override
        public PlaceDetails createFromParcel(Parcel source) {
            return new PlaceDetails(source);
        }

        @Override
        public PlaceDetails[] newArray(int size) {
            return new PlaceDetails[size];
        }
    };
}
