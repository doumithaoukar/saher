package com.acidwater.saher.Places.view;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.acidwater.saher.Places.model.Place;
import com.acidwater.saher.main.MainActivityListener;
import com.acidwater.saher.Places.PlacesMvp;
import com.acidwater.saher.Places.model.PlaceCategory;
import com.acidwater.saher.Places.presenter.PlacesPresenter;
import com.acidwater.saher.R;
import com.acidwater.saher.data.DataEntitiesWrapper;
import com.acidwater.saher.utils.ConstantStrings;
import com.acidwater.saher.utils.mvp.LceRetryFragment;
import com.acidwater.saher.widgets.SpacesItemDecoration;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceFragment;
import com.jcodecraeer.xrecyclerview.XRecyclerView;

import java.util.List;

/**
 * Created by Doumith on 6/9/2017.
 */
public class PlacesFragment extends LceRetryFragment<View, DataEntitiesWrapper<List<PlaceCategory>>, PlacesMvp.View, PlacesMvp.Presenter> implements PlacesMvp.View {

    private int page = 1;
    private XRecyclerView recyclerView;
    private MainActivityListener mListener;
    private SwipeRefreshLayout swipeRefreshLayout;
    private PlaceCategoryAdapter placeCategoryAdapter;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MainActivityListener) {
            mListener = (MainActivityListener) activity;
        } else {
            throw new RuntimeException("you should implemented MainActivityListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public static PlacesFragment newInstance() {
        PlacesFragment placesFragment = new PlacesFragment();
        return placesFragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadData(false);
    }

    @Override
    protected void customizeLoadingView(View loadingView) {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View containerView = inflater.inflate(R.layout.fragment_places, container, false);
        // reset all data after fragment is destroyed
        page = 1;
        swipeRefreshLayout = (SwipeRefreshLayout) containerView.findViewById(R.id.swipe_refresh_layout);
        recyclerView = (XRecyclerView) containerView.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.addItemDecoration(new SpacesItemDecoration(15));
        recyclerView.setPullRefreshEnabled(false);



        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                page = 1;
                presenter.getPlaces(page);
            }
        });
        return containerView;

    }

    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return null;
    }

    @Override
    public void setData(DataEntitiesWrapper<List<PlaceCategory>> data) {
        if (page == 1) {
            placeCategoryAdapter = new PlaceCategoryAdapter(getContext(), data.getData(), presenter);
            recyclerView.setAdapter(placeCategoryAdapter);
            placeCategoryAdapter.setOnItemClickListener(new PlaceCategoryAdapter.onItemClickListener() {
                @Override
                public void onClick(Place place, int position) {
                    mListener.navigateToPlaceDetails(place.getId());
                }
            });
            recyclerView.setLoadingMoreEnabled(true);
            recyclerView.setLoadingListener(new XRecyclerView.LoadingListener() {
                @Override
                public void onRefresh() {
                    //refresh data here
                }

                @Override
                public void onLoadMore() {
                    // load more data here
                    page++;
                    presenter.loadMorePlaces(page);
                }
            });
        } else {
            placeCategoryAdapter.getList().addAll(data.getData());
            placeCategoryAdapter.notifyDataSetChanged();
            hideLoadMore();
            if (data.getData().size() < ConstantStrings.PLACES_PAGING) {
                recyclerView.setLoadingMoreEnabled(false);
            }
        }

        swipeRefreshLayout.setRefreshing(false);

    }

    @Override
    public void loadData(boolean pullToRefresh) {
        presenter.getPlaces(page);
    }

    @Override
    public PlacesMvp.Presenter createPresenter() {
        return new PlacesPresenter();
    }

    @Override
    public void retry() {
        loadData(false);
    }


    @Override
    public void setError(String message) {

    }

    @Override
    public void openMorePlaces(String categoryName, int categoryID) {
        mListener.navigateToMorePlaces(categoryName, categoryID);
    }

    @Override
    public void hideLoadMore() {
        recyclerView.loadMoreComplete();
    }
}
