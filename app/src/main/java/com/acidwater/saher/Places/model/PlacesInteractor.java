package com.acidwater.saher.Places.model;

import com.acidwater.saher.Places.PlacesMvp;
import com.acidwater.saher.data.DataEntitiesWrapper;
import com.acidwater.saher.events.EventMvp;
import com.acidwater.saher.events.model.Event;
import com.acidwater.saher.utils.ConstantStrings;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

/**
 * Created by Doumith on 6/18/2017.
 */
public class PlacesInteractor implements PlacesMvp.Interactor {


    private PlacesApi placesApi;

    public PlacesInteractor() {
        String baseURL = ConstantStrings.BASE_URL;
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder().build();
        Retrofit.Builder retrofitBuilder = new Retrofit.Builder().baseUrl(baseURL.endsWith("/") ? baseURL : baseURL.concat("/"))
                .addConverterFactory(GsonConverterFactory.create()).addCallAdapterFactory(RxJava2CallAdapterFactory.create());
        retrofitBuilder.client(okHttpClient);
        Retrofit retrofit = retrofitBuilder.build();
        placesApi = retrofit.create(PlacesApi.class);
    }

    @Override
    public Observable<DataEntitiesWrapper<List<PlaceCategory>>> getPlaces(int page) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("page", String.valueOf(page));
        params.put("limit", String.valueOf(ConstantStrings.PLACES_PAGING));
        Observable<DataEntitiesWrapper<List<PlaceCategory>>> observable =
                placesApi.getPlaces(params).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());

        return observable;
    }

    public interface PlacesApi {
        @GET(ConstantStrings.SERVICE_PLACES)
        Observable<DataEntitiesWrapper<List<PlaceCategory>>> getPlaces(@QueryMap Map<String, String> params);
    }


}
