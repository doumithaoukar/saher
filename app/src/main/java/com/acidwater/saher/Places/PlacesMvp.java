package com.acidwater.saher.Places;

import com.acidwater.saher.Places.model.Place;
import com.acidwater.saher.Places.model.PlaceCategory;
import com.acidwater.saher.data.DataEntitiesWrapper;
import com.acidwater.saher.events.model.Event;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by Doumith on 6/18/2017.
 */
public interface PlacesMvp {


    interface Interactor {

        Observable<DataEntitiesWrapper<List<PlaceCategory>>> getPlaces(int page);

    }

    interface Presenter extends MvpPresenter<View> {

        void getPlaces(int page);

        void openMorePlaces(String categoryName, int categoryID);

        void loadMorePlaces(int page);
    }

    interface View extends MvpLceView<DataEntitiesWrapper<List<PlaceCategory>>> {

        void setError(String message);

        void openMorePlaces(String categoryName, int categoryID);

        void hideLoadMore();


    }
}
