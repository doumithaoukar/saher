package com.acidwater.saher.Places.details;

import com.acidwater.saher.Places.details.model.PlaceDetails;
import com.acidwater.saher.data.DataEntityWrapper;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

import io.reactivex.Observable;

/**
 * Created by Doumith on 6/18/2017.
 */
public interface PlaceDetailsMvp {

    interface Interactor {

        Observable<DataEntityWrapper<PlaceDetails>> getPlaceDetails(int placeID);

    }

    interface Presenter extends MvpPresenter<View> {

        void getPlaceDetails(int placeID);

        void addToFavorite(int placeID);

        void removeFromFavorite(int placeID);

        void playVideo(String videoURL);

    }

    interface View extends MvpLceView<DataEntityWrapper<PlaceDetails>> {

        void setError(String message);

        void showLoadingView();

        void hideLoadingView();

        void refreshData();

        void playVideo(String videoURL);

    }
}
