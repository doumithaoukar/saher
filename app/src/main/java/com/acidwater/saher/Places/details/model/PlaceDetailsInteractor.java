package com.acidwater.saher.Places.details.model;
import com.acidwater.saher.Places.details.PlaceDetailsMvp;
import com.acidwater.saher.data.DataEntityWrapper;
import com.acidwater.saher.utils.ConstantStrings;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by Doumith on 6/18/2017.
 */
public class PlaceDetailsInteractor implements PlaceDetailsMvp.Interactor {


    private PlaceDetailsApi placeDetailsApi;

    public PlaceDetailsInteractor() {
        String baseURL = ConstantStrings.BASE_URL;
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder().build();
        Retrofit.Builder retrofitBuilder = new Retrofit.Builder().baseUrl(baseURL.endsWith("/") ? baseURL : baseURL.concat("/"))
                .addConverterFactory(GsonConverterFactory.create()).addCallAdapterFactory(RxJava2CallAdapterFactory.create());
        retrofitBuilder.client(okHttpClient);
        Retrofit retrofit = retrofitBuilder.build();
        placeDetailsApi = retrofit.create(PlaceDetailsApi.class);
    }


    @Override
    public Observable<DataEntityWrapper<PlaceDetails>> getPlaceDetails(int placeID) {
        String URL = ConstantStrings.BASE_URL.endsWith("/") ? ConstantStrings.BASE_URL : ConstantStrings.BASE_URL.concat("/");
        URL += ConstantStrings.SERVICE_PLACE_DETAILS + "/" + String.valueOf(placeID);

        Observable<DataEntityWrapper<PlaceDetails>> observable =
                placeDetailsApi.getPlaceDetails(String.valueOf(URL)).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());

        return observable;
    }


    public interface PlaceDetailsApi {
        @GET
        Observable<DataEntityWrapper<PlaceDetails>> getPlaceDetails(@Url String url);
    }


}
