package com.acidwater.saher.Places.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.acidwater.saher.data.ListItem;
import com.acidwater.saher.musicians.model.Musician;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Doumith on 7/15/2017.
 */
public class Place extends ListItem implements Parcelable {


    @SerializedName("opening_time")
    private String openingTime;

    private String city;

    private String  price;

    private String address;

    public String getOpeningTime() {
        return openingTime;
    }

    public void setOpeningTime(String openingTime) {
        this.openingTime = openingTime;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Place() {
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(this.openingTime);
        dest.writeString(this.city);
        dest.writeString(this.price);
        dest.writeString(this.address);
    }

    protected Place(Parcel in) {
        super(in);
        this.openingTime = in.readString();
        this.city = in.readString();
        this.price = in.readString();
        this.address = in.readString();
    }

    public static final Creator<Place> CREATOR = new Creator<Place>() {
        @Override
        public Place createFromParcel(Parcel source) {
            return new Place(source);
        }

        @Override
        public Place[] newArray(int size) {
            return new Place[size];
        }
    };
}
