package com.acidwater.saher.Places.details.view;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;

import com.acidwater.saher.Places.details.PlaceDetailsMvp;
import com.acidwater.saher.Places.details.model.PlaceDetails;
import com.acidwater.saher.Places.details.presenter.PlaceDetailsPresenter;
import com.acidwater.saher.R;
import com.acidwater.saher.data.Contact;
import com.acidwater.saher.data.DataEntityWrapper;
import com.acidwater.saher.events.model.Event;
import com.acidwater.saher.main.MainActivityListener;
import com.acidwater.saher.utils.GeneralUtils;
import com.acidwater.saher.utils.ScrollAwareFABBehavior;
import com.acidwater.saher.widgets.SpacesItemDecoration;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceFragment;

import java.util.ArrayList;

/**
 * Created by Doumith on 6/9/2017.
 */
public class PlaceDetailsFragment extends MvpLceFragment<View, DataEntityWrapper<PlaceDetails>, PlaceDetailsMvp.View, PlaceDetailsMvp.Presenter> implements PlaceDetailsMvp.View, View.OnClickListener {

    private RecyclerView recyclerView;
    private MainActivityListener mListener;
    private int placeID;
    private static final String PLACE_ID = "place_id";
    private FloatingActionButton floatingActionButton;
    private RelativeLayout progressBar;
    private PlaceDetailsAdapter placeDetailsAdapter;
    private PlaceDetails mPlaceDetails;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MainActivityListener) {
            mListener = (MainActivityListener) activity;
        } else {
            throw new RuntimeException("you should implemented MainActivityListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public static PlaceDetailsFragment newInstance(int placeID) {
        PlaceDetailsFragment placeDetailsFragment = new PlaceDetailsFragment();
        Bundle b = new Bundle();
        b.putInt(PLACE_ID, placeID);
        placeDetailsFragment.setArguments(b);
        return placeDetailsFragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (savedInstanceState != null) {
            placeID = savedInstanceState.getInt(PLACE_ID);
        } else {
            if (getArguments() != null) {
                placeID = getArguments().getInt(PLACE_ID);
            }
        }
        if (placeID != 0) {
            loadData(false);
        }
    }

    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View containerView = inflater.inflate(R.layout.fragment_details, container, false);
        recyclerView = (RecyclerView) containerView.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.addItemDecoration(new SpacesItemDecoration(15));
        floatingActionButton = (FloatingActionButton) containerView.findViewById(R.id.floatingActionButton);
        progressBar = (RelativeLayout) containerView.findViewById(R.id.main_progress);
        floatingActionButton.setOnClickListener(this);
        ScrollAwareFABBehavior scrollAwareFABBehavior = new ScrollAwareFABBehavior();
        CoordinatorLayout.LayoutParams floatingActionButtonParams = (CoordinatorLayout.LayoutParams) floatingActionButton.getLayoutParams();
        floatingActionButtonParams.setBehavior(scrollAwareFABBehavior);
        floatingActionButton.setLayoutParams(floatingActionButtonParams);
        floatingActionButton.show();
        return containerView;

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(PLACE_ID, placeID);
    }

    @Override
    public void setData(DataEntityWrapper<PlaceDetails> data) {
        this.mPlaceDetails = data.getData();
        placeDetailsAdapter = new PlaceDetailsAdapter(getContext(), data.getData(), presenter);
        recyclerView.setAdapter(placeDetailsAdapter);
        placeDetailsAdapter.setOnItemClickListener(new PlaceDetailsAdapter.onItemClickListener() {
            @Override
            public void onClick(Event event, int position) {
                mListener.navigateToEventDetails(event.getId());
            }
        });

        if (mPlaceDetails.getContacts() == null || mPlaceDetails.getContacts().isEmpty()) {
            floatingActionButton.setVisibility(View.GONE);
        } else {
            floatingActionButton.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void loadData(boolean pullToRefresh) {
        presenter.getPlaceDetails(placeID);
    }

    @Override
    public PlaceDetailsMvp.Presenter createPresenter() {
        return new PlaceDetailsPresenter(getContext());
    }


    @Override
    public void setError(String message) {

    }

    @Override
    public void showLoadingView() {
        progressBar.setVisibility(View.VISIBLE);

    }

    @Override
    public void hideLoadingView() {
        progressBar.setVisibility(View.GONE);

    }

    @Override
    public void refreshData() {
        if (placeDetailsAdapter != null) {
            placeDetailsAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void playVideo(String videoURL) {
        if (!TextUtils.isEmpty(videoURL)) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(videoURL)));
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.floatingActionButton:
                if (mPlaceDetails != null && mPlaceDetails.getContacts() != null) {
                    showContactListDialog(mPlaceDetails.getContacts());
                }
                break;
        }
    }

    private void showContactListDialog(ArrayList<Contact> contacts) {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(getActivity());
        builderSingle.setTitle(getString(R.string.select_contact));

        final ArrayAdapter<Contact> arrayAdapter = new ArrayAdapter<Contact>(getContext(), R.layout.row_contact_dialog);
        for (Contact contact : contacts) {
            arrayAdapter.add(contact);
        }
        builderSingle.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Contact contact = arrayAdapter.getItem(which);
                GeneralUtils.callNumber(getContext(), contact.getPhone());
            }
        });
        builderSingle.show();
    }
}
