package com.acidwater.saher.Places.view;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.acidwater.saher.Places.PlacesMvp;
import com.acidwater.saher.Places.model.Place;
import com.acidwater.saher.Places.model.PlaceCategory;
import com.acidwater.saher.R;
import com.acidwater.saher.widgets.GridItemSpacesDecoration;

import java.util.List;

/**
 * Created by D on 7/20/2015.
 */

public class PlaceCategoryAdapter extends RecyclerView.Adapter<PlaceCategoryAdapter.ViewHolder> {

    private List<PlaceCategory> list;
    private Context context;
    private onItemClickListener onItemClickListener;
    private PlacesMvp.Presenter presenter;

    public PlaceCategoryAdapter(Context context, List<PlaceCategory> list, PlacesMvp.Presenter presenter) {
        this.list = list;
        this.context = context;
        this.presenter = presenter;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView title;
        public RecyclerView recyclerView;
        public LinearLayout btnMore;


        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            title = (TextView) itemLayoutView.findViewById(R.id.place_category_title);
            recyclerView = (RecyclerView) itemLayoutView.findViewById(R.id.recyclerView);
            btnMore = (LinearLayout) itemLayoutView.findViewById(R.id.btn_more);

        }
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_place_category, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        viewHolder.title.setText(list.get(position).getName());
        PlaceAdapter placeAdapter = new PlaceAdapter(context, list.get(position).getPlaces());
        placeAdapter.setOnItemClickListener(new PlaceAdapter.onItemClickListener() {
            @Override
            public void onClick(Place place, int position) {
                if (onItemClickListener != null) {
                    onItemClickListener.onClick(place, position);
                }
            }
        });
        viewHolder.recyclerView.setLayoutManager(new GridLayoutManager(context, 2));
        viewHolder.recyclerView.addItemDecoration(new GridItemSpacesDecoration(15));
        viewHolder.recyclerView.setNestedScrollingEnabled(false);
        viewHolder.recyclerView.setAdapter(placeAdapter);
        viewHolder.btnMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.openMorePlaces(list.get(position).getName(), list.get(position).getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface onItemClickListener {
        void onClick(Place place, int position);
    }

    public void setOnItemClickListener(PlaceCategoryAdapter.onItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public List<PlaceCategory> getList() {
        return list;
    }

    public void setList(List<PlaceCategory> list) {
        this.list = list;
    }
}