package com.acidwater.saher.Places.moreplaces.presenter;

import com.acidwater.saher.Places.model.Place;
import com.acidwater.saher.Places.moreplaces.MorePlacesMvp;
import com.acidwater.saher.Places.moreplaces.model.MorePlacesInteractor;
import com.acidwater.saher.data.DataEntitiesWrapper;
import com.acidwater.saher.exceptions.SaherException;
import com.acidwater.saher.musicians.model.Musician;
import com.acidwater.saher.musicians.moremusicians.MoreMusicianMvp;
import com.acidwater.saher.musicians.moremusicians.model.MoreMusiciansInteractor;
import com.acidwater.saher.services.ServiceObserver;
import com.acidwater.saher.utils.RxUtils;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import java.util.List;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Doumith on 6/9/2017.
 */
public class MorePlacesPresenter extends MvpBasePresenter<MorePlacesMvp.View> implements MorePlacesMvp.Presenter {

    private MorePlacesInteractor morePlacesInteractor;
    private CompositeDisposable compositeDisposable;

    public MorePlacesPresenter() {
        morePlacesInteractor = new MorePlacesInteractor();
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        if (!retainInstance) {
            RxUtils.disposeIfNotNull(compositeDisposable);
        }
    }

    @Override
    public void getPlacesByCategory(int page, int categoryID) {

        if (isViewAttached()) {
            getView().showLoading(false);
        }

        compositeDisposable.add(morePlacesInteractor.getPlacesByCategory(page, categoryID).subscribeWith(new ServiceObserver<DataEntitiesWrapper<List<Place>>>() {
            @Override
            public void onStart() {

            }

            @Override
            public void onComplete() {

                if (isViewAttached()) {
                    getView().showContent();
                }
            }

            @Override
            public void onNext(DataEntitiesWrapper<List<Place>> listDataEntitiesWrapper) {
                if (isViewAttached()) {
                    getView().setData(listDataEntitiesWrapper);
                }
            }

            @Override
            public void onError(SaherException e) {

                if (isViewAttached()) {
                    getView().showError(e, false);
                }
            }
        }));
    }
}
