package com.acidwater.saher.Places.details.view;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.acidwater.saher.Places.details.PlaceDetailsMvp;
import com.acidwater.saher.Places.details.model.PlaceDetails;
import com.acidwater.saher.R;
import com.acidwater.saher.events.details.EventDetailsMvp;
import com.acidwater.saher.events.details.model.EventDetails;
import com.acidwater.saher.events.model.Event;
import com.acidwater.saher.events.view.EventAdapter;
import com.acidwater.saher.favorites.FavoritesMvp;
import com.acidwater.saher.favorites.presenter.FavoritesPresenter;
import com.acidwater.saher.utils.ConstantStrings;
import com.acidwater.saher.utils.DateUtils;
import com.acidwater.saher.utils.GeneralUtils;
import com.acidwater.saher.viewholders.AddressViewHolder;
import com.acidwater.saher.viewholders.DateViewHolder;
import com.acidwater.saher.viewholders.DescriptionViewHolder;
import com.acidwater.saher.viewholders.EmailViewHolder;
import com.acidwater.saher.viewholders.EventCoverViewHolder;
import com.acidwater.saher.viewholders.MobileViewHolder;
import com.acidwater.saher.viewholders.MusicianEventsViewHolder;
import com.acidwater.saher.viewholders.MusicianViewHolder;
import com.acidwater.saher.viewholders.PlaceCoverViewHolder;
import com.acidwater.saher.viewholders.PlaceEventsViewHolder;
import com.acidwater.saher.viewholders.TeaserViewHolder;
import com.squareup.picasso.Picasso;

/**
 * Created by Doumith on 7/25/2017.
 */
public class PlaceDetailsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private PlaceDetails placeDetails;
    private PlaceDetailsMvp.Presenter presenter;


    private final int ROW_COVER = 0;
    private final int ROW_DESCRIPTION = 1;
    private final int ROW_NUMBER = 2;
    private final int ROW_EMAIL = 3;
    private final int ROW_ADDRESS = 4;
    private final int ROW_TEASER = 5;
    private final int ROW_EVENTS = 6;
    private final int ITEM_COUNT = 7;

    LayoutInflater inflater;
    private onItemClickListener onItemClickListener;
    private FavoritesMvp.Presenter favoritePresenter;


    public PlaceDetailsAdapter(Context context, PlaceDetails placeDetails, PlaceDetailsMvp.Presenter presenter) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.placeDetails = placeDetails;
        this.presenter = presenter;
        this.favoritePresenter = new FavoritesPresenter(context);

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;

        switch (viewType) {
            case ROW_COVER:
                viewHolder = new PlaceCoverViewHolder(
                        inflater.inflate(R.layout.row_details_place_cover, parent, false));
                return viewHolder;

            case ROW_TEASER:
                viewHolder = new TeaserViewHolder(
                        inflater.inflate(R.layout.row_details_teaser, parent, false));
                return viewHolder;


            case ROW_DESCRIPTION:
                viewHolder = new DescriptionViewHolder(
                        inflater.inflate(R.layout.row_details_description, parent, false));
                return viewHolder;

            case ROW_NUMBER:
                viewHolder = new MobileViewHolder(
                        inflater.inflate(R.layout.row_details_mobile_number, parent, false));
                return viewHolder;

            case ROW_EMAIL:
                viewHolder = new EmailViewHolder(
                        inflater.inflate(R.layout.row_details_email, parent, false));
                return viewHolder;

            case ROW_ADDRESS:
                viewHolder = new AddressViewHolder(
                        inflater.inflate(R.layout.row_details_address, parent, false));
                return viewHolder;

            case ROW_EVENTS:
                viewHolder = new PlaceEventsViewHolder(
                        inflater.inflate(R.layout.row_place_events, parent, false));
                return viewHolder;


        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        switch (holder.getItemViewType()) {
            case ROW_COVER:
                final PlaceCoverViewHolder placeCoverViewHolder = (PlaceCoverViewHolder) holder;
                Picasso.with(context).load(placeDetails.getThumbnail()).into(placeCoverViewHolder.cover);
                placeCoverViewHolder.title.setText(placeDetails.getName());
                placeCoverViewHolder.subtitle.setText(placeDetails.getAddress());

                final boolean isFavorite = favoritePresenter.isFavorite(placeDetails.getId(), ConstantStrings.FAVORITE_PUB);
                if (isFavorite) {
                    placeCoverViewHolder.btnFollow.setImageResource(R.drawable.btn_favorite_on);

                } else {
                    placeCoverViewHolder.btnFollow.setImageResource(R.drawable.btn_follow_on);

                }
                placeCoverViewHolder.btnFollow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (isFavorite) {
                            presenter.removeFromFavorite(placeDetails.getId());

                        } else {
                            presenter.addToFavorite(placeDetails.getId());
                        }
                    }
                });


                placeCoverViewHolder.btnPlayVideo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        presenter.playVideo(placeDetails.getVideoURL());
                    }
                });

                break;

            case ROW_TEASER:
                TeaserViewHolder teaserViewHolder = (TeaserViewHolder) holder;
                teaserViewHolder.teaser.setText(placeDetails.getTeaser());

                break;

            case ROW_DESCRIPTION:
                DescriptionViewHolder descriptionViewHolder = (DescriptionViewHolder) holder;
                if (!TextUtils.isEmpty(placeDetails.getDescription())) {
                    descriptionViewHolder.description.setText(Html.fromHtml(placeDetails.getDescription()));
                }

                break;
            case ROW_NUMBER:
                MobileViewHolder mobileViewHolder = (MobileViewHolder) holder;
                if (placeDetails.getContacts() != null && !placeDetails.getContacts().isEmpty()) {
                    mobileViewHolder.mobileNumber.setText(placeDetails.getContacts().get(0).getPhone());
                } else {
                    mobileViewHolder.mobileNumber.setText(context.getString(R.string.not_applicable));
                }

                break;
            case ROW_EMAIL:
                EmailViewHolder emailViewHolder = (EmailViewHolder) holder;

                if (placeDetails.getContacts() != null && !placeDetails.getContacts().isEmpty()) {
                    emailViewHolder.email.setText(placeDetails.getContacts().get(0).getEmail());
                } else {
                    emailViewHolder.email.setText(context.getString(R.string.not_applicable));
                }


                emailViewHolder.facebook.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (!TextUtils.isEmpty(placeDetails.getFacebookURL())) {
                            GeneralUtils.openIntent(context, placeDetails.getFacebookURL());
                        }
                    }
                });

                emailViewHolder.instagram.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (!TextUtils.isEmpty(placeDetails.getInstagramURL())) {
                            GeneralUtils.openIntent(context, placeDetails.getInstagramURL());
                        }
                    }
                });

                emailViewHolder.youtube.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (!TextUtils.isEmpty(placeDetails.getYouTubeChanel())) {
                            GeneralUtils.openIntent(context, placeDetails.getYouTubeChanel());
                        }
                    }
                });

                break;
            case ROW_ADDRESS:
                AddressViewHolder addressViewHolder = (AddressViewHolder) holder;
                if (!TextUtils.isEmpty(placeDetails.getAddress())) {
                    addressViewHolder.address.setText(placeDetails.getAddress());

                } else {
                    addressViewHolder.address.setText(context.getString(R.string.not_applicable));
                }
                break;

            case ROW_EVENTS:
                PlaceEventsViewHolder placeEventsViewHolder = (PlaceEventsViewHolder) holder;
                if (placeDetails.getEvents() != null && !placeDetails.getEvents().isEmpty()) {
                    placeEventsViewHolder.title.setVisibility(View.VISIBLE);
                    placeEventsViewHolder.recyclerView.setLayoutManager(new LinearLayoutManager(context));
                    EventAdapter eventAdapter = new EventAdapter(context, placeDetails.getEvents());
                    placeEventsViewHolder.recyclerView.setAdapter(eventAdapter);
                    eventAdapter.setOnItemClickListener(new EventAdapter.onItemClickListener() {
                        @Override
                        public void onClick(Event event, int position) {
                            if (onItemClickListener != null) {
                                onItemClickListener.onClick(event, position);
                            }
                        }
                    });
                } else {
                    placeEventsViewHolder.title.setVisibility(View.GONE);

                }


                break;

        }

    }

    @Override
    public int getItemViewType(int position) {
        switch (position) {
            case 0:
                return ROW_COVER;

            case 1:
                return ROW_TEASER;


            case 2:
                return ROW_DESCRIPTION;

            case 3:
                return ROW_NUMBER;

            case 4:
                return ROW_EMAIL;

            case 5:
                return ROW_ADDRESS;

            case 6:
                return ROW_EVENTS;

        }

        return -1;
    }

    public void setOnItemClickListener(PlaceDetailsAdapter.onItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public interface onItemClickListener {
        void onClick(Event event, int position);
    }

    @Override
    public int getItemCount() {
        return ITEM_COUNT;
    }

}

