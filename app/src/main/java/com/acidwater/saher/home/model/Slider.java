package com.acidwater.saher.home.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Doumith on 6/9/2017.
 */
public class Slider implements Parcelable {

    private int id;
    private String title;
    private List<SliderPhoto> photos;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<SliderPhoto> getPhotos() {
        return photos;
    }

    public void setPhotos(List<SliderPhoto> photos) {
        this.photos = photos;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.title);
        dest.writeTypedList(this.photos);
    }

    public Slider() {
    }

    protected Slider(Parcel in) {
        this.id = in.readInt();
        this.title = in.readString();
        this.photos = in.createTypedArrayList(SliderPhoto.CREATOR);
    }

    public static final Parcelable.Creator<Slider> CREATOR = new Parcelable.Creator<Slider>() {
        @Override
        public Slider createFromParcel(Parcel source) {
            return new Slider(source);
        }

        @Override
        public Slider[] newArray(int size) {
            return new Slider[size];
        }
    };
}
