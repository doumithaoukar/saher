package com.acidwater.saher.home.view;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.acidwater.saher.R;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;


public class SliderItem extends BaseSliderView {

    public SliderItem(Context context) {
        super(context);
    }

    @Override
    public View getView() {
        View v = LayoutInflater.from(getContext()).inflate(R.layout.slider_item, null);
        ImageView target = (ImageView) v.findViewById(R.id.daimajia_slider_image);

        //Set background color of description text view to transparent
        LinearLayout frame = (LinearLayout) v.findViewById(R.id.description_layout);
        frame.setBackgroundColor(Color.TRANSPARENT);

        TextView description = (TextView) v.findViewById(R.id.description);
        description.setText(getDescription());
        bindEventAndShow(v, target);
        return v;
    }
}
