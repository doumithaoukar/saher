package com.acidwater.saher.home.presenter;

import android.content.Context;
import android.support.v4.app.Fragment;

import com.acidwater.saher.Places.view.PlacesFragment;
import com.acidwater.saher.R;
import com.acidwater.saher.data.DataEntityWrapper;
import com.acidwater.saher.events.view.EventsFragment;
import com.acidwater.saher.exceptions.SaherException;
import com.acidwater.saher.home.HomeMvp;
import com.acidwater.saher.home.model.HomeInteractor;
import com.acidwater.saher.home.model.Slider;
import com.acidwater.saher.musicians.view.ArtistsFragment;
import com.acidwater.saher.musicians.view.BandsFragment;
import com.acidwater.saher.services.ServiceObserver;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Doumith on 6/9/2017.
 */
public class HomePresenter extends MvpBasePresenter<HomeMvp.View> implements HomeMvp.Presenter {

    private Context context;
    private HomeInteractor homeInteractor;
    private CompositeDisposable compositeDisposable;

    public HomePresenter(Context context) {
        this.context = context;
        compositeDisposable = new CompositeDisposable();
        homeInteractor = new HomeInteractor();
    }

    @Override
    public void getSliders() {

        compositeDisposable.add(homeInteractor.getSliders().subscribeWith(new ServiceObserver<DataEntityWrapper<List<Slider>>>() {
            @Override
            public void onStart() {

                if (isViewAttached()) {
                    getView().showLoading(false);
                }
            }

            @Override
            public void onComplete() {
                if (isViewAttached()) {
                    getView().showContent();
                }
            }

            @Override
            public void onNext(DataEntityWrapper<List<Slider>> listDataEntityWrapper) {

                if (isViewAttached()) {
                    getView().setData(listDataEntityWrapper);
                }
            }

            @Override
            public void onError(SaherException e) {

                if (isViewAttached()) {
                    getView().showError(e, false);
                }
            }
        }));
    }

    @Override
    public void getMainTabs() {

        List<String> sections = new ArrayList<>();
        List<Fragment> fragments = new ArrayList<>();

        sections.add(context.getString(R.string.Events));
        sections.add(context.getString(R.string.Places));
        sections.add(context.getString(R.string.Artists));
        sections.add(context.getString(R.string.Bands));

        fragments.add(EventsFragment.newInstance());
        fragments.add(PlacesFragment.newInstance());
        fragments.add(ArtistsFragment.newInstance());
        fragments.add(BandsFragment.newInstance());

        getView().renderMainTabs(fragments, sections);

    }

}
