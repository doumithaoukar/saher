package com.acidwater.saher.home.view;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.acidwater.saher.main.MainActivityListener;
import com.acidwater.saher.R;
import com.acidwater.saher.data.DataEntityWrapper;
import com.acidwater.saher.home.HomeMvp;
import com.acidwater.saher.home.model.Slider;
import com.acidwater.saher.home.model.SliderPhoto;
import com.acidwater.saher.utils.BasePagerAdapter;
import com.acidwater.saher.utils.GeneralUtils;
import com.acidwater.saher.utils.NonSwipeableViewPager;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.acidwater.saher.home.presenter.HomePresenter;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceFragment;

import java.util.List;

/**
 * Created by Doumith on 6/9/2017.
 */
public class HomeFragment extends MvpLceFragment<View, DataEntityWrapper<List<Slider>>, HomeMvp.View, HomeMvp.Presenter> implements HomeMvp.View {

    private TabLayout tabs;
    private NonSwipeableViewPager pager;
    private View rootView;
    private SliderLayout sliderLayout;
    private final long SLIDER_DURATION = 5000;
    private MainActivityListener mListener;
    private Handler mHandler = new Handler();
    private Runnable mRunnable;

    @Override
    public HomeMvp.Presenter createPresenter() {
        return new HomePresenter(getContext());
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MainActivityListener) {
            mListener = (MainActivityListener) activity;
        } else {
            throw new RuntimeException("you should implemented MainActivityListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        presenter.getMainTabs();
        presenter.getSliders();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rootView = LayoutInflater.from(getActivity().getBaseContext()).inflate(R.layout.fragment_home, null);
        sliderLayout = (SliderLayout) rootView.findViewById(R.id.slider);
        tabs = (TabLayout) rootView.findViewById(R.id.tabs);
        pager = (NonSwipeableViewPager) rootView.findViewById(R.id.pager);
        return rootView;
    }


    @Override
    public void renderMainTabs(List<Fragment> fragments, List<String> sections) {
        BasePagerAdapter adapter = new BasePagerAdapter(getChildFragmentManager());
        for (int i = 0; i < fragments.size(); i++) {
            adapter.addFragment(fragments.get(i), sections.get(i));
        }
        pager.setAdapter(adapter);
        tabs.setupWithViewPager(pager);
//        mRunnable = new Runnable() {
//            @Override
//            public void run() {
//                fitTabLayout();
//            }
//        };
//        mHandler.postDelayed(mRunnable, 1000);

    }

    @Override
    public void onResume() {
        super.onResume();
        sliderLayout.setDuration(SLIDER_DURATION);
        sliderLayout.startAutoCycle();

    }

    @Override
    public void onStop() {
        super.onStop();
        sliderLayout.stopAutoCycle();
    }

    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return null;
    }

    @Override
    public void setData(DataEntityWrapper<List<Slider>> data) {

        if (data.getData() != null && data.getData().size() > 0) {
            //get home slider

            Slider home = data.getData().get(0);
            sliderLayout.setVisibility(View.VISIBLE);

            for (SliderPhoto sliderPhoto : home.getPhotos()) {
                final SliderItem sliderItem = new SliderItem(getContext());
                sliderItem.setScaleType(BaseSliderView.ScaleType.CenterCrop);
                sliderItem.image(sliderPhoto.getCoverImage()).setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                    @Override
                    public void onSliderClick(BaseSliderView slider) {
                    }
                });
                DisplayMetrics displaymetrics = new DisplayMetrics();
                getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
                int width = displaymetrics.widthPixels;
                ViewGroup.LayoutParams params = sliderLayout.getLayoutParams();
                // Changes the height and width to the specified *pixels*
                params.height = (width * 9) / 16;
                sliderLayout.addSlider(sliderItem);
            }

            sliderLayout.setDuration(SLIDER_DURATION);
            sliderLayout.startAutoCycle();
        }


    }

    @Override
    public void loadData(boolean pullToRefresh) {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        if (mRunnable != null && mHandler != null) {
//            mHandler.removeCallbacks(mRunnable);
//        }
    }

    private void fitTabLayout() {
        ViewTreeObserver vto = tabs.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int tabWidth = tabs.getMeasuredWidth();
                if (tabWidth < GeneralUtils.getScreenWidth(getActivity())) {
                    tabs.setTabGravity(TabLayout.GRAVITY_FILL);
                    tabs.setTabMode(TabLayout.MODE_FIXED);
                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                            RelativeLayout.LayoutParams.WRAP_CONTENT);
                    tabs.setLayoutParams(layoutParams);
                } else {
                    tabs.setTabMode(TabLayout.MODE_SCROLLABLE);
                }
                tabs.getViewTreeObserver().removeGlobalOnLayoutListener(this);

            }

        });
    }

}
