package com.acidwater.saher.home.model;

import com.acidwater.saher.data.DataEntityWrapper;
import com.acidwater.saher.events.EventMvp;
import com.acidwater.saher.events.model.Event;
import com.acidwater.saher.home.HomeMvp;
import com.acidwater.saher.utils.ConstantStrings;
import com.acidwater.saher.data.DataEntitiesWrapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

/**
 * Created by Doumith on 6/18/2017.
 */
public class HomeInteractor implements HomeMvp.Interactor {


    private SliderApi eventApi;

    public HomeInteractor() {
        String baseURL  = ConstantStrings.BASE_URL;
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder().build();
        Retrofit.Builder retrofitBuilder = new Retrofit.Builder().baseUrl(baseURL.endsWith("/") ? baseURL : baseURL.concat("/"))
                .addConverterFactory(GsonConverterFactory.create()).addCallAdapterFactory(RxJava2CallAdapterFactory.create());
        retrofitBuilder.client(okHttpClient);
        Retrofit retrofit = retrofitBuilder.build();
        eventApi = retrofit.create(SliderApi.class);
    }

    @Override
    public Observable<DataEntityWrapper<List<Slider>>> getSliders() {

        Observable<DataEntityWrapper<List<Slider>>> observable =
                eventApi.getSliders().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());

        return observable;
    }

    public interface SliderApi {
        @GET(ConstantStrings.SLIDER_HOME)
        Observable<DataEntityWrapper<List<Slider>>> getSliders();
    }


}
