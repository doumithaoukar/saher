package com.acidwater.saher.home;

import android.support.v4.app.Fragment;

import com.acidwater.saher.data.DataEntitiesWrapper;
import com.acidwater.saher.data.DataEntityWrapper;
import com.acidwater.saher.events.model.Event;
import com.acidwater.saher.home.model.Slider;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by Doumith on 6/9/2017.
 */
public interface HomeMvp {

    interface Interactor {
        Observable<DataEntityWrapper<List<Slider>>> getSliders();
    }

    interface Presenter extends MvpPresenter<View> {

        void getSliders();

        void getMainTabs();
    }

    interface View extends MvpLceView<DataEntityWrapper<List<Slider>>> {


        void renderMainTabs(List<Fragment> fragments, List<String> titles);
    }
}
