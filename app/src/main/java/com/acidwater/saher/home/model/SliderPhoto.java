package com.acidwater.saher.home.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Doumith on 7/15/2017.
 */
public class SliderPhoto implements Parcelable {

    private int id;
    private String title;
    private String url;
    private String teaser;

    @SerializedName("cover")
    private String coverImage;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTeaser() {
        return teaser;
    }

    public void setTeaser(String teaser) {
        this.teaser = teaser;
    }

    public String getCoverImage() {
        return coverImage;
    }

    public void setCoverImage(String coverImage) {
        this.coverImage = coverImage;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.title);
        dest.writeString(this.url);
        dest.writeString(this.teaser);
        dest.writeString(this.coverImage);
    }

    public SliderPhoto() {
    }

    protected SliderPhoto(Parcel in) {
        this.id = in.readInt();
        this.title = in.readString();
        this.url = in.readString();
        this.teaser = in.readString();
        this.coverImage = in.readString();
    }

    public static final Parcelable.Creator<SliderPhoto> CREATOR = new Parcelable.Creator<SliderPhoto>() {
        @Override
        public SliderPhoto createFromParcel(Parcel source) {
            return new SliderPhoto(source);
        }

        @Override
        public SliderPhoto[] newArray(int size) {
            return new SliderPhoto[size];
        }
    };
}
