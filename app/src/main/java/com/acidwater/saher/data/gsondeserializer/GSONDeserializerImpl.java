package com.acidwater.saher.data.gsondeserializer;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * JsonDataDeserializer is an implementation of Interface DataDeserializer.<br>
 * Contains all the JSON deserlization of all JSON that the application reads
 * from.
 *
 * @author CME
 */
public class GSONDeserializerImpl<T> implements GSONDeserializer<T> {

   public List<T> deserializeArray(String source, Class<T[]> cls, Gson gson) throws JsonSyntaxException
   {
        T[] schedules = gson.fromJson(source, cls);
        return new ArrayList<T>(Arrays.asList(schedules));
    }

   public T deserialize(String source, Class<T> cls, Gson gson) throws JsonSyntaxException
   {
        return gson.fromJson(source, cls);
    }

}
