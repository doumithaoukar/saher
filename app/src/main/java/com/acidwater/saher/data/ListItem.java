package com.acidwater.saher.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Doumith on 7/15/2017.
 */
public class ListItem implements Parcelable {

    private int id;
    private String thumbnail;
    private String name;
    private String teaser;
    private String cover;

    @SerializedName("share_text")
    private String shareText;
    @SerializedName("average_rating")
    private double averageRating;

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    @SerializedName("rating_count")
    private double ratingCount;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTeaser() {
        return teaser;
    }

    public void setTeaser(String teaser) {
        this.teaser = teaser;
    }

    public String getShareText() {
        return shareText;
    }

    public void setShareText(String shareText) {
        this.shareText = shareText;
    }

    public double getAverageRating() {
        return averageRating;
    }

    public void setAverageRating(int averageRating) {
        this.averageRating = averageRating;
    }

    public double getRatingCount() {
        return ratingCount;
    }

    public void setRatingCount(int ratingCount) {
        this.ratingCount = ratingCount;
    }

    public ListItem() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.thumbnail);
        dest.writeString(this.name);
        dest.writeString(this.teaser);
        dest.writeString(this.cover);
        dest.writeString(this.shareText);
        dest.writeDouble(this.averageRating);
        dest.writeDouble(this.ratingCount);
    }

    protected ListItem(Parcel in) {
        this.id = in.readInt();
        this.thumbnail = in.readString();
        this.name = in.readString();
        this.teaser = in.readString();
        this.cover = in.readString();
        this.shareText = in.readString();
        this.averageRating = in.readDouble();
        this.ratingCount = in.readDouble();
    }

    public static final Creator<ListItem> CREATOR = new Creator<ListItem>() {
        @Override
        public ListItem createFromParcel(Parcel source) {
            return new ListItem(source);
        }

        @Override
        public ListItem[] newArray(int size) {
            return new ListItem[size];
        }
    };
}
