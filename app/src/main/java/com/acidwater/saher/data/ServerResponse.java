package com.acidwater.saher.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Doumith on 8/29/2017.
 */
public class ServerResponse {

    @SerializedName("STATUSCODE")
    private String statusCode;
    @SerializedName("STATUS")
    private String status;
    @SerializedName("STATUSDESCRIPTION")
    private String statusDescription;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }
}
