package com.acidwater.saher.data;

import com.google.gson.annotations.Expose;

/**
 * Created by Doumith on 7/15/2017.
 */
public class DataEntityWrapper<T> {


    private int status;
    private String message;
    private T data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
