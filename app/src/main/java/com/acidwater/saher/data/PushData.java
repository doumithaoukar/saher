package com.acidwater.saher.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Doumith on 4/23/2018.
 */
public class PushData implements Parcelable {

    @SerializedName("type")
    private String type;

    @SerializedName("id")
    private int id;


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.type);
        dest.writeInt(this.id);
    }

    public PushData() {
    }

    protected PushData(Parcel in) {
        this.type = in.readString();
        this.id = in.readInt();
    }

    public static final Parcelable.Creator<PushData> CREATOR = new Parcelable.Creator<PushData>() {
        @Override
        public PushData createFromParcel(Parcel source) {
            return new PushData(source);
        }

        @Override
        public PushData[] newArray(int size) {
            return new PushData[size];
        }
    };
}
