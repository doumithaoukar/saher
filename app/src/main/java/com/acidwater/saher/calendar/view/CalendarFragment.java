package com.acidwater.saher.calendar.view;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.acidwater.saher.R;
import com.acidwater.saher.calendar.CalendarMvp;
import com.acidwater.saher.calendar.presenter.CalendarPresenter;
import com.acidwater.saher.data.DataEntitiesWrapper;
import com.acidwater.saher.events.model.Event;
import com.acidwater.saher.events.view.EventAdapter;
import com.acidwater.saher.forgotpassword.presenter.ForgotPasswordPresenter;
import com.acidwater.saher.main.MainActivityListener;
import com.acidwater.saher.utils.mvp.LceRetryFragment;
import com.acidwater.saher.widgets.SpacesItemDecoration;
import com.acidwater.saher.widgets.SpacesItemDecorationNoTopMargin;
import com.hannesdorfmann.mosby3.mvp.MvpFragment;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceFragment;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;

import org.w3c.dom.Text;

import java.util.Calendar;
import java.util.Date;
import java.util.List;


/**
 * Created by Doumith on 7/6/2017.
 */
public class CalendarFragment extends LceRetryFragment<View, DataEntitiesWrapper<List<Event>>, CalendarMvp.View, CalendarMvp.Presenter> implements View.OnClickListener, CalendarMvp.View {


    private RecyclerView recyclerView;
    private MaterialCalendarView calendarView;
    private MainActivityListener mListener;

    private TextView emptyView;


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MainActivityListener) {
            mListener = (MainActivityListener) activity;
        } else {
            throw new RuntimeException("you should implemented MainActivityListener");
        }
    }

    public static CalendarFragment newInstance() {
        CalendarFragment calendarFragment = new CalendarFragment();
        return calendarFragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        loadData(false);

    }

    @Override
    protected void customizeLoadingView(View loadingView) {

    }

    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View containerView = inflater.inflate(R.layout.fragment_calendar, container, false);
        recyclerView = (RecyclerView) containerView.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.addItemDecoration(new SpacesItemDecorationNoTopMargin(35));
        calendarView = (MaterialCalendarView) containerView.findViewById(R.id.calendarView);
        calendarView.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
                long selectedTime = date.getDate().getTime() / 1000;
                presenter.getEvents(selectedTime);
            }
        });
        emptyView = (TextView) containerView.findViewById(R.id.emptyView);
        return containerView;
    }


    @Override
    public CalendarMvp.Presenter createPresenter() {
        return new CalendarPresenter(getContext());
    }

    @Override
    public void retry() {
        loadData(false);
    }

    @Override
    public void showErrorMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();

    }


    private void destroyThis() {
        getFragmentManager().beginTransaction().remove(this).commit();
        getFragmentManager().popBackStack();
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void setData(DataEntitiesWrapper<List<Event>> data) {
        if (data.getData().size() == 0) {
            emptyView.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        } else {
            emptyView.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);

            EventAdapter eventAdapter = new EventAdapter(getContext(), data.getData());
            eventAdapter.setOnItemClickListener(new EventAdapter.onItemClickListener() {
                @Override
                public void onClick(Event event, int position) {
                    mListener.navigateToEventDetails(event.getId());

                }
            });
            recyclerView.setAdapter(eventAdapter);
        }


    }

    @Override
    public void loadData(boolean pullToRefresh) {
        long now = Calendar.getInstance().getTimeInMillis() / 1000;
        calendarView.setSelectedDate(Calendar.getInstance());
        presenter.getEvents(now);
    }
}