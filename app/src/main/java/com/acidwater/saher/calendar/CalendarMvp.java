package com.acidwater.saher.calendar;

import com.acidwater.saher.data.DataEntitiesWrapper;
import com.acidwater.saher.data.DataEntityWrapper;
import com.acidwater.saher.data.ServerResponse;
import com.acidwater.saher.events.details.model.EventDetails;
import com.acidwater.saher.events.model.Event;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by Doumith on 6/18/2017.
 */
public interface CalendarMvp {

    interface Interactor {

        Observable<DataEntitiesWrapper<List<Event>>> getEvent(long time);
    }

    interface Presenter extends MvpPresenter<View> {


        void getEvents(long time);
    }

    interface View extends MvpLceView<DataEntitiesWrapper<List<Event>>> {

        void showErrorMessage(String message);

    }
}
