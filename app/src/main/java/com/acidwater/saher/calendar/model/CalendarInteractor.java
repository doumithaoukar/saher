package com.acidwater.saher.calendar.model;

import android.content.Context;

import com.acidwater.saher.calendar.CalendarMvp;
import com.acidwater.saher.data.DataEntitiesWrapper;
import com.acidwater.saher.data.DataEntityWrapper;
import com.acidwater.saher.data.ServerResponse;
import com.acidwater.saher.events.details.model.EventDetails;
import com.acidwater.saher.events.model.Event;
import com.acidwater.saher.forgotpassword.ForgotPasswordMvp;
import com.acidwater.saher.services.TokenInterceptor;
import com.acidwater.saher.utils.ConstantStrings;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Url;

/**
 * Created by Doumith on 7/13/2017.
 */
public class CalendarInteractor implements CalendarMvp.Interactor {


    private CalendarApi calendarApi;

    public CalendarInteractor(Context context) {
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder().addInterceptor(new TokenInterceptor(context)).build();
        String baseURL = ConstantStrings.BASE_URL;
        Retrofit.Builder retrofitBuilder = new Retrofit.Builder().baseUrl(baseURL.endsWith("/") ? baseURL : baseURL.concat("/"))
                .addConverterFactory(GsonConverterFactory.create()).addCallAdapterFactory(RxJava2CallAdapterFactory.create());
        retrofitBuilder.client(okHttpClient);
        Retrofit retrofit = retrofitBuilder.build();
        calendarApi = retrofit.create(CalendarApi.class);
    }


    @Override
    public Observable<DataEntitiesWrapper<List<Event>>> getEvent(long time) {
        String URL = ConstantStrings.BASE_URL.endsWith("/") ? ConstantStrings.BASE_URL : ConstantStrings.BASE_URL.concat("/");
        URL += ConstantStrings.SERVICE_CALENDAR + "/" + String.valueOf(time);


        Observable<DataEntitiesWrapper<List<Event>>> observable =
                calendarApi.getEvents(String.valueOf(URL)).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());

        return observable;
    }


    public interface CalendarApi {
        @GET
        Observable<DataEntitiesWrapper<List<Event>>> getEvents(@Url String url);
    }


}
