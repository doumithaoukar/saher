package com.acidwater.saher.calendar.presenter;

import android.content.Context;
import android.text.TextUtils;

import com.acidwater.saher.R;
import com.acidwater.saher.calendar.CalendarMvp;
import com.acidwater.saher.calendar.model.CalendarInteractor;
import com.acidwater.saher.data.DataEntitiesWrapper;
import com.acidwater.saher.data.DataEntityWrapper;
import com.acidwater.saher.data.ServerResponse;
import com.acidwater.saher.events.model.Event;
import com.acidwater.saher.exceptions.SaherException;
import com.acidwater.saher.forgotpassword.ForgotPasswordMvp;
import com.acidwater.saher.forgotpassword.model.ForgotPasswordInteractor;
import com.acidwater.saher.services.ServiceObserver;
import com.acidwater.saher.utils.GeneralUtils;
import com.acidwater.saher.utils.RxUtils;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import java.util.List;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Doumith on 7/13/2017.
 */
public class CalendarPresenter extends MvpBasePresenter<CalendarMvp.View> implements CalendarMvp.Presenter {

    private Context context;
    private CalendarInteractor calendarInteractor;
    private CompositeDisposable compositeDisposable;

    public CalendarPresenter(Context context) {
        this.context = context;
        calendarInteractor = new CalendarInteractor(context);
        compositeDisposable = new CompositeDisposable();

    }

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        if (!retainInstance) {
            RxUtils.disposeIfNotNull(compositeDisposable);
        }
    }

    @Override
    public void getEvents(long time) {
        compositeDisposable.add(calendarInteractor.getEvent(time).subscribeWith(new ServiceObserver<DataEntitiesWrapper<List<Event>>>() {
            @Override
            public void onStart() {
                if (isViewAttached()) {
                    getView().showLoading(false);
                }
            }

            @Override
            public void onComplete() {

                if (isViewAttached()) {

                    getView().showContent();
                }
            }

            @Override
            public void onNext(DataEntitiesWrapper<List<Event>> dataEntityWrapper) {

                if (isViewAttached()) {
                    getView().showContent();
                    getView().setData(dataEntityWrapper);
                }
            }

            @Override
            public void onError(SaherException e) {

                if (isViewAttached()) {

                    getView().showErrorMessage(e.errorMessage);
                    getView().showContent();
                }
            }
        }));
    }
}
