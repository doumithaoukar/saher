package com.acidwater.saher.profile;

import android.graphics.Bitmap;

import com.acidwater.saher.data.DataEntitiesWrapper;
import com.acidwater.saher.data.ServerResponse;
import com.acidwater.saher.events.model.Event;
import com.acidwater.saher.profile.model.User;
import com.acidwater.saher.data.DataEntityWrapper;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by Doumith on 10/21/2017.
 */
public interface ProfileMvp {


    interface Interactor {

        Observable<DataEntityWrapper<User>> getProfile();

        Observable<ServerResponse> changePassword(String oldPass, String newPass);

        Observable<ServerResponse> updateProfile(Bitmap bitmap, String fullName, String birthday, String gender);

    }

    interface Presenter extends MvpPresenter<View> {

        void getProfile();

        void updateProfile(Bitmap bitmap, String fullName, String birthday, String gender);

        void validateProfile(Bitmap bitmap, String firstName, String birthday, String gender);

    }

    interface View extends MvpLceView<DataEntityWrapper<User>> {

        void showErrorMessage(String message);

        void showLoadingView();

        void HideLoadingView();

        void setResultSuccessful(String message);
    }
}
