package com.acidwater.saher.profile.model;

import android.content.Context;
import android.graphics.Bitmap;

import com.acidwater.saher.data.ServerResponse;
import com.acidwater.saher.profile.ProfileMvp;
import com.acidwater.saher.data.DataEntityWrapper;
import com.acidwater.saher.services.AddCookiesInterceptor;
import com.acidwater.saher.services.ReceivedCookiesInterceptor;
import com.acidwater.saher.utils.ConstantStrings;
import com.acidwater.saher.utils.GeneralUtils;
import com.acidwater.saher.utils.bus.MainBus;
import com.acidwater.saher.utils.bus.events.UpdateProfile;


import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;


/**
 * Created by Doumith on 7/13/2017.
 */
public class ProfileInteractor implements ProfileMvp.Interactor {


    private ProfileApi profileApi;
    private Context context;

    public ProfileInteractor(Context context) {
        this.context = context;
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder().
                addInterceptor(new ReceivedCookiesInterceptor(context)).addInterceptor(new AddCookiesInterceptor(context)).build();

        String baseURL = ConstantStrings.BASE_URL;
        Retrofit.Builder retrofitBuilder = new Retrofit.Builder().baseUrl(baseURL.endsWith("/") ? baseURL : baseURL.concat("/"))
                .addConverterFactory(GsonConverterFactory.create()).addCallAdapterFactory(RxJava2CallAdapterFactory.create());
        retrofitBuilder.client(okHttpClient);
        Retrofit retrofit = retrofitBuilder.build();
        profileApi = retrofit.create(ProfileApi.class);
    }


    @Override
    public Observable<DataEntityWrapper<User>> getProfile() {
        Observable<DataEntityWrapper<User>> observable =
                profileApi.getProfile().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
        return observable;
    }

    @Override
    public Observable<ServerResponse> changePassword(String oldPass, String newPass) {
        Observable<ServerResponse> observable = null;
        observable =
                profileApi.changePassword(GeneralUtils.getUserProfile(context).getEmail(), GeneralUtils.md5(oldPass), GeneralUtils.md5(newPass)).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
        return observable;
    }

    @Override
    public Observable<ServerResponse> updateProfile(Bitmap bitmap, String fullName, String birthday, String gender) {
        Observable<ServerResponse> observable = null;

        MultipartBody.Part filePart = null;
        if (bitmap != null) {
            filePart = MultipartBody.Part.createFormData("FILE", "image", RequestBody.create(MediaType.parse("image/*"), GeneralUtils.convertBitmapToFile(context, bitmap, "FILE")));

        }
        observable =
                profileApi.updateProfile(filePart, GeneralUtils.toRequestBody(fullName), GeneralUtils.toRequestBody(birthday),
                        GeneralUtils.toRequestBody(gender)).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
        return observable;
    }


    public interface ProfileApi {
        @GET(ConstantStrings.SERVICE_USER_PROFILE)
        Observable<DataEntityWrapper<User>> getProfile();

        @FormUrlEncoded
        @POST(ConstantStrings.SERVICE_PROFILE_UPDATE)
        Observable<ServerResponse> changePassword(@Field("EMAIL") String email,
                                                  @Field("OLD_PASSWORD") String oldPassword,
                                                  @Field("PASSWORD") String password);

        @Multipart
        @POST(ConstantStrings.SERVICE_PROFILE_UPDATE)
        Observable<ServerResponse> updateProfile(@Part MultipartBody.Part filePart,
                                                 @Part("FULL_NAME") RequestBody name,
                                                 @Part("BIRTH_DATE") RequestBody birthday,
                                                 @Part("GENDER") RequestBody gender);
    }


}
