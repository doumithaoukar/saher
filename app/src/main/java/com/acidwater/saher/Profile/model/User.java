package com.acidwater.saher.profile.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.acidwater.saher.favorites.model.Favorite;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Doumith on 10/21/2017.
 */
public class User implements Parcelable {

    @SerializedName("id")
    private int id;
    @SerializedName("full_name")
    private String fullName;
    @SerializedName("user_name")
    private String username;
    @SerializedName("email")
    private String email;
    @SerializedName("date_of_birth")
    private String dateOfBirth;
    @SerializedName("photo")
    private String image;
    @SerializedName("gender")
    private String gender;

    @SerializedName("favorites")
    ArrayList<Favorite> favorites;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public ArrayList<Favorite> getFavorites() {
        return favorites;
    }

    public void setFavorites(ArrayList<Favorite> favorites) {
        this.favorites = favorites;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.fullName);
        dest.writeString(this.username);
        dest.writeString(this.email);
        dest.writeString(this.dateOfBirth);
        dest.writeString(this.image);
        dest.writeTypedList(this.favorites);
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public User() {
    }

    protected User(Parcel in) {
        this.id = in.readInt();
        this.fullName = in.readString();
        this.username = in.readString();
        this.email = in.readString();
        this.dateOfBirth = in.readString();
        this.image = in.readString();
        this.favorites = in.createTypedArrayList(Favorite.CREATOR);
    }

    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
