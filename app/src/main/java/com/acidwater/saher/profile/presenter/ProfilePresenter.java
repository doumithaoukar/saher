package com.acidwater.saher.profile.presenter;

import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.widget.Toast;

import com.acidwater.saher.data.DataEntityWrapper;
import com.acidwater.saher.data.ServerResponse;
import com.acidwater.saher.exceptions.SaherException;
import com.acidwater.saher.profile.ProfileMvp;
import com.acidwater.saher.profile.model.ProfileInteractor;
import com.acidwater.saher.profile.model.User;
import com.acidwater.saher.registration.RegistrationMvp;
import com.acidwater.saher.services.ServiceObserver;
import com.acidwater.saher.utils.GeneralUtils;
import com.acidwater.saher.utils.RxUtils;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import java.io.File;
import java.io.FileOutputStream;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Doumith on 11/30/2017.
 */
public class ProfilePresenter extends MvpBasePresenter<ProfileMvp.View> implements ProfileMvp.Presenter {

    private Context context;
    CompositeDisposable compositeDisposable;
    ProfileInteractor profileInteractor;

    public ProfilePresenter(Context context) {
        this.context = context;
        compositeDisposable = new CompositeDisposable();
        this.profileInteractor = new ProfileInteractor(context);
    }

    @Override
    public void getProfile() {

        compositeDisposable.add(profileInteractor.getProfile().subscribeWith(new ServiceObserver<DataEntityWrapper<User>>() {
            @Override
            public void onStart() {
                if (isViewAttached()) {
                    getView().showLoadingView();
                }
            }

            @Override
            public void onComplete() {
                if (isViewAttached()) {
                    getView().HideLoadingView();
                }
            }

            @Override
            public void onNext(DataEntityWrapper<User> userDataEntityWrapper) {
                if (userDataEntityWrapper != null) {
                    if (isViewAttached()) {
                        getView().setData(userDataEntityWrapper);
                    }
                    GeneralUtils.saveUserProfile(context, userDataEntityWrapper.getData());
                }
            }

            @Override
            public void onError(SaherException e) {
                if (isViewAttached()) {
                    getView().HideLoadingView();
                    getView().showError(e, false);
                }
            }
        }));
    }

    @Override
    public void updateProfile(Bitmap bitmap, String fullName, String birthday, String gender) {
        compositeDisposable.add(profileInteractor.updateProfile(bitmap, fullName, birthday, gender)
                .subscribeWith(new ServiceObserver<ServerResponse>() {
                    @Override
                    public void onStart() {
                        if (isViewAttached()) {
                            getView().showLoadingView();
                        }
                    }

                    @Override
                    public void onComplete() {

                        if (isViewAttached()) {
                            getView().HideLoadingView();
                        }
                    }

                    @Override
                    public void onNext(ServerResponse serverResponse) {

                        if (isViewAttached()) {
                            getView().setResultSuccessful(serverResponse.getStatusDescription());
                        }
                        compositeDisposable.add(profileInteractor.getProfile().subscribeWith(new ServiceObserver<DataEntityWrapper<User>>() {
                            @Override
                            public void onStart() {

                            }

                            @Override
                            public void onComplete() {

                            }

                            @Override
                            public void onNext(DataEntityWrapper<User> userDataEntityWrapper) {
                                if (userDataEntityWrapper != null) {
                                    GeneralUtils.saveUserProfile(context, userDataEntityWrapper.getData());
                                }
                            }

                            @Override
                            public void onError(SaherException e) {

                            }
                        }));
                    }

                    @Override
                    public void onError(SaherException e) {

                        if (isViewAttached()) {

                            getView().showErrorMessage(e.errorMessage);
                            getView().HideLoadingView();
                        }
                    }
                }));
    }

    @Override
    public void validateProfile(Bitmap bitmap, String firstName, String birthday, String gender) {

        updateProfile(bitmap, firstName, birthday, gender);
    }

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        if (!retainInstance) {
            RxUtils.disposeIfNotNull(compositeDisposable);
        }
    }


}
