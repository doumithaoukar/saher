package com.acidwater.saher.profile.view;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.acidwater.saher.R;
import com.acidwater.saher.data.DataEntityWrapper;
import com.acidwater.saher.main.MainActivityListener;
import com.acidwater.saher.profile.ProfileMvp;
import com.acidwater.saher.profile.model.User;
import com.acidwater.saher.profile.presenter.ProfilePresenter;
import com.acidwater.saher.registration.RegistrationMvp;
import com.acidwater.saher.registration.presenter.RegistrationPresenter;
import com.acidwater.saher.utils.ConstantStrings;
import com.acidwater.saher.utils.GeneralUtils;
import com.acidwater.saher.utils.mvp.LceRetryFragment;
import com.hannesdorfmann.mosby3.mvp.MvpFragment;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.IOException;
import java.util.Calendar;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Doumith on 7/6/2017.
 */
public class ProfileFragment extends LceRetryFragment<View, DataEntityWrapper<User>, ProfileMvp.View, ProfileMvp.Presenter> implements View.OnClickListener, ProfileMvp.View {


    private Button btnContinue;
    private EditText fullName;

    private TextView birthday;
    private TextView gender;
    private RelativeLayout progressBar;

    public static final int SELECT_PICTURE = 1001;

    @Override
    public void loadData(boolean pullToRefresh) {
        super.loadData(pullToRefresh);
        presenter.getProfile();
    }

    private CircleImageView profileImage;
    private MainActivityListener mListener;
    private Bitmap bitmap;

    public static ProfileFragment newInstance() {
        ProfileFragment registrationFragment = new ProfileFragment();
        return registrationFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(
                R.layout.fragment_profile, container, false);

        fullName = (EditText) mView.findViewById(R.id.fullname);
        btnContinue = (Button) mView.findViewById(R.id.btn_continue);
        progressBar = (RelativeLayout) mView.findViewById(R.id.main_progress);
        birthday = (TextView) mView.findViewById(R.id.birthday);
        gender = (TextView) mView.findViewById(R.id.gender);


        btnContinue.setOnClickListener(this);
        profileImage = (CircleImageView) mView.findViewById(R.id.profile_image);
        profileImage.setOnClickListener(this);

        birthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                                birthday.setText(dayOfMonth + "/" + monthOfYear + 1 + "/" + year);
                            }
                        },
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)

                );
                dpd.setAccentColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
                dpd.show(getActivity().getFragmentManager(), "show");

            }
        });

        gender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builderSingle = new AlertDialog.Builder(getActivity());
                builderSingle.setTitle(getString(R.string.select_gender));

                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.select_dialog_item);
                arrayAdapter.add("male");
                arrayAdapter.add("female");

                builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        gender.setText(arrayAdapter.getItem(which).toString());
                    }
                });
                builderSingle.show();
            }
        });

        return mView;
    }

    @Override
    protected void customizeLoadingView(View loadingView) {

    }

    @Override
    public void setData(DataEntityWrapper<User> data) {
        super.setData(data);

        if (data.getData() != null) {
            fullName.setText(data.getData().getFullName());
            if (!TextUtils.isEmpty(data.getData().getDateOfBirth())) {
                birthday.setText(GeneralUtils.parseDateToddMMyyyy(data.getData().getDateOfBirth()));
            }
            gender.setText(data.getData().getGender());
            if (!TextUtils.isEmpty(data.getData().getImage())) {
                Picasso.with(getContext()).load(data.getData().getImage()).placeholder(R.drawable.ic_profile_placeholder).into(profileImage);
            }
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_continue:

                presenter.validateProfile(bitmap,
                        fullName.getText().toString().trim(),
                        birthday.getText().toString(), gender.getText().toString());
                break;

            case R.id.profile_image:
                choosePictureFromGallery();
                break;

        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        loadData(false);
    }

    @Override
    public ProfileMvp.Presenter createPresenter() {
        return new ProfilePresenter(getContext());
    }

    @Override
    public void retry() {
        loadData(false);
    }

    @Override
    public void showErrorMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void showLoadingView() {
        progressBar.setVisibility(View.VISIBLE);

    }

    @Override
    public void HideLoadingView() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void setResultSuccessful(String message) {

        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MainActivityListener) {
            mListener = (MainActivityListener) activity;
        } else {
            throw new RuntimeException("you should implemented MainActivityListener");
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void destroyThis() {
        getFragmentManager().beginTransaction().remove(this).commit();
        getFragmentManager().popBackStack();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        mListener = null;

    }

    private void choosePictureFromGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            if (resultCode == Activity.RESULT_OK) {
                if (requestCode == SELECT_PICTURE) {
                    Uri selectedImageUri = data.getData();
                    Intent intent = CropImage.activity(selectedImageUri).getIntent(getContext());
                    startActivityForResult(intent, CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE);

                } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                    CropImage.ActivityResult result = CropImage.getActivityResult(data);
                    bitmap = loadBitmapFromUri(result.getUri());
                    profileImage.setImageBitmap(bitmap);
                }
            }

        }
    }

    /**
     * This method loads bitmap from a given image URI
     *
     * @param selectedImage
     * @return
     */
    private Bitmap loadBitmapFromUri(Uri selectedImage) {
        Bitmap bitmap = null;
        try {
            bitmap = MediaStore.Images.Media.getBitmap(this.getActivity().getContentResolver(), selectedImage);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;
    }


}