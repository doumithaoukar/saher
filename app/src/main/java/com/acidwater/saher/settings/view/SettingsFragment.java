package com.acidwater.saher.settings.view;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.preference.EditTextPreference;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.preference.SwitchPreferenceCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.acidwater.saher.R;
import com.acidwater.saher.main.MainActivityListener;
import com.acidwater.saher.utils.ConstantStrings;
import com.acidwater.saher.utils.GeneralUtils;


public class SettingsFragment extends PreferenceFragmentCompat implements OnSharedPreferenceChangeListener {
    private SharedPreferences prefs;
    private MainActivityListener mListener;


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MainActivityListener) {
            mListener = (MainActivityListener) activity;
        } else {
            throw new RuntimeException("you should implemented MainActivityListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        prefs.registerOnSharedPreferenceChangeListener(this);
        setHasOptionsMenu(true);
        addPreferencesFromResource(R.xml.prefs);

        Preference PROFILE = (Preference) findPreference(ConstantStrings.SETTINGS_PROFILE);
        PROFILE.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                mListener.navigateToProfile();
                return false;
            }
        });


        Preference CHANGE_PASSWORD = (Preference) findPreference(ConstantStrings.SETTINGS_CHANGE_PASSWORD);
        CHANGE_PASSWORD.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                mListener.navigateToChangePassword();
                return false;
            }
        });


        final Preference NOTIFICATIONS = (Preference) findPreference(ConstantStrings.SETTINGS_NOTIFICATIONS);
        NOTIFICATIONS.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {

                if (newValue.equals(false)) {
                    //set switch on off programatically after the logic
                    ((SwitchPreferenceCompat) NOTIFICATIONS).setChecked(false);
                } else {
                    ((SwitchPreferenceCompat) NOTIFICATIONS).setChecked(true);

                }
                return false;
            }
        });

        Preference LOGOUT = (Preference) findPreference(ConstantStrings.SETTINGS_LOGOUT);
        LOGOUT.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {

                confirmLogout();
                return true;
            }
        });

        Preference FAQ = (Preference) findPreference(ConstantStrings.SETTINGS_FAQ);
        FAQ.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {

                openUrl(ConstantStrings.FAQ_URL);
                return true;
            }
        });


        Preference ABOUT = (Preference) findPreference(ConstantStrings.SETTINGS_ABOUT);
        ABOUT.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {

                openUrl(ConstantStrings.ABOUT_URL);
                return true;
            }
        });

        Preference TERMS = (Preference) findPreference(ConstantStrings.SETTINGS_TERMS);
        TERMS.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {

                openUrl(ConstantStrings.TERMS_URL);
                return true;
            }
        });

        Preference CONTACT_US = (Preference) findPreference(ConstantStrings.SETTINGS_CONTACT_US);
        CONTACT_US.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {

                openUrl(ConstantStrings.CONTACT_URL);
                return true;
            }
        });

        Preference WEBSITE = (Preference) findPreference(ConstantStrings.SETTINGS_WEBSITE);
        WEBSITE.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {

                openUrl(ConstantStrings.WEBSITE_URL);
                return true;
            }
        });

        Preference ShareAPP = (Preference) findPreference(ConstantStrings.SETTINGS_SHARE_APP);
        ShareAPP.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {

                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.share_app_text) + GeneralUtils.getShareURl(getContext()));
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
                return true;
            }
        });


        if (GeneralUtils.getSharedPref(getContext(), ConstantStrings.USER_SESSION).equals(ConstantStrings.user_is_logged_in)) {
            PROFILE.setVisible(true);
            LOGOUT.setVisible(true);
        } else {
            PROFILE.setVisible(false);
            LOGOUT.setVisible(false);
        }

        if (GeneralUtils.getSharedPref(getContext(), ConstantStrings.LOGGED_ACCOUNT_TYPE).equals(ConstantStrings.EMAIL_ACCOUNT)) {
            CHANGE_PASSWORD.setVisible(true);
        } else {
            CHANGE_PASSWORD.setVisible(false);
        }
    }

    @Override
    public void onResume() {

        super.onResume();
        prefs.registerOnSharedPreferenceChangeListener(this);

    }

    @Override
    public void onPause() {
        super.onPause();
        prefs.unregisterOnSharedPreferenceChangeListener(this);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        if (view != null) {
            if (getListView() != null) {
                getListView().setPadding(0, 0, 0, 0);
            }
        }
        return view;
    }

    @Override
    public void setDivider(Drawable divider) {
        super.setDivider(new ColorDrawable(Color.WHITE));
    }

    @Override
    public void setDividerHeight(int height) {
        super.setDividerHeight(1);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        setSummaryText(key);
    }

    private void setSummaryText(String key) {

        Preference pref = findPreference(key);
        if (pref instanceof EditTextPreference) {
            EditTextPreference etp = (EditTextPreference) pref;
            pref.setSummary(etp.getText());
        }

    }

    private void confirmLogout() {
        new AlertDialog.Builder(getContext())
                .setTitle(getString(R.string.settings_logout))
                .setMessage(getString(R.string.confirm_logout))
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        GeneralUtils.logout(getContext());
                    }

                })
                .setNegativeButton(android.R.string.no, null)
                .show();
    }

    private void openUrl(String url) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }
}
