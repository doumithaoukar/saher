package com.acidwater.saher.musicians.view;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.acidwater.saher.main.MainActivityListener;
import com.acidwater.saher.R;
import com.acidwater.saher.data.DataEntitiesWrapper;
import com.acidwater.saher.musicians.MusicianMvp;
import com.acidwater.saher.musicians.model.Musician;
import com.acidwater.saher.musicians.model.MusicianCategory;
import com.acidwater.saher.musicians.presenter.MusiciansPresenter;
import com.acidwater.saher.utils.ConstantStrings;
import com.acidwater.saher.utils.mvp.LceRetryFragment;
import com.acidwater.saher.widgets.SpacesItemDecoration;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceFragment;
import com.jcodecraeer.xrecyclerview.XRecyclerView;

import java.util.List;

/**
 * Created by Doumith on 6/9/2017.
 */
public class ArtistsFragment extends LceRetryFragment<View, DataEntitiesWrapper<List<MusicianCategory>>, MusicianMvp.View, MusicianMvp.Presenter> implements MusicianMvp.View {

    private int page = 1;
    private XRecyclerView recyclerView;
    private MainActivityListener mListener;
    private SwipeRefreshLayout swipeRefreshLayout;
    private MusicianCategoryAdapter musicianCategoryAdapter;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MainActivityListener) {
            mListener = (MainActivityListener) activity;
        } else {
            throw new RuntimeException("you should implemented MainActivityListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public static ArtistsFragment newInstance() {
        ArtistsFragment artistsFragment = new ArtistsFragment();
        return artistsFragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadData(false);
    }

    @Override
    protected void customizeLoadingView(View loadingView) {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View containerView = inflater.inflate(R.layout.fragment_artists, container, false);
        // reset all data after fragment is destroyed
        page = 1;
        swipeRefreshLayout = (SwipeRefreshLayout) containerView.findViewById(R.id.swipe_refresh_layout);
        recyclerView = (XRecyclerView) containerView.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.addItemDecoration(new SpacesItemDecoration(15));
        recyclerView.setPullRefreshEnabled(false);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                page = 1;
                presenter.getMusicians(page, ConstantStrings.TYPE_ARTIST);
            }
        });
        return containerView;

    }

    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return null;
    }


    @Override
    public void setData(DataEntitiesWrapper<List<MusicianCategory>> data) {
        if (page == 1) {
            musicianCategoryAdapter = new MusicianCategoryAdapter(getContext(), data.getData(), presenter);
            recyclerView.setAdapter(musicianCategoryAdapter);
            musicianCategoryAdapter.setOnItemClickListener(new MusicianCategoryAdapter.onItemClickListener() {
                @Override
                public void onClick(Musician musician, int position) {
                    mListener.navigateToMusicianDetails(musician.getId());
                }
            });
            recyclerView.setLoadingMoreEnabled(true);
            recyclerView.setLoadingListener(new XRecyclerView.LoadingListener() {
                @Override
                public void onRefresh() {
                    //refresh data here
                }

                @Override
                public void onLoadMore() {
                    // load more data here
                    page++;
                    presenter.loadMoreMusicians(page, ConstantStrings.TYPE_ARTIST);
                }
            });
        } else {
            musicianCategoryAdapter.getList().addAll(data.getData());
            musicianCategoryAdapter.notifyDataSetChanged();
            hideLoadMore();
            if (data.getData().size() < ConstantStrings.EVENTS_PAGING) {
                recyclerView.setLoadingMoreEnabled(false);
            }
        }

        swipeRefreshLayout.setRefreshing(false);

    }

    @Override
    public void loadData(boolean pullToRefresh) {
        presenter.getMusicians(page, ConstantStrings.TYPE_ARTIST);
    }

    @Override
    public MusicianMvp.Presenter createPresenter() {
        return new MusiciansPresenter();
    }

    @Override
    public void retry() {
        page = 1;
        loadData(false);
    }


    @Override
    public void setError(String message) {

    }

    @Override
    public void openMoreMusicians(String categoryName, int categoryID) {
        mListener.navigateToMoreArtists(categoryName, categoryID);
    }

    @Override
    public void hideLoadMore() {
        recyclerView.loadMoreComplete();
    }

}
