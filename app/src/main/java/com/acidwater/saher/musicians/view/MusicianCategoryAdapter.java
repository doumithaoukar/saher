package com.acidwater.saher.musicians.view;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.acidwater.saher.Places.view.PlaceAdapter;
import com.acidwater.saher.R;
import com.acidwater.saher.musicians.MusicianMvp;
import com.acidwater.saher.musicians.model.Musician;
import com.acidwater.saher.musicians.model.MusicianCategory;
import com.acidwater.saher.musicians.presenter.MusiciansPresenter;
import com.acidwater.saher.widgets.GridItemSpacesDecoration;

import java.util.List;

/**
 * Created by D on 7/20/2015.
 */

public class MusicianCategoryAdapter extends RecyclerView.Adapter<MusicianCategoryAdapter.ViewHolder> {

    private List<MusicianCategory> list;
    private Context context;
    private MusicianMvp.Presenter presenter;
    private onItemClickListener onItemClickListener;

    public MusicianCategoryAdapter(Context context, List<MusicianCategory> list, MusicianMvp.Presenter presenter) {
        this.list = list;
        this.context = context;
        this.presenter = presenter;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView title;
        public RecyclerView recyclerView;
        public LinearLayout btnMore;


        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            title = (TextView) itemLayoutView.findViewById(R.id.musician_category_title);
            recyclerView = (RecyclerView) itemLayoutView.findViewById(R.id.recyclerView);
            btnMore = (LinearLayout) itemLayoutView.findViewById(R.id.btn_more);

        }
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_musician_category, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        viewHolder.title.setText(list.get(position).getName());
        MusicianAdapter placeAdapter = new MusicianAdapter(context, list.get(position).getMusician());
        placeAdapter.setOnItemClickListener(new MusicianAdapter.onItemClickListener() {
            @Override
            public void onClick(Musician musician, int position) {
                if (onItemClickListener != null) {
                    onItemClickListener.onClick(musician, position);
                }
            }
        });
        viewHolder.recyclerView.setLayoutManager(new GridLayoutManager(context, 2));
        viewHolder.recyclerView.addItemDecoration(new GridItemSpacesDecoration(15));
        viewHolder.recyclerView.setNestedScrollingEnabled(false);
        viewHolder.recyclerView.setAdapter(placeAdapter);
        viewHolder.btnMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.openMoreMusicians(list.get(position).getName(), list.get(position).getId());
            }
        });
    }

    public interface onItemClickListener {
        void onClick(Musician musician, int position);
    }

    public void setOnItemClickListener(MusicianCategoryAdapter.onItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public List<MusicianCategory> getList() {
        return list;
    }

    public void setList(List<MusicianCategory> list) {
        this.list = list;
    }
}