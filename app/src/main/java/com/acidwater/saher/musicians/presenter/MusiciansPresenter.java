package com.acidwater.saher.musicians.presenter;

import com.acidwater.saher.musicians.MusicianMvp;
import com.acidwater.saher.musicians.model.Musician;
import com.acidwater.saher.musicians.model.MusicianCategory;
import com.acidwater.saher.musicians.model.MusiciansInteractor;
import com.acidwater.saher.data.DataEntitiesWrapper;
import com.acidwater.saher.exceptions.SaherException;
import com.acidwater.saher.services.ServiceObserver;
import com.acidwater.saher.utils.RxUtils;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import java.util.List;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Doumith on 6/9/2017.
 */
public class MusiciansPresenter extends MvpBasePresenter<MusicianMvp.View> implements MusicianMvp.Presenter {

    private MusiciansInteractor musiciansInteractor;
    private CompositeDisposable compositeDisposable;

    public MusiciansPresenter() {
        musiciansInteractor = new MusiciansInteractor();
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        if (!retainInstance) {
            RxUtils.disposeIfNotNull(compositeDisposable);
        }
    }

    @Override
    public void getMusicians(int page, String type) {

        if (isViewAttached()) {
            getView().showLoading(false);
        }

        compositeDisposable.add(musiciansInteractor.getMusicians(page, type).subscribeWith(new ServiceObserver<DataEntitiesWrapper<List<MusicianCategory>>>() {
            @Override
            public void onStart() {

            }

            @Override
            public void onComplete() {

                if (isViewAttached()) {
                    getView().showContent();
                }
            }

            @Override
            public void onNext(DataEntitiesWrapper<List<MusicianCategory>> listDataEntitiesWrapper) {
                if (isViewAttached()) {
                    getView().setData(listDataEntitiesWrapper);
                }
            }

            @Override
            public void onError(SaherException e) {

                if (isViewAttached()) {
                    getView().showError(e, false);
                }
            }
        }));
    }

    @Override
    public void openMoreMusicians(String categoryName, int categoryId) {

        if(isViewAttached()){
            getView().openMoreMusicians(categoryName,categoryId);
        }
    }

    @Override
    public void loadMoreMusicians(int page, String type) {


        compositeDisposable.add(musiciansInteractor.getMusicians(page, type).subscribeWith(new ServiceObserver<DataEntitiesWrapper<List<MusicianCategory>>>() {
            @Override
            public void onStart() {

            }

            @Override
            public void onComplete() {

                if (isViewAttached()) {
                    getView().hideLoadMore();
                }
            }

            @Override
            public void onNext(DataEntitiesWrapper<List<MusicianCategory>> listDataEntitiesWrapper) {
                if (isViewAttached()) {
                    getView().setData(listDataEntitiesWrapper);
                }
            }

            @Override
            public void onError(SaherException e) {

                if (isViewAttached()) {
                    getView().hideLoadMore();
                }
            }
        }));
    }


}
