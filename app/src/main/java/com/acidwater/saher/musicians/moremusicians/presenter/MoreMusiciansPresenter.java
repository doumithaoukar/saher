package com.acidwater.saher.musicians.moremusicians.presenter;

import com.acidwater.saher.data.DataEntitiesWrapper;
import com.acidwater.saher.exceptions.SaherException;
import com.acidwater.saher.musicians.MusicianMvp;
import com.acidwater.saher.musicians.model.Musician;
import com.acidwater.saher.musicians.model.MusicianCategory;
import com.acidwater.saher.musicians.model.MusiciansInteractor;
import com.acidwater.saher.musicians.moremusicians.MoreMusicianMvp;
import com.acidwater.saher.musicians.moremusicians.model.MoreMusiciansInteractor;
import com.acidwater.saher.services.ServiceObserver;
import com.acidwater.saher.utils.RxUtils;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import java.util.List;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Doumith on 6/9/2017.
 */
public class MoreMusiciansPresenter extends MvpBasePresenter<MoreMusicianMvp.View> implements MoreMusicianMvp.Presenter {

    private MoreMusiciansInteractor moreMusiciansInteractor;
    private CompositeDisposable compositeDisposable;

    public MoreMusiciansPresenter() {
        moreMusiciansInteractor = new MoreMusiciansInteractor();
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        if (!retainInstance) {
            RxUtils.disposeIfNotNull(compositeDisposable);
        }
    }

    @Override
    public void getMusiciansByCategory(int page, String type, int categoryID) {

        if (isViewAttached()) {
            getView().showLoading(false);
        }

        compositeDisposable.add(moreMusiciansInteractor.getMusiciansByCategory(page, type, categoryID).subscribeWith(new ServiceObserver<DataEntitiesWrapper<List<Musician>>>() {
            @Override
            public void onStart() {

            }

            @Override
            public void onComplete() {

                if (isViewAttached()) {
                    getView().showContent();
                }
            }

            @Override
            public void onNext(DataEntitiesWrapper<List<Musician>> listDataEntitiesWrapper) {
                if (isViewAttached()) {
                    getView().setData(listDataEntitiesWrapper);
                }
            }

            @Override
            public void onError(SaherException e) {

                if (isViewAttached()) {
                    getView().showError(e, false);
                }
            }
        }));
    }


}
