package com.acidwater.saher.musicians.model;

import com.acidwater.saher.musicians.MusicianMvp;
import com.acidwater.saher.data.DataEntitiesWrapper;
import com.acidwater.saher.utils.ConstantStrings;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

/**
 * Created by Doumith on 6/18/2017.
 */
public class MusiciansInteractor implements MusicianMvp.Interactor {


    private MusicianApi placesApi;

    public MusiciansInteractor() {
        String baseURL = ConstantStrings.BASE_URL;
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder().build();
        Retrofit.Builder retrofitBuilder = new Retrofit.Builder().baseUrl(baseURL.endsWith("/") ? baseURL : baseURL.concat("/"))
                .addConverterFactory(GsonConverterFactory.create()).addCallAdapterFactory(RxJava2CallAdapterFactory.create());
        retrofitBuilder.client(okHttpClient);
        Retrofit retrofit = retrofitBuilder.build();
        placesApi = retrofit.create(MusicianApi.class);
    }

    @Override
    public Observable<DataEntitiesWrapper<List<MusicianCategory>>> getMusicians(int page, String type) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("type", type);
        params.put("page", String.valueOf(page));
        params.put("limit", String.valueOf(ConstantStrings.MUSICIANS_PAGING));
        Observable<DataEntitiesWrapper<List<MusicianCategory>>> observable =
                placesApi.getMusicians(params).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());

        return observable;
    }


    public interface MusicianApi {
        @GET(ConstantStrings.SERVICE_MUSICIANS_CATEGORY)
        Observable<DataEntitiesWrapper<List<MusicianCategory>>> getMusicians(@QueryMap Map<String, String> params);


    }


}
