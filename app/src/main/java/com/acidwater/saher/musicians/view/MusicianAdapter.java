package com.acidwater.saher.musicians.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.acidwater.saher.Places.model.Place;
import com.acidwater.saher.R;
import com.acidwater.saher.musicians.model.Musician;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by D on 7/20/2015.
 */

public class MusicianAdapter extends RecyclerView.Adapter<MusicianAdapter.ViewHolder> {

    private List<Musician> list;
    private Context context;
    private onItemClickListener onItemClickListener;

    public MusicianAdapter(Context context, List<Musician> list) {
        if (list == null) {
            this.list = new ArrayList<>();
        } else {
            this.list = list;
        }
        this.context = context;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView title;
        public TextView teaser;
        public ImageView image;


        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            teaser = (TextView) itemLayoutView.findViewById(R.id.musician_teaser);
            title = (TextView) itemLayoutView.findViewById(R.id.musician_title);
            image = (ImageView) itemLayoutView.findViewById(R.id.musician_image);

        }
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_musician, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        Musician musician = list.get(position);
        if (musician.getName() != null) {
            viewHolder.title.setText(musician.getName());

        }
        if (musician.getTeaser() != null) {
            viewHolder.teaser.setText(musician.getTeaser());

        }
        if (musician.getThumbnail() != null) {
            Picasso.with(context).load(musician.getThumbnail()).into(viewHolder.image);

        }

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClickListener != null) {
                    onItemClickListener.onClick(list.get(position), position);
                }
            }
        });

    }

    public List<Musician> getList() {
        return list;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface onItemClickListener {
        void onClick(Musician musician, int position);
    }

    public void setOnItemClickListener(MusicianAdapter.onItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }


}