package com.acidwater.saher.musicians.details.view;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.acidwater.saher.R;
import com.acidwater.saher.events.details.EventDetailsMvp;
import com.acidwater.saher.events.details.model.EventDetails;
import com.acidwater.saher.events.model.Event;
import com.acidwater.saher.events.view.EventAdapter;
import com.acidwater.saher.favorites.FavoritesMvp;
import com.acidwater.saher.favorites.presenter.FavoritesPresenter;
import com.acidwater.saher.musicians.details.MusicianDetailsMvp;
import com.acidwater.saher.musicians.details.model.MusicianDetails;
import com.acidwater.saher.utils.ConstantStrings;
import com.acidwater.saher.utils.DateUtils;
import com.acidwater.saher.utils.GeneralUtils;
import com.acidwater.saher.utils.bus.MainBus;
import com.acidwater.saher.viewholders.AddressViewHolder;
import com.acidwater.saher.viewholders.DateViewHolder;
import com.acidwater.saher.viewholders.DescriptionViewHolder;
import com.acidwater.saher.viewholders.EmailViewHolder;
import com.acidwater.saher.viewholders.EventCoverViewHolder;
import com.acidwater.saher.viewholders.MobileViewHolder;
import com.acidwater.saher.viewholders.MusicianCoverViewHolder;
import com.acidwater.saher.viewholders.MusicianEventsViewHolder;
import com.acidwater.saher.viewholders.MusicianViewHolder;
import com.acidwater.saher.viewholders.PlaceCoverViewHolder;
import com.acidwater.saher.viewholders.TeaserViewHolder;
import com.squareup.picasso.Picasso;

/**
 * Created by Doumith on 7/25/2017.
 */
public class MusicianDetailsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private MusicianDetails musicianDetails;
    private MusicianDetailsMvp.Presenter presenter;
    private FavoritesMvp.Presenter favoritePresenter;


    private final int ROW_COVER = 0;
    private final int ROW_DESCRIPTION = 1;
    private final int ROW_NUMBER = 2;
    private final int ROW_EMAIL = 3;
    private final int ROW_TEASER = 4;
    private final int ROW_EVENTS = 5;
    private final int ITEM_COUNT = 6;

    LayoutInflater inflater;
    private onItemClickListener onItemClickListener;


    public MusicianDetailsAdapter(Context context, MusicianDetails musicianDetails, MusicianDetailsMvp.Presenter presenter) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.musicianDetails = musicianDetails;
        this.presenter = presenter;
        this.favoritePresenter = new FavoritesPresenter(context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;

        switch (viewType) {
            case ROW_COVER:
                viewHolder = new MusicianCoverViewHolder(
                        inflater.inflate(R.layout.row_details_musician_cover, parent, false));
                return viewHolder;

            case ROW_TEASER:
                viewHolder = new TeaserViewHolder(
                        inflater.inflate(R.layout.row_details_teaser, parent, false));
                return viewHolder;

            case ROW_DESCRIPTION:
                viewHolder = new DescriptionViewHolder(
                        inflater.inflate(R.layout.row_details_description, parent, false));
                return viewHolder;

            case ROW_NUMBER:
                viewHolder = new MobileViewHolder(
                        inflater.inflate(R.layout.row_details_mobile_number, parent, false));
                return viewHolder;

            case ROW_EMAIL:
                viewHolder = new EmailViewHolder(
                        inflater.inflate(R.layout.row_details_email, parent, false));
                return viewHolder;

            case ROW_EVENTS:
                viewHolder = new MusicianEventsViewHolder(
                        inflater.inflate(R.layout.row_musician_events, parent, false));
                return viewHolder;


        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        switch (holder.getItemViewType()) {
            case ROW_COVER:
                final MusicianCoverViewHolder musicianCoverViewHolder = (MusicianCoverViewHolder) holder;
                if (!TextUtils.isEmpty(musicianDetails.getCover())) {
                    Picasso.with(context).load(musicianDetails.getCover()).into(musicianCoverViewHolder.cover);
                }
                musicianCoverViewHolder.title.setText(musicianDetails.getName());
                final boolean isFavorite = favoritePresenter.isFavorite(musicianDetails.getId(), ConstantStrings.FAVORITE_MUSICIAN);
                if (isFavorite) {
                    musicianCoverViewHolder.btnFollow.setImageResource(R.drawable.btn_favorite_on);

                } else {
                    musicianCoverViewHolder.btnFollow.setImageResource(R.drawable.btn_follow_on);

                }
                musicianCoverViewHolder.btnFollow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (isFavorite) {
                            presenter.removeFromFavorite(musicianDetails.getId());

                        } else {
                            presenter.addToFavorite(musicianDetails.getId());
                        }
                    }
                });

                musicianCoverViewHolder.btnPlayVideo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        presenter.playVideo(musicianDetails.getVideoURL());
                    }
                });

                break;

            case ROW_TEASER:
                TeaserViewHolder teaserViewHolder = (TeaserViewHolder) holder;
                teaserViewHolder.teaser.setText(musicianDetails.getTeaser());

                break;

            case ROW_DESCRIPTION:
                DescriptionViewHolder descriptionViewHolder = (DescriptionViewHolder) holder;
                if (!TextUtils.isEmpty(musicianDetails.getDescription())) {
                    descriptionViewHolder.description.setText(Html.fromHtml(musicianDetails.getDescription()));
                }

                break;
            case ROW_NUMBER:
                MobileViewHolder mobileViewHolder = (MobileViewHolder) holder;

                if (musicianDetails.getContacts() != null && !musicianDetails.getContacts().isEmpty()) {
                    mobileViewHolder.mobileNumber.setText(musicianDetails.getContacts().get(0).getPhone());
                } else {
                    mobileViewHolder.mobileNumber.setText(context.getString(R.string.not_applicable));
                }

                break;
            case ROW_EMAIL:
                EmailViewHolder emailViewHolder = (EmailViewHolder) holder;

                if (musicianDetails.getContacts() != null && !musicianDetails.getContacts().isEmpty()) {
                    emailViewHolder.email.setText(musicianDetails.getContacts().get(0).getEmail());
                } else {
                    emailViewHolder.email.setText(context.getString(R.string.not_applicable));
                }

                emailViewHolder.facebook.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (!TextUtils.isEmpty(musicianDetails.getFacebookURL())) {
                            GeneralUtils.openIntent(context, musicianDetails.getFacebookURL());
                        }
                    }
                });

                emailViewHolder.instagram.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (!TextUtils.isEmpty(musicianDetails.getInstagramURL())) {
                            GeneralUtils.openIntent(context, musicianDetails.getInstagramURL());
                        }
                    }
                });

                emailViewHolder.youtube.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (!TextUtils.isEmpty(musicianDetails.getYouTubeChanel())) {
                            GeneralUtils.openIntent(context, musicianDetails.getYouTubeChanel());
                        }
                    }
                });

                break;

            case ROW_EVENTS:
                MusicianEventsViewHolder musicianEventsViewHolder = (MusicianEventsViewHolder) holder;

                if (musicianDetails.getEvents() != null && !musicianDetails.getEvents().isEmpty()) {
                    musicianEventsViewHolder.title.setVisibility(View.VISIBLE);
                    musicianEventsViewHolder.recyclerView.setLayoutManager(new LinearLayoutManager(context));
                    EventAdapter eventAdapter = new EventAdapter(context, musicianDetails.getEvents());
                    musicianEventsViewHolder.recyclerView.setAdapter(eventAdapter);
                    eventAdapter.setOnItemClickListener(new EventAdapter.onItemClickListener() {
                        @Override
                        public void onClick(Event event, int position) {
                            if (onItemClickListener != null) {
                                onItemClickListener.onClick(event, position);
                            }
                        }
                    });
                } else {
                    musicianEventsViewHolder.title.setVisibility(View.GONE);
                }

                break;


        }

    }

    @Override
    public int getItemViewType(int position) {
        switch (position) {
            case 0:
                return ROW_COVER;

            case 1:
                return ROW_TEASER;

            case 2:
                return ROW_DESCRIPTION;

            case 3:
                return ROW_NUMBER;

            case 4:
                return ROW_EMAIL;

            case 5:
                return ROW_EVENTS;


        }

        return -1;
    }

    public void setOnItemClickListener(MusicianDetailsAdapter.onItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public interface onItemClickListener {
        void onClick(Event event, int position);
    }

    @Override
    public int getItemCount() {
        return ITEM_COUNT;
    }

}

