package com.acidwater.saher.musicians.model;

import android.os.Parcel;

import com.acidwater.saher.data.Category;
import com.acidwater.saher.data.ListItem;
import com.acidwater.saher.events.model.Event;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Doumith on 7/16/2017.
 */
public class Musician extends ListItem {

    private String type;

    public String getType() {
        return type;
    }


    public void setType(String type) {
        this.type = type;
    }

    public Musician() {
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(this.type);
    }

    protected Musician(Parcel in) {
        super(in);
        this.type = in.readString();
    }

    public static final Creator<Musician> CREATOR = new Creator<Musician>() {
        @Override
        public Musician createFromParcel(Parcel source) {
            return new Musician(source);
        }

        @Override
        public Musician[] newArray(int size) {
            return new Musician[size];
        }
    };
}
