package com.acidwater.saher.musicians.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Doumith on 7/15/2017.
 */
public class MusicianCategory implements Parcelable {

    private int id;
    private String name;
    @SerializedName("artists")
    private List<Musician> musician;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Musician> getMusician() {
        return musician;
    }

    public void setMusician(List<Musician> musician) {
        this.musician = musician;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeTypedList(this.musician);
    }

    public MusicianCategory() {
    }

    protected MusicianCategory(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.musician = in.createTypedArrayList(Musician.CREATOR);
    }

    public static final Creator<MusicianCategory> CREATOR = new Creator<MusicianCategory>() {
        @Override
        public MusicianCategory createFromParcel(Parcel source) {
            return new MusicianCategory(source);
        }

        @Override
        public MusicianCategory[] newArray(int size) {
            return new MusicianCategory[size];
        }
    };
}
