package com.acidwater.saher.musicians;

import com.acidwater.saher.musicians.model.Musician;
import com.acidwater.saher.musicians.model.MusicianCategory;
import com.acidwater.saher.data.DataEntitiesWrapper;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by Doumith on 6/18/2017.
 */
public interface MusicianMvp {

    interface Interactor {

        Observable<DataEntitiesWrapper<List<MusicianCategory>>> getMusicians(int page, String type);


    }

    interface Presenter extends MvpPresenter<View> {

        void getMusicians(int page, String type);

        void openMoreMusicians(String categoryName, int categoryId);

        void loadMoreMusicians(int page, String type);

    }

    interface View extends MvpLceView<DataEntitiesWrapper<List<MusicianCategory>>> {

        void setError(String message);

        void openMoreMusicians(String categoryName, int categoryID);

        void hideLoadMore();

    }
}
