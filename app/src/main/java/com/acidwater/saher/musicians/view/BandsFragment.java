package com.acidwater.saher.musicians.view;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.acidwater.saher.main.MainActivityListener;
import com.acidwater.saher.R;
import com.acidwater.saher.data.DataEntitiesWrapper;
import com.acidwater.saher.musicians.MusicianMvp;
import com.acidwater.saher.musicians.model.Musician;
import com.acidwater.saher.musicians.model.MusicianCategory;
import com.acidwater.saher.musicians.presenter.MusiciansPresenter;
import com.acidwater.saher.utils.ConstantStrings;
import com.acidwater.saher.utils.mvp.LceRetryFragment;
import com.acidwater.saher.widgets.SpacesItemDecoration;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceFragment;

import java.util.List;

/**
 * Created by Doumith on 6/9/2017.
 */
public class BandsFragment extends LceRetryFragment<View, DataEntitiesWrapper<List<MusicianCategory>>, MusicianMvp.View, MusicianMvp.Presenter> implements MusicianMvp.View {

    private int page = 1;
    private RecyclerView recyclerView;
    private MainActivityListener mListener;

    public static BandsFragment newInstance() {
        BandsFragment bandsFragment = new BandsFragment();
        return bandsFragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MainActivityListener) {
            mListener = (MainActivityListener) activity;
        } else {
            throw new RuntimeException("you should implemented MainActivityListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadData(false);
    }

    @Override
    protected void customizeLoadingView(View loadingView) {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View containerView = inflater.inflate(R.layout.fragment_bands, container, false);
        recyclerView = (RecyclerView) containerView.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.addItemDecoration(new SpacesItemDecoration(15));
        return containerView;

    }

    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return null;
    }


    @Override
    public void setData(DataEntitiesWrapper<List<MusicianCategory>> data) {
        MusicianCategoryAdapter musicianCategoryAdapter = new MusicianCategoryAdapter(getContext(), data.getData(), presenter);
        recyclerView.setAdapter(musicianCategoryAdapter);
        musicianCategoryAdapter.setOnItemClickListener(new MusicianCategoryAdapter.onItemClickListener() {
            @Override
            public void onClick(Musician musician, int position) {
                mListener.navigateToMusicianDetails(musician.getId());
            }
        });
    }

    @Override
    public void loadData(boolean pullToRefresh) {
        presenter.getMusicians(page, ConstantStrings.TYPE_BAND);
    }

    @Override
    public MusicianMvp.Presenter createPresenter() {
        return new MusiciansPresenter();
    }

    @Override
    public void retry() {
        loadData(false);
    }


    @Override
    public void setError(String message) {

    }

    @Override
    public void openMoreMusicians(String categoryName, int categoryID) {
        mListener.navigateToMoreBands(categoryName, categoryID);
    }

    @Override
    public void hideLoadMore() {

    }
}
