package com.acidwater.saher.musicians.moremusicians;

import com.acidwater.saher.data.DataEntitiesWrapper;
import com.acidwater.saher.musicians.model.Musician;
import com.acidwater.saher.musicians.model.MusicianCategory;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by Doumith on 6/18/2017.
 */
public interface MoreMusicianMvp {

    interface Interactor {


        Observable<DataEntitiesWrapper<List<Musician>>> getMusiciansByCategory(int page, String type, int categoryID);

    }

    interface Presenter extends MvpPresenter<View> {


        void getMusiciansByCategory(int page, String type, int categoryID);
    }

    interface View extends MvpLceView<DataEntitiesWrapper<List<Musician>>> {

        void setError(String message);

        void hideLoadMore();

    }
}
