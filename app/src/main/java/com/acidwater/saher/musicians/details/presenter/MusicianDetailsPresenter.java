package com.acidwater.saher.musicians.details.presenter;

import android.content.Context;

import com.acidwater.saher.Places.details.PlaceDetailsMvp;
import com.acidwater.saher.Places.details.model.PlaceDetails;
import com.acidwater.saher.Places.details.model.PlaceDetailsInteractor;
import com.acidwater.saher.data.DataEntityWrapper;
import com.acidwater.saher.data.ServerResponse;
import com.acidwater.saher.exceptions.SaherException;
import com.acidwater.saher.favorites.FavoritesMvp;
import com.acidwater.saher.favorites.model.FavoritesInteractor;
import com.acidwater.saher.musicians.details.MusicianDetailsMvp;
import com.acidwater.saher.musicians.details.model.MusicianDetails;
import com.acidwater.saher.musicians.details.model.MusicianDetailsInteractor;
import com.acidwater.saher.profile.model.ProfileInteractor;
import com.acidwater.saher.profile.model.User;
import com.acidwater.saher.services.ServiceObserver;
import com.acidwater.saher.utils.ConstantStrings;
import com.acidwater.saher.utils.GeneralUtils;
import com.acidwater.saher.utils.RxUtils;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Doumith on 7/23/2017.
 */
public class MusicianDetailsPresenter extends MvpBasePresenter<MusicianDetailsMvp.View> implements MusicianDetailsMvp.Presenter {

    private MusicianDetailsInteractor musicianDetailsInteractor;
    private CompositeDisposable compositeDisposable;
    private FavoritesMvp.Interactor favoritesInteractor;
    private ProfileInteractor profileInteractor;
    private Context context;

    public MusicianDetailsPresenter(Context context) {
        this.context = context;
        musicianDetailsInteractor = new MusicianDetailsInteractor();
        compositeDisposable = new CompositeDisposable();
        this.favoritesInteractor = new FavoritesInteractor(context);
        this.profileInteractor = new ProfileInteractor(context);
    }


    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        if (!retainInstance) {
            RxUtils.disposeIfNotNull(compositeDisposable);
        }
    }


    @Override
    public void getMusicianDetails(int musicianID) {
        if (isViewAttached()) {
            getView().showLoading(false);
        }

        compositeDisposable.add(musicianDetailsInteractor.getMusicianDetails(musicianID).subscribeWith(new ServiceObserver<DataEntityWrapper<MusicianDetails>>() {
            @Override
            public void onStart() {

            }

            @Override
            public void onComplete() {

                if (isViewAttached()) {
                    getView().showContent();
                }
            }

            @Override
            public void onNext(DataEntityWrapper<MusicianDetails> musicianDetailsDataEntityWrapper) {
                if (isViewAttached()) {
                    getView().setData(musicianDetailsDataEntityWrapper);
                }
            }

            @Override
            public void onError(SaherException e) {

                if (isViewAttached()) {
                    getView().showError(e, false);
                }
            }
        }));
    }

    @Override
    public void addToFavorite(int musicianID) {
        compositeDisposable.add(favoritesInteractor.addToFavorite(musicianID, ConstantStrings.FAVORITE_MUSICIAN).subscribeWith(new ServiceObserver<ServerResponse>() {
            @Override
            public void onStart() {
                if (isViewAttached()) {
                    getView().showLoadingView();
                }
            }

            @Override
            public void onComplete() {
                if (isViewAttached()) {
                    getView().hideLoadingView();

                }
            }

            @Override
            public void onNext(ServerResponse serverResponse) {
                compositeDisposable.add(profileInteractor.getProfile().subscribeWith(new ServiceObserver<DataEntityWrapper<User>>() {
                    @Override
                    public void onStart() {
                        if (isViewAttached()) {
                            getView().showLoadingView();
                        }
                    }

                    @Override
                    public void onComplete() {
                        if (isViewAttached()) {
                            getView().hideLoadingView();
                        }
                    }

                    @Override
                    public void onNext(DataEntityWrapper<User> userDataEntityWrapper) {
                        if (userDataEntityWrapper != null) {
                            GeneralUtils.saveUserProfile(context, userDataEntityWrapper.getData());
                        }
                        if (isViewAttached()) {
                            getView().refreshData();
                        }
                    }

                    @Override
                    public void onError(SaherException e) {
                        if (isViewAttached()) {
                            getView().hideLoadingView();
                            getView().showError(e, false);
                        }
                    }
                }));
            }

            @Override
            public void onError(SaherException e) {
                if (isViewAttached()) {
                    getView().hideLoadingView();
                    getView().showError(e, false);
                }
            }
        }));
    }

    @Override
    public void removeFromFavorite(int musicianID) {
        compositeDisposable.add(favoritesInteractor.removeFromFavorite(musicianID, ConstantStrings.FAVORITE_MUSICIAN).subscribeWith(new ServiceObserver<ServerResponse>() {
            @Override
            public void onStart() {
                if (isViewAttached()) {
                    getView().showLoadingView();
                }
            }

            @Override
            public void onComplete() {
                if (isViewAttached()) {
                    getView().hideLoadingView();

                }
            }

            @Override
            public void onNext(ServerResponse serverResponse) {
                compositeDisposable.add(profileInteractor.getProfile().subscribeWith(new ServiceObserver<DataEntityWrapper<User>>() {
                    @Override
                    public void onStart() {
                        if (isViewAttached()) {
                            getView().showLoadingView();
                        }
                    }

                    @Override
                    public void onComplete() {
                        if (isViewAttached()) {
                            getView().hideLoadingView();

                        }
                    }

                    @Override
                    public void onNext(DataEntityWrapper<User> userDataEntityWrapper) {
                        if (userDataEntityWrapper != null) {
                            GeneralUtils.saveUserProfile(context, userDataEntityWrapper.getData());
                        }
                        if (isViewAttached()) {
                            getView().refreshData();
                        }
                    }

                    @Override
                    public void onError(SaherException e) {
                        if (isViewAttached()) {
                            getView().hideLoadingView();
                            getView().showError(e, false);
                        }
                    }
                }));
            }

            @Override
            public void onError(SaherException e) {
                if (isViewAttached()) {
                    getView().hideLoadingView();
                    getView().showError(e, false);
                }
            }
        }));
    }

    @Override
    public void playVideo(String videoURL) {
        if (isViewAttached()) {
            getView().playVideo(videoURL);
        }
    }
}
