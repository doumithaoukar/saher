package com.acidwater.saher.musicians.details;

import com.acidwater.saher.Places.details.model.PlaceDetails;
import com.acidwater.saher.data.DataEntityWrapper;
import com.acidwater.saher.musicians.details.model.MusicianDetails;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

import io.reactivex.Observable;

/**
 * Created by Doumith on 6/18/2017.
 */
public interface MusicianDetailsMvp {

    interface Interactor {

        Observable<DataEntityWrapper<MusicianDetails>> getMusicianDetails(int musicianID);

    }

    interface Presenter extends MvpPresenter<View> {

        void getMusicianDetails(int musicianID);

        void addToFavorite(int musicianID);

        void removeFromFavorite(int musicianID);

        void playVideo(String videoURL);

    }

    interface View extends MvpLceView<DataEntityWrapper<MusicianDetails>> {

        void setError(String message);

        void showLoadingView();

        void hideLoadingView();

        void refreshData();

        void playVideo(String videoURL);

    }
}
