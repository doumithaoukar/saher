package com.acidwater.saher.musicians.moremusicians.model;

import com.acidwater.saher.data.DataEntitiesWrapper;
import com.acidwater.saher.musicians.MusicianMvp;
import com.acidwater.saher.musicians.model.Musician;
import com.acidwater.saher.musicians.model.MusicianCategory;
import com.acidwater.saher.musicians.moremusicians.MoreMusicianMvp;
import com.acidwater.saher.utils.ConstantStrings;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

/**
 * Created by Doumith on 6/18/2017.
 */
public class MoreMusiciansInteractor implements MoreMusicianMvp.Interactor {


    private MoreMusicianApi placesApi;

    public MoreMusiciansInteractor() {
        String baseURL = ConstantStrings.BASE_URL;
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder().build();
        Retrofit.Builder retrofitBuilder = new Retrofit.Builder().baseUrl(baseURL.endsWith("/") ? baseURL : baseURL.concat("/"))
                .addConverterFactory(GsonConverterFactory.create()).addCallAdapterFactory(RxJava2CallAdapterFactory.create());
        retrofitBuilder.client(okHttpClient);
        Retrofit retrofit = retrofitBuilder.build();
        placesApi = retrofit.create(MoreMusicianApi.class);
    }


    @Override
    public Observable<DataEntitiesWrapper<List<Musician>>> getMusiciansByCategory(int page, String type, int categoryID) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("categories[]", String.valueOf(categoryID));
        params.put("type", type);
        params.put("page", String.valueOf(page));
        params.put("limit", String.valueOf(ConstantStrings.EVENTS_PAGING));
        Observable<DataEntitiesWrapper<List<Musician>>> observable =
                placesApi.getMusiciansByCategory(params).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());

        return observable;
    }

    public interface MoreMusicianApi {

        @GET(ConstantStrings.SERVICE_MUSICIANS)
        Observable<DataEntitiesWrapper<List<Musician>>> getMusiciansByCategory(@QueryMap Map<String, String> params);
    }


}
