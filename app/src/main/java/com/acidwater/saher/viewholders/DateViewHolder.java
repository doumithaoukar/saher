package com.acidwater.saher.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.acidwater.saher.R;

/**
 * Created by Doumith on 7/25/2017.
 */
public class DateViewHolder extends RecyclerView.ViewHolder {


     public TextView date;

    public DateViewHolder(View itemView) {
        super(itemView);

         date = (TextView) itemView.findViewById(R.id.date);

    }
}
