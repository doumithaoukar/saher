package com.acidwater.saher.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.acidwater.saher.R;

/**
 * Created by Doumith on 7/25/2017.
 */
public class MusicianViewHolder extends RecyclerView.ViewHolder {


    public TextView labelMusician;
    public TextView musician;

    public MusicianViewHolder(View itemView) {
        super(itemView);

        labelMusician = (TextView) itemView.findViewById(R.id.label_musician);
        musician = (TextView) itemView.findViewById(R.id.musician);

    }
}
