package com.acidwater.saher.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.acidwater.saher.R;

/**
 * Created by Doumith on 7/25/2017.
 */
public class TeaserViewHolder extends RecyclerView.ViewHolder {

    public TextView teaser;

    public TeaserViewHolder(View itemView) {
        super(itemView);

        teaser = (TextView) itemView.findViewById(R.id.teaser);

    }
}
