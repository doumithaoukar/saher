package com.acidwater.saher.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.acidwater.saher.R;

/**
 * Created by Doumith on 7/25/2017.
 */
public class AddressViewHolder extends RecyclerView.ViewHolder {


     public TextView address;

    public AddressViewHolder(View itemView) {
        super(itemView);

         address = (TextView) itemView.findViewById(R.id.address);

    }
}
