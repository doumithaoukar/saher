package com.acidwater.saher.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.acidwater.saher.R;

/**
 * Created by Doumith on 7/25/2017.
 */
public class MusicianEventsViewHolder extends RecyclerView.ViewHolder {


    public RecyclerView recyclerView;
    public TextView title;


    public MusicianEventsViewHolder(View itemView) {
        super(itemView);

        title = (TextView) itemView.findViewById(R.id.title);
        recyclerView = (RecyclerView) itemView.findViewById(R.id.recyclerView);

    }
}
