package com.acidwater.saher.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.acidwater.saher.R;

/**
 * Created by Doumith on 7/25/2017.
 */
public class DescriptionViewHolder extends RecyclerView.ViewHolder {


    public TextView description;

    public DescriptionViewHolder(View itemView) {
        super(itemView);

        description = (TextView) itemView.findViewById(R.id.description);

    }
}
