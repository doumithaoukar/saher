package com.acidwater.saher.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.acidwater.saher.R;

/**
 * Created by Doumith on 7/25/2017.
 */
public class EmailViewHolder extends RecyclerView.ViewHolder {


    public TextView email;
    public ImageButton facebook;
    public ImageButton instagram;
    public ImageButton youtube;


    public EmailViewHolder(View itemView) {
        super(itemView);

        email = (TextView) itemView.findViewById(R.id.email);
        facebook = (ImageButton) itemView.findViewById(R.id.facebook);
        instagram = (ImageButton) itemView.findViewById(R.id.instagram);
        youtube = (ImageButton) itemView.findViewById(R.id.youtube);

    }
}
