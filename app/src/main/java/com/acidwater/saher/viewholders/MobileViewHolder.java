package com.acidwater.saher.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.acidwater.saher.R;

/**
 * Created by Doumith on 7/25/2017.
 */
public class MobileViewHolder extends RecyclerView.ViewHolder {


     public TextView mobileNumber;

    public MobileViewHolder(View itemView) {
        super(itemView);

         mobileNumber = (TextView) itemView.findViewById(R.id.mobile_number);

    }
}
