package com.acidwater.saher.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.acidwater.saher.R;

/**
 * Created by Doumith on 7/25/2017.
 */
public class PlaceCoverViewHolder extends RecyclerView.ViewHolder {

    public ImageView cover;
    public ImageButton btnFollow;
    public TextView title;
    public TextView subtitle;
    public ImageButton btnPlayVideo;

    public PlaceCoverViewHolder(View itemView) {
        super(itemView);
        cover = (ImageView) itemView.findViewById(R.id.cover_image);
        btnFollow = (ImageButton) itemView.findViewById(R.id.btn_follow);
        title = (TextView) itemView.findViewById(R.id.title);
        subtitle = (TextView) itemView.findViewById(R.id.subtitle);
        btnPlayVideo = (ImageButton) itemView.findViewById(R.id.btnPlayVideo);
    }
}
