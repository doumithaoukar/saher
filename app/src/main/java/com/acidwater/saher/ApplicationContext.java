package com.acidwater.saher;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.acidwater.saher.utils.SaherNotificationOpenedHandler;
import com.crashlytics.android.Crashlytics;
import com.onesignal.OneSignal;

import io.fabric.sdk.android.Fabric;

/**
 * Created by Doumith on 6/8/2017.
 */
public class ApplicationContext extends Application {
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .setNotificationOpenedHandler(new SaherNotificationOpenedHandler(getApplicationContext()))
                .init();


    }
}
