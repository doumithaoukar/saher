package com.acidwater.saher;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;

import com.acidwater.saher.forgotpassword.view.ForgotPasswordFragment;
import com.acidwater.saher.login.view.LoginFragment;
import com.acidwater.saher.main.MainActivity;
import com.acidwater.saher.registration.view.RegistrationFragment;
import com.acidwater.saher.intro.view.IntroFragment;
import com.facebook.FacebookSdk;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Doumith on 7/6/2017.
 */
public class IntroActivity extends AppCompatActivity implements IntroActivityListener {

    public static final int LOGIN = 1;
    public static final int SIGN_UP = 2;
    public static final int FORGOT_PASSWORD = 3;
    public static final int MAIN_ACTIVITY = 4;

    FragmentManager fragmentManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);

        fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, IntroFragment.newInstance(),
                getString(R.string.tag_signup)).commit();


        PackageInfo info;
        try {
            info = getPackageManager().getPackageInfo("com.acidwater.saher", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String something = new String(Base64.encode(md.digest(), 0));
                //String something = new String(Base64.encodeBytes(md.digest()));
                Log.e("hash key", something);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("no such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("exception", e.toString());
        }
    }

    private void setRegistrationFragment() {

        Fragment fragment;
        if (fragmentManager.findFragmentByTag(getString(R.string.tag_registration)) != null) {
            fragment = fragmentManager.findFragmentByTag(getString(R.string.tag_registration));
        } else {
            fragment = RegistrationFragment.newInstance();
        }

        if (!fragment.isAdded()) {
            fragmentManager.beginTransaction()
                    .add(R.id.fragment_container, fragment, getString(R.string.tag_registration))
                    .addToBackStack(getString(R.string.tag_registration))
                    .commit();
        }

    }

    private void setLoginFragment() {

        Fragment fragment;
        if (fragmentManager.findFragmentByTag(getString(R.string.tag_login)) != null) {
            fragment = fragmentManager.findFragmentByTag(getString(R.string.tag_login));
        } else {
            fragment = LoginFragment.newInstance();
        }

        if (!fragment.isAdded()) {
            fragmentManager.beginTransaction()
                    .add(R.id.fragment_container, fragment, getString(R.string.tag_login))
                    .addToBackStack(getString(R.string.tag_login))
                    .commit();
        }

    }

    private void setForgotPassword() {

        Fragment fragment;
        if (fragmentManager.findFragmentByTag(getString(R.string.tag_forgot_password)) != null) {
            fragment = fragmentManager.findFragmentByTag(getString(R.string.tag_forgot_password));
        } else {
            fragment = ForgotPasswordFragment.newInstance();
        }

        if (!fragment.isAdded()) {
            fragmentManager.beginTransaction()
                    .add(R.id.fragment_container, fragment, getString(R.string.tag_forgot_password))
                    .addToBackStack(getString(R.string.tag_forgot_password))
                    .commit();
        }

    }

    private void launchMainActivity() {
        Intent it = new Intent(this, MainActivity.class);
        it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(it);
        finish();
    }

    @Override
    public void navigateToSection(int sectionID) {

        switch (sectionID) {
            case SIGN_UP:
                setRegistrationFragment();
                break;

            case FORGOT_PASSWORD:
                setForgotPassword();
                break;

            case LOGIN:
                setLoginFragment();
                break;


            case MAIN_ACTIVITY:
                launchMainActivity();
                break;

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (FacebookSdk.isFacebookRequestCode(requestCode) || requestCode == LoginFragment.GOOGLE_PLUS_SIGN_IN) {
            LoginFragment fragment = (LoginFragment) getSupportFragmentManager().findFragmentByTag(getString(R.string.tag_login));
            if (fragment != null) {
                fragment.onActivityResult(requestCode, resultCode, data);
            }

        }
    }

    @Override
    public void onNavigationBackPressed() {
        onBackPressed();
    }
}
