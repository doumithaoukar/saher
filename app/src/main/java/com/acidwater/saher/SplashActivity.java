package com.acidwater.saher;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.acidwater.saher.main.MainActivity;
import com.acidwater.saher.utils.ConstantStrings;
import com.acidwater.saher.utils.GeneralUtils;


public class SplashActivity extends AppCompatActivity {


    private static final long DELAY = 500;
    private static Handler sHandler = new Handler();
    private Runnable mRunnableHome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);
        mRunnableHome = new Runnable() {
            @Override
            public void run() {
                if (GeneralUtils.getSharedPref(SplashActivity.this, ConstantStrings.USER_SESSION).equals(ConstantStrings.user_is_logged_in)) {
                    Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(SplashActivity.this, IntroActivity.class);
                    startActivity(intent);
                }
                finish();
            }
        };
        sHandler.postDelayed(mRunnableHome, DELAY);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (mRunnableHome != null) {
            sHandler.removeCallbacks(mRunnableHome);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mRunnableHome != null) {
            sHandler.removeCallbacks(mRunnableHome);
        }
    }
}
