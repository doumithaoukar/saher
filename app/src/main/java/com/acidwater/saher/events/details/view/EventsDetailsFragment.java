package com.acidwater.saher.events.details.view;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;

import com.acidwater.saher.R;
import com.acidwater.saher.data.Contact;
import com.acidwater.saher.data.DataEntitiesWrapper;
import com.acidwater.saher.data.DataEntityWrapper;
import com.acidwater.saher.events.EventMvp;
import com.acidwater.saher.events.details.EventDetailsMvp;
import com.acidwater.saher.events.details.model.EventDetails;
import com.acidwater.saher.events.details.presenter.EventDetailsPresenter;
import com.acidwater.saher.events.model.Event;
import com.acidwater.saher.events.presenter.EventsPresenter;
import com.acidwater.saher.events.view.EventAdapter;
import com.acidwater.saher.main.MainActivityListener;
import com.acidwater.saher.musicians.model.Musician;
import com.acidwater.saher.utils.GeneralUtils;
import com.acidwater.saher.utils.ScrollAwareFABBehavior;
import com.acidwater.saher.widgets.SpacesItemDecoration;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Doumith on 6/9/2017.
 */
public class EventsDetailsFragment extends MvpLceFragment<View, DataEntityWrapper<EventDetails>, EventDetailsMvp.View, EventDetailsMvp.Presenter> implements EventDetailsMvp.View, View.OnClickListener {

    private RecyclerView recyclerView;
    private MainActivityListener mListener;
    private int eventID;
    private static final String EVENT_ID = "event_id";
    private FloatingActionButton floatingActionButton;
    private RelativeLayout progressBar;
    private EventDetailsAdapter eventDetailsAdapter;
    private EventDetails mEventDetails;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MainActivityListener) {
            mListener = (MainActivityListener) activity;
        } else {
            throw new RuntimeException("you should implemented MainActivityListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public static EventsDetailsFragment newInstance(int eventID) {
        EventsDetailsFragment eventsDetailsFragment = new EventsDetailsFragment();
        Bundle b = new Bundle();
        b.putInt(EVENT_ID, eventID);
        eventsDetailsFragment.setArguments(b);
        return eventsDetailsFragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (savedInstanceState != null) {
            eventID = savedInstanceState.getInt(EVENT_ID);
        } else {
            if (getArguments() != null) {
                eventID = getArguments().getInt(EVENT_ID);
            }
        }
        if (eventID != 0) {
            loadData(false);
        }
    }

    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View containerView = inflater.inflate(R.layout.fragment_details, container, false);
        recyclerView = (RecyclerView) containerView.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        floatingActionButton = (FloatingActionButton) containerView.findViewById(R.id.floatingActionButton);
        progressBar = (RelativeLayout) containerView.findViewById(R.id.main_progress);
        floatingActionButton.setOnClickListener(this);
        ScrollAwareFABBehavior scrollAwareFABBehavior = new ScrollAwareFABBehavior();
        CoordinatorLayout.LayoutParams floatingActionButtonParams = (CoordinatorLayout.LayoutParams) floatingActionButton.getLayoutParams();
        floatingActionButtonParams.setBehavior(scrollAwareFABBehavior);
        floatingActionButton.setLayoutParams(floatingActionButtonParams);
        floatingActionButton.show();
        return containerView;

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(EVENT_ID, eventID);
    }

    @Override
    public void setData(final DataEntityWrapper<EventDetails> data) {
        this.mEventDetails = data.getData();
        eventDetailsAdapter = new EventDetailsAdapter(getContext(), data.getData(), presenter);
        eventDetailsAdapter.setOnCoverClickedListener(new EventDetailsAdapter.OnCoverClickedListener() {
            @Override
            public void onArtistClicked(EventDetails eventDetails) {
                if (data.getData().getMusicians() != null && !data.getData().getMusicians().isEmpty()) {
                    Musician musician = data.getData().getMusicians().get(0);
                    mListener.navigateToMusicianDetails(musician.getId());
                }
            }

            @Override
            public void onPlaceClicked(EventDetails eventDetails) {
                if (eventDetails.getPlace() != null) {
                    mListener.navigateToPlaceDetails(eventDetails.getPlace().getId());
                }
            }
        });


        recyclerView.setAdapter(eventDetailsAdapter);
        if (mEventDetails.getContacts() == null || mEventDetails.getContacts().isEmpty()) {
            floatingActionButton.setVisibility(View.GONE);
        } else {
            floatingActionButton.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void loadData(boolean pullToRefresh) {
        presenter.getEventDetails(eventID);
    }

    @Override
    public EventDetailsMvp.Presenter createPresenter() {
        return new EventDetailsPresenter(getContext());
    }


    @Override
    public void setError(String message) {

    }

    @Override
    public void showLoadingView() {
        progressBar.setVisibility(View.VISIBLE);

    }

    @Override
    public void hideLoadingView() {
        progressBar.setVisibility(View.GONE);

    }

    @Override
    public void refreshData() {
        if (eventDetailsAdapter != null) {
            eventDetailsAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void playVideo(String videoURL) {

        if (!TextUtils.isEmpty(videoURL)) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(videoURL)));
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.floatingActionButton:
                if (mEventDetails != null && mEventDetails.getContacts() != null) {
                    showContactListDialog(mEventDetails.getContacts());
                }
                break;
        }
    }

    public void showContactListDialog(ArrayList<Contact> contacts) {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(getActivity());
        builderSingle.setTitle(getString(R.string.select_contact));

        final ArrayAdapter<Contact> arrayAdapter = new ArrayAdapter<Contact>(getContext(), R.layout.row_contact_dialog);
        for (Contact contact : contacts) {
            arrayAdapter.add(contact);
        }
        builderSingle.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Contact contact = arrayAdapter.getItem(which);
                GeneralUtils.callNumber(getContext(), contact.getPhone());
            }
        });
        builderSingle.show();
    }
}
