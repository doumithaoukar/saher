package com.acidwater.saher.events.view;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.acidwater.saher.main.MainActivityListener;
import com.acidwater.saher.R;
import com.acidwater.saher.events.EventMvp;
import com.acidwater.saher.events.model.Event;
import com.acidwater.saher.events.presenter.EventsPresenter;
import com.acidwater.saher.data.DataEntitiesWrapper;
import com.acidwater.saher.utils.ConstantStrings;
import com.acidwater.saher.utils.mvp.LceRetryFragment;
import com.acidwater.saher.widgets.SpacesItemDecoration;
import com.jcodecraeer.xrecyclerview.XRecyclerView;


import java.util.List;

/**
 * Created by Doumith on 6/9/2017.
 */
public class EventsFragment extends LceRetryFragment<View, DataEntitiesWrapper<List<Event>>, EventMvp.View, EventMvp.Presenter> implements EventMvp.View {

    private int page = 1;
    private XRecyclerView recyclerView;
    private MainActivityListener mListener;
    private LinearLayoutManager layoutManager;
    private EventAdapter eventAdapter;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MainActivityListener) {
            mListener = (MainActivityListener) activity;
        } else {
            throw new RuntimeException("you should implemented MainActivityListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public static EventsFragment newInstance() {
        EventsFragment eventsFragment = new EventsFragment();
        return eventsFragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        loadData(false);
    }

    @Override
    protected void customizeLoadingView(View loadingView) {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View containerView = inflater.inflate(R.layout.fragment_events, container, false);
        // reset all data after fragment is destroyed
        page = 1;
        swipeRefreshLayout = (SwipeRefreshLayout) containerView.findViewById(R.id.swipe_refresh_layout);
        recyclerView = (XRecyclerView) containerView.findViewById(R.id.recyclerView);
        layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new SpacesItemDecoration(35));
        recyclerView.setPullRefreshEnabled(false);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                page = 1;
                presenter.getEvents(page);
            }
        });
        return containerView;

    }

    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return null;
    }

    @Override
    public void setData(DataEntitiesWrapper<List<Event>> data) {
        if (page == 1) {
            eventAdapter = new EventAdapter(getContext(), data.getData());
            eventAdapter.setOnItemClickListener(new EventAdapter.onItemClickListener() {
                @Override
                public void onClick(Event event, int position) {
                    mListener.navigateToEventDetails(event.getId());
                }
            });
            recyclerView.setAdapter(eventAdapter);
            recyclerView.setLoadingMoreEnabled(true);
            recyclerView.setLoadingListener(new XRecyclerView.LoadingListener() {
                @Override
                public void onRefresh() {
                    //refresh data here
                }

                @Override
                public void onLoadMore() {
                    // load more data here
                    page++;
                    presenter.loadMoreEvents(page);
                }
            });
        } else {
            eventAdapter.getList().addAll(data.getData());
            eventAdapter.notifyDataSetChanged();
            hideLoadMore();
            if (data.getData().size() < ConstantStrings.EVENTS_PAGING) {
                recyclerView.setLoadingMoreEnabled(false);
            }
        }

        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void loadData(boolean pullToRefresh) {
        presenter.getEvents(page);
    }

    @Override
    public EventMvp.Presenter createPresenter() {
        return new EventsPresenter();
    }

    @Override
    public void retry() {
        page = 1;
        loadData(false);
    }


    @Override
    public void setError(String message) {

    }

    @Override
    public void hideLoadMore() {
        recyclerView.loadMoreComplete();
    }
}
