package com.acidwater.saher.events.details.model;

import com.acidwater.saher.data.DataEntitiesWrapper;
import com.acidwater.saher.data.DataEntityWrapper;
import com.acidwater.saher.events.EventMvp;
import com.acidwater.saher.events.details.EventDetailsMvp;
import com.acidwater.saher.events.model.Event;
import com.acidwater.saher.utils.ConstantStrings;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

/**
 * Created by Doumith on 6/18/2017.
 */
public class EventDetailsInteractor implements EventDetailsMvp.Interactor {


    private EventDetailsApi eventDetailsApi;

    public EventDetailsInteractor() {
        String baseURL = ConstantStrings.BASE_URL;
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder().build();
        Retrofit.Builder retrofitBuilder = new Retrofit.Builder().baseUrl(baseURL.endsWith("/") ? baseURL : baseURL.concat("/"))
                .addConverterFactory(GsonConverterFactory.create()).addCallAdapterFactory(RxJava2CallAdapterFactory.create());
        retrofitBuilder.client(okHttpClient);
        Retrofit retrofit = retrofitBuilder.build();
        eventDetailsApi = retrofit.create(EventDetailsApi.class);
    }


    @Override
    public Observable<DataEntityWrapper<EventDetails>> getEventDetails(int eventID) {
        String URL = ConstantStrings.BASE_URL.endsWith("/") ? ConstantStrings.BASE_URL : ConstantStrings.BASE_URL.concat("/");
        URL += ConstantStrings.SERVICE_EVENT_DETAILS + "/" + String.valueOf(eventID);

        Observable<DataEntityWrapper<EventDetails>> observable =
                eventDetailsApi.getEventDetails(String.valueOf(URL)).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());

        return observable;
    }

    public interface EventDetailsApi {
        @GET
        Observable<DataEntityWrapper<EventDetails>> getEventDetails(@Url String url);
    }


}
