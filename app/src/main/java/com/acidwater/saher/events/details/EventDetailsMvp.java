package com.acidwater.saher.events.details;

import com.acidwater.saher.data.DataEntitiesWrapper;
import com.acidwater.saher.data.DataEntityWrapper;
import com.acidwater.saher.events.details.model.EventDetails;
import com.acidwater.saher.events.model.Event;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by Doumith on 6/18/2017.
 */
public interface EventDetailsMvp {

    interface Interactor {

        Observable<DataEntityWrapper<EventDetails>> getEventDetails(int eventID);

    }

    interface Presenter extends MvpPresenter<View> {

        void getEventDetails(int eventID);

        void addToFavorite(int eventID);

        void removeFromFavorite(int eventID);

        void playVideo(String videoURL);

    }

    interface View extends MvpLceView<DataEntityWrapper<EventDetails>> {

        void setError(String message);

        void showLoadingView();

        void hideLoadingView();

        void refreshData();

        void playVideo(String videoURL);

    }
}
