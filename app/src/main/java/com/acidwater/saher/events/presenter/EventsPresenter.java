package com.acidwater.saher.events.presenter;

import com.acidwater.saher.events.EventMvp;
import com.acidwater.saher.events.model.Event;
import com.acidwater.saher.events.model.EventInteractor;
import com.acidwater.saher.exceptions.SaherException;
import com.acidwater.saher.services.ServiceObserver;
import com.acidwater.saher.utils.ConstantStrings;
import com.acidwater.saher.utils.RxUtils;
import com.acidwater.saher.data.DataEntitiesWrapper;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import java.util.List;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Doumith on 6/9/2017.
 */
public class EventsPresenter extends MvpBasePresenter<EventMvp.View> implements EventMvp.Presenter {

    private EventInteractor eventInteractor;
    private CompositeDisposable compositeDisposable;


    public EventsPresenter() {
        eventInteractor = new EventInteractor();
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void getEvents(int page) {

        if (isViewAttached()) {
            getView().showLoading(false);
        }

        compositeDisposable.add(eventInteractor.getEvents(page).subscribeWith(new ServiceObserver<DataEntitiesWrapper<List<Event>>>() {
            @Override
            public void onStart() {

            }

            @Override
            public void onComplete() {

                if (isViewAttached()) {
                    getView().showContent();
                }
            }

            @Override
            public void onNext(DataEntitiesWrapper<List<Event>> listDataEntitiesWrapper) {
                if (isViewAttached()) {
                    getView().setData(listDataEntitiesWrapper);
                }
            }

            @Override
            public void onError(SaherException e) {

                if (isViewAttached()) {
                    getView().showError(e, false);
                }
            }
        }));
    }

    @Override
    public void loadMoreEvents(int page) {


        compositeDisposable.add(eventInteractor.getEvents(page).subscribeWith(new ServiceObserver<DataEntitiesWrapper<List<Event>>>() {
            @Override
            public void onStart() {

            }

            @Override
            public void onComplete() {
                if (isViewAttached()) {
                    getView().hideLoadMore();
                }

            }

            @Override
            public void onNext(DataEntitiesWrapper<List<Event>> listDataEntitiesWrapper) {
                if (isViewAttached()) {
                    getView().setData(listDataEntitiesWrapper);
                }
            }

            @Override
            public void onError(SaherException e) {

                if (isViewAttached()) {
                    getView().hideLoadMore();
                }
            }
        }));
    }


    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        RxUtils.disposeIfNotNull(compositeDisposable);

    }
}
