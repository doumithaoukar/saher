package com.acidwater.saher.events;

import com.acidwater.saher.events.model.Event;
import com.acidwater.saher.data.DataEntitiesWrapper;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by Doumith on 6/18/2017.
 */
public interface EventMvp {

    interface Interactor {

        Observable<DataEntitiesWrapper<List<Event>>> getEvents(int page);

    }

    interface Presenter extends MvpPresenter<View> {

        void getEvents(int page);

        void loadMoreEvents(int page);
    }

    interface View extends MvpLceView<DataEntitiesWrapper<List<Event>>> {

        void setError(String message);

        void hideLoadMore();

    }
}
