package com.acidwater.saher.events.details.model;

import android.os.Parcel;

import com.acidwater.saher.Places.model.Place;
import com.acidwater.saher.data.Contact;
import com.acidwater.saher.events.model.Event;
import com.acidwater.saher.musicians.model.Musician;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Doumith on 7/23/2017.
 */
public class EventDetails extends Event {


    @SerializedName("address")
    private String address;


    @SerializedName("contacts")
    private ArrayList<Contact> contacts;


    @SerializedName("pub")
    private Place place;

    @SerializedName("artists")
    private ArrayList<Musician> musicians;


    @SerializedName("youtube_url")
    private String videoURL;

    @SerializedName("facebook_url")
    private String facebookURL;

    @SerializedName("site_url")
    private String websiteURL;

    @SerializedName("description")
    private String description;

    @SerializedName("video")
    private String videoTrailer;


    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

    public ArrayList<Musician> getMusicians() {
        return musicians;
    }

    public void setMusicians(ArrayList<Musician> musicians) {
        this.musicians = musicians;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public ArrayList<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }

    public EventDetails() {
    }

    public String getVideoURL() {
        return videoURL;
    }

    public void setVideoURL(String videoURL) {
        this.videoURL = videoURL;
    }

    public String getFacebookURL() {
        return facebookURL;
    }

    public void setFacebookURL(String facebookURL) {
        this.facebookURL = facebookURL;
    }

    public String getWebsiteURL() {
        return websiteURL;
    }

    public void setWebsiteURL(String websiteURL) {
        this.websiteURL = websiteURL;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVideoTrailer() {
        return videoTrailer;
    }

    public void setVideoTrailer(String videoTrailer) {
        this.videoTrailer = videoTrailer;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(this.address);
        dest.writeTypedList(this.contacts);
        dest.writeParcelable(this.place, flags);
        dest.writeTypedList(this.musicians);
        dest.writeString(this.videoURL);
        dest.writeString(this.facebookURL);
        dest.writeString(this.websiteURL);
        dest.writeString(this.description);
        dest.writeString(this.videoTrailer);
    }

    protected EventDetails(Parcel in) {
        super(in);
        this.address = in.readString();
        this.contacts = in.createTypedArrayList(Contact.CREATOR);
        this.place = in.readParcelable(Place.class.getClassLoader());
        this.musicians = in.createTypedArrayList(Musician.CREATOR);
        this.videoURL = in.readString();
        this.facebookURL = in.readString();
        this.websiteURL = in.readString();
        this.description = in.readString();
        this.videoTrailer = in.readString();
    }

    public static final Creator<EventDetails> CREATOR = new Creator<EventDetails>() {
        @Override
        public EventDetails createFromParcel(Parcel source) {
            return new EventDetails(source);
        }

        @Override
        public EventDetails[] newArray(int size) {
            return new EventDetails[size];
        }
    };
}
