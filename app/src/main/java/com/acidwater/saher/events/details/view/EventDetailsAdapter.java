package com.acidwater.saher.events.details.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.acidwater.saher.R;
import com.acidwater.saher.events.details.EventDetailsMvp;
import com.acidwater.saher.events.details.model.EventDetails;
import com.acidwater.saher.favorites.FavoritesMvp;
import com.acidwater.saher.favorites.presenter.FavoritesPresenter;
import com.acidwater.saher.utils.ConstantStrings;
import com.acidwater.saher.utils.DateUtils;
import com.acidwater.saher.utils.GeneralUtils;
import com.acidwater.saher.viewholders.AddressViewHolder;
import com.acidwater.saher.viewholders.DateViewHolder;
import com.acidwater.saher.viewholders.DescriptionViewHolder;
import com.acidwater.saher.viewholders.EmailViewHolder;
import com.acidwater.saher.viewholders.EventCoverViewHolder;
import com.acidwater.saher.viewholders.MobileViewHolder;
import com.acidwater.saher.viewholders.MusicianViewHolder;
import com.acidwater.saher.viewholders.TeaserViewHolder;
import com.squareup.picasso.Picasso;

/**
 * Created by Doumith on 7/25/2017.
 */
public class EventDetailsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private EventDetails eventDetails;
    private EventDetailsMvp.Presenter presenter;
    private FavoritesMvp.Presenter favoritePresenter;


    private final int ROW_COVER = 0;
    private final int ROW_TEASER = 1;
    private final int ROW_DATE = 2;
    private final int ROW_ADDRESS = 3;
    private final int ROW_NUMBER = 4;
    private final int ROW_EMAIL = 5;
    private final int ROW_DESCRIPTION = 6;
    //    private final int ROW_MUSICIAN = 7;
    private final int ITEM_COUNT = 7;
    private OnCoverClickedListener onCoverClickedListener;
    LayoutInflater inflater;


    public EventDetailsAdapter(Context context, EventDetails eventDetails, EventDetailsMvp.Presenter presenter) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.eventDetails = eventDetails;
        this.presenter = presenter;
        this.favoritePresenter = new FavoritesPresenter(context);

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;

        switch (viewType) {
            case ROW_COVER:
                viewHolder = new EventCoverViewHolder(
                        inflater.inflate(R.layout.row_details_event_cover, parent, false));
                return viewHolder;

            case ROW_TEASER:
                viewHolder = new TeaserViewHolder(
                        inflater.inflate(R.layout.row_details_teaser, parent, false));
                return viewHolder;


            case ROW_DATE:
                viewHolder = new DateViewHolder(
                        inflater.inflate(R.layout.row_details_date, parent, false));
                return viewHolder;

            case ROW_DESCRIPTION:
                viewHolder = new DescriptionViewHolder(
                        inflater.inflate(R.layout.row_details_description, parent, false));
                return viewHolder;

            case ROW_NUMBER:
                viewHolder = new MobileViewHolder(
                        inflater.inflate(R.layout.row_details_mobile_number, parent, false));
                return viewHolder;

            case ROW_EMAIL:
                viewHolder = new EmailViewHolder(
                        inflater.inflate(R.layout.row_details_email, parent, false));
                return viewHolder;

            case ROW_ADDRESS:
                viewHolder = new AddressViewHolder(
                        inflater.inflate(R.layout.row_details_address, parent, false));
                return viewHolder;

        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        switch (holder.getItemViewType()) {
            case ROW_COVER:
                final EventCoverViewHolder eventCoverViewHolder = (EventCoverViewHolder) holder;
                Picasso.with(context).load(eventDetails.getThumbnail()).into(eventCoverViewHolder.cover);
                eventCoverViewHolder.title.setText(eventDetails.getName());
                eventCoverViewHolder.subtitle.setText(eventDetails.getCityName());
                eventCoverViewHolder.subtitle.setText(eventDetails.getPlaceName() + " - " + eventDetails.getCityName());
                eventCoverViewHolder.day.setText(String.valueOf(DateUtils.getDay(eventDetails.getDate())));
                eventCoverViewHolder.month.setText(DateUtils.getMonthName(eventDetails.getDate()));

                if (eventDetails.isPlaceClickable()) {

                    eventCoverViewHolder.title.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (onCoverClickedListener != null) {
                                onCoverClickedListener.onArtistClicked(eventDetails);
                            }
                        }
                    });

                    eventCoverViewHolder.subtitle.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (onCoverClickedListener != null) {
                                onCoverClickedListener.onPlaceClicked(eventDetails);
                            }
                        }
                    });
                }

                final boolean isFavorite = favoritePresenter.isFavorite(eventDetails.getId(), ConstantStrings.FAVORITE_EVENT);
                if (isFavorite) {
                    eventCoverViewHolder.btnFollow.setImageResource(R.drawable.btn_favorite_on);

                } else {
                    eventCoverViewHolder.btnFollow.setImageResource(R.drawable.btn_follow_on);

                }
                eventCoverViewHolder.btnFollow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (isFavorite) {
                            presenter.removeFromFavorite(eventDetails.getId());

                        } else {
                            presenter.addToFavorite(eventDetails.getId());
                        }
                    }
                });

                eventCoverViewHolder.btnPlayVideo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        presenter.playVideo(eventDetails.getVideoURL());
                    }
                });

                break;

            case ROW_TEASER:
                TeaserViewHolder teaserViewHolder = (TeaserViewHolder) holder;
                teaserViewHolder.teaser.setText(eventDetails.getTeaser());

                break;
            case ROW_DATE:
                DateViewHolder dateViewHolder = (DateViewHolder) holder;
                int day = DateUtils.getDay(eventDetails.getDate());
                String month = DateUtils.getMonthName(eventDetails.getDate());
                int year = DateUtils.getYear(eventDetails.getDate());
                String time = DateUtils.getTime(eventDetails.getDate());
                String fullDate = day + " " + month + " " + year + " - " + time + " " + DateUtils.getAmPM(eventDetails.getDate());
                dateViewHolder.date.setText(fullDate);
                break;
            case ROW_DESCRIPTION:
                DescriptionViewHolder descriptionViewHolder = (DescriptionViewHolder) holder;

                if (!TextUtils.isEmpty(eventDetails.getDescription())) {
                    descriptionViewHolder.description.setText(Html.fromHtml(eventDetails.getDescription()));
                }

                break;
            case ROW_NUMBER:
                MobileViewHolder mobileViewHolder = (MobileViewHolder) holder;

                if (eventDetails.getContacts() != null && !eventDetails.getContacts().isEmpty()) {
                    mobileViewHolder.mobileNumber.setText(eventDetails.getContacts().get(0).getPhone());
                }
                break;
            case ROW_EMAIL:
                EmailViewHolder emailViewHolder = (EmailViewHolder) holder;

                // no youtube for event
                emailViewHolder.youtube.setVisibility(View.GONE);

                if (eventDetails.getContacts() != null && !eventDetails.getContacts().isEmpty()) {
                    emailViewHolder.email.setText(eventDetails.getContacts().get(0).getEmail());
                }

                emailViewHolder.facebook.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (!TextUtils.isEmpty(eventDetails.getFacebookURL())) {
                            GeneralUtils.openIntent(context, eventDetails.getFacebookURL());
                        }
                    }
                });

                emailViewHolder.instagram.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (!TextUtils.isEmpty(eventDetails.getWebsiteURL())) {
                            GeneralUtils.openIntent(context, eventDetails.getWebsiteURL());
                        }
                    }
                });

                break;
            case ROW_ADDRESS:
                AddressViewHolder addressViewHolder = (AddressViewHolder) holder;

                String fullAddress = "";
                if (!TextUtils.isEmpty(eventDetails.getPlaceName())) {
                    fullAddress += eventDetails.getPlaceName();
                }
                if (eventDetails.getPlace() != null && !TextUtils.isEmpty(eventDetails.getPlace().getCity())) {
                    fullAddress += " " + eventDetails.getPlace().getCity();
                }
                fullAddress += " " + eventDetails.getAddress();
                addressViewHolder.address.setText(fullAddress);

                break;
//            case ROW_MUSICIAN:
//                MusicianViewHolder musicianViewHolder = (MusicianViewHolder) holder;
//
//                if (eventDetails.getMusicians() != null && !eventDetails.getMusicians().isEmpty()) {
//                    musicianViewHolder.musician.setText(eventDetails.getMusicians().get(0).getName());
//                }

//                break;
        }

    }

    @Override
    public int getItemViewType(int position) {
        switch (position) {
            case 0:
                return ROW_COVER;

            case 1:
                return ROW_TEASER;

            case 2:
                return ROW_DATE;

            case 3:
                return ROW_ADDRESS;

            case 4:
                return ROW_NUMBER;

            case 5:
                return ROW_EMAIL;

            case 6:
                return ROW_DESCRIPTION;


        }

        return -1;
    }

    public void setOnCoverClickedListener(OnCoverClickedListener onCoverClickedListener) {
        this.onCoverClickedListener = onCoverClickedListener;
    }

    public interface OnCoverClickedListener {
        void onArtistClicked(EventDetails eventDetails);

        void onPlaceClicked(EventDetails eventDetails);
    }

    @Override
    public int getItemCount() {
        return ITEM_COUNT;
    }

}

