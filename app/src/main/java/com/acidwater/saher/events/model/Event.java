package com.acidwater.saher.events.model;

import android.os.Parcel;

import com.acidwater.saher.data.ListItem;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Doumith on 6/9/2017.
 */
public class Event extends ListItem {

    @SerializedName("city")
    private String cityName;

    @SerializedName("pub_name")
    private String placeName;


    @SerializedName("event_date")
    private String date;

    private boolean isPlaceClickable;

    public Event() {
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }


    public boolean isPlaceClickable() {
        return isPlaceClickable;
    }

    public void setPlaceClickable(boolean placeClickable) {
        isPlaceClickable = placeClickable;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(this.cityName);
        dest.writeString(this.placeName);
        dest.writeString(this.date);
        dest.writeByte(this.isPlaceClickable ? (byte) 1 : (byte) 0);
    }

    protected Event(Parcel in) {
        super(in);
        this.cityName = in.readString();
        this.placeName = in.readString();
        this.date = in.readString();
        this.isPlaceClickable = in.readByte() != 0;
    }

    public static final Creator<Event> CREATOR = new Creator<Event>() {
        @Override
        public Event createFromParcel(Parcel source) {
            return new Event(source);
        }

        @Override
        public Event[] newArray(int size) {
            return new Event[size];
        }
    };
}
