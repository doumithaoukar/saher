package com.acidwater.saher.events.details.presenter;

import android.content.Context;

import com.acidwater.saher.data.DataEntityWrapper;
import com.acidwater.saher.data.ServerResponse;
import com.acidwater.saher.events.details.EventDetailsMvp;
import com.acidwater.saher.events.details.model.EventDetails;
import com.acidwater.saher.events.details.model.EventDetailsInteractor;
import com.acidwater.saher.exceptions.SaherException;
import com.acidwater.saher.favorites.FavoritesMvp;
import com.acidwater.saher.favorites.model.FavoritesInteractor;
import com.acidwater.saher.profile.model.ProfileInteractor;
import com.acidwater.saher.profile.model.User;
import com.acidwater.saher.services.ServiceObserver;
import com.acidwater.saher.utils.ConstantStrings;
import com.acidwater.saher.utils.GeneralUtils;
import com.acidwater.saher.utils.RxUtils;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;


import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Doumith on 7/23/2017.
 */
public class EventDetailsPresenter extends MvpBasePresenter<EventDetailsMvp.View> implements EventDetailsMvp.Presenter {

    private EventDetailsInteractor eventDetailsInteractore;
    private CompositeDisposable compositeDisposable;
    private FavoritesMvp.Interactor favoritesInteractor;
    private ProfileInteractor profileInteractor;
    private Context context;

    public EventDetailsPresenter(Context context) {
        this.context = context;
        eventDetailsInteractore = new EventDetailsInteractor();
        compositeDisposable = new CompositeDisposable();
        this.favoritesInteractor = new FavoritesInteractor(context);
        this.profileInteractor = new ProfileInteractor(context);

    }

    @Override
    public void getEventDetails(int eventID) {
        if (isViewAttached()) {
            getView().showLoading(false);
        }

        compositeDisposable.add(eventDetailsInteractore.getEventDetails(eventID).subscribeWith(new ServiceObserver<DataEntityWrapper<EventDetails>>() {
            @Override
            public void onStart() {

            }

            @Override
            public void onComplete() {

                if (isViewAttached()) {
                    getView().showContent();
                }
            }

            @Override
            public void onNext(DataEntityWrapper<EventDetails> eventDetailsDataEntityWrapper) {
                if (isViewAttached()) {
                    getView().setData(eventDetailsDataEntityWrapper);
                }

            }

            @Override
            public void onError(SaherException e) {

                if (isViewAttached()) {
                    getView().showError(e, false);
                }
            }
        }));
    }

    @Override
    public void addToFavorite(int eventID) {
        compositeDisposable.add(favoritesInteractor.addToFavorite(eventID, ConstantStrings.FAVORITE_EVENT).subscribeWith(new ServiceObserver<ServerResponse>() {
            @Override
            public void onStart() {
                if (isViewAttached()) {
                    getView().showLoadingView();
                }
            }

            @Override
            public void onComplete() {
                if (isViewAttached()) {
                    getView().hideLoadingView();

                }
            }

            @Override
            public void onNext(ServerResponse serverResponse) {
                compositeDisposable.add(profileInteractor.getProfile().subscribeWith(new ServiceObserver<DataEntityWrapper<User>>() {
                    @Override
                    public void onStart() {
                        if (isViewAttached()) {
                            getView().showLoadingView();
                        }
                    }

                    @Override
                    public void onComplete() {
                        if (isViewAttached()) {
                            getView().hideLoadingView();

                        }
                    }

                    @Override
                    public void onNext(DataEntityWrapper<User> userDataEntityWrapper) {
                        if (userDataEntityWrapper != null) {
                            GeneralUtils.saveUserProfile(context, userDataEntityWrapper.getData());
                        }
                        if (isViewAttached()) {
                            getView().refreshData();
                        }
                    }

                    @Override
                    public void onError(SaherException e) {
                        if (isViewAttached()) {
                            getView().hideLoadingView();
                            getView().showError(e, false);
                        }
                    }
                }));
            }

            @Override
            public void onError(SaherException e) {
                if (isViewAttached()) {
                    getView().hideLoadingView();
                    getView().showError(e, false);
                }
            }
        }));
    }

    @Override
    public void removeFromFavorite(int eventID) {
        compositeDisposable.add(favoritesInteractor.removeFromFavorite(eventID, ConstantStrings.FAVORITE_EVENT).subscribeWith(new ServiceObserver<ServerResponse>() {
            @Override
            public void onStart() {
                if (isViewAttached()) {
                    getView().showLoadingView();
                }
            }

            @Override
            public void onComplete() {
                if (isViewAttached()) {
                    getView().hideLoadingView();

                }
            }

            @Override
            public void onNext(ServerResponse serverResponse) {
                compositeDisposable.add(profileInteractor.getProfile().subscribeWith(new ServiceObserver<DataEntityWrapper<User>>() {
                    @Override
                    public void onStart() {
                        if (isViewAttached()) {
                            getView().showLoadingView();
                        }
                    }

                    @Override
                    public void onComplete() {
                        if (isViewAttached()) {
                            getView().hideLoadingView();

                        }
                    }

                    @Override
                    public void onNext(DataEntityWrapper<User> userDataEntityWrapper) {
                        if (userDataEntityWrapper != null) {
                            GeneralUtils.saveUserProfile(context, userDataEntityWrapper.getData());
                        }

                        if (isViewAttached()) {
                            getView().refreshData();
                        }
                    }

                    @Override
                    public void onError(SaherException e) {
                        if (isViewAttached()) {
                            getView().hideLoadingView();
                            getView().showError(e, false);
                        }
                    }
                }));
            }

            @Override
            public void onError(SaherException e) {
                if (isViewAttached()) {
                    getView().hideLoadingView();
                    getView().showError(e, false);
                }
            }
        }));
    }

    @Override
    public void playVideo(String videoURL) {
        if (isViewAttached()) {
            getView().playVideo(videoURL);
        }
    }

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        if (!retainInstance) {
            RxUtils.disposeIfNotNull(compositeDisposable);
        }
    }
}
