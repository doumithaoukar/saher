package com.acidwater.saher.events.model;

import com.acidwater.saher.events.EventMvp;
import com.acidwater.saher.utils.ConstantStrings;
import com.acidwater.saher.data.DataEntitiesWrapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

/**
 * Created by Doumith on 6/18/2017.
 */
public class EventInteractor implements EventMvp.Interactor {


    private EventApi eventApi;

    public EventInteractor() {
        String baseURL = ConstantStrings.BASE_URL;
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder().build();
        Retrofit.Builder retrofitBuilder = new Retrofit.Builder().baseUrl(baseURL.endsWith("/") ? baseURL : baseURL.concat("/"))
                .addConverterFactory(GsonConverterFactory.create()).addCallAdapterFactory(RxJava2CallAdapterFactory.create());
        retrofitBuilder.client(okHttpClient);
        Retrofit retrofit = retrofitBuilder.build();
        eventApi = retrofit.create(EventApi.class);
    }

    @Override
    public Observable<DataEntitiesWrapper<List<Event>>> getEvents(int page) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("page", String.valueOf(page));
        params.put("limit", String.valueOf(ConstantStrings.EVENTS_PAGING));
        Observable<DataEntitiesWrapper<List<Event>>> observable =
                eventApi.getEvents(params).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());

        return observable;
    }

    public interface EventApi {
        @GET(ConstantStrings.SERVICE_EVENTS)
        Observable<DataEntitiesWrapper<List<Event>>> getEvents(@QueryMap Map<String, String> params);
    }


}
