package com.acidwater.saher.exceptions;

/**
 * Created by Doumith on 6/25/2017.
 */
public class SaherException extends Exception {


    private static final long serialVersionUID = 1L;
    public Reason reason;
    public String errorMessage;

    public SaherException(Reason reason) {
        super();
        this.reason = reason;

    }

    public SaherException(Reason reason, String errorMessage) {
        super();
        this.reason = reason;
        this.errorMessage = errorMessage;
    }

    public enum Reason {
          CONNECTION_ERROR, SERVICE_ERROR,  NOT_LATEST_VERSION, NOT_AUTHORIZED, NOT_AUTHENTICATED, NOT_FOUND, BAD_REQUEST
    }
}
