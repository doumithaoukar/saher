package com.acidwater.saher.changePassword.presenter;

import android.content.Context;
import android.text.TextUtils;
import android.widget.Toast;

import com.acidwater.saher.R;
import com.acidwater.saher.changePassword.ChangePasswordMvp;
import com.acidwater.saher.data.DataEntityWrapper;
import com.acidwater.saher.data.ServerResponse;
import com.acidwater.saher.exceptions.SaherException;
import com.acidwater.saher.profile.ProfileMvp;
import com.acidwater.saher.profile.model.ProfileInteractor;
import com.acidwater.saher.profile.model.User;
import com.acidwater.saher.services.ServiceObserver;
import com.acidwater.saher.utils.ConstantStrings;
import com.acidwater.saher.utils.GeneralUtils;
import com.acidwater.saher.utils.RxUtils;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Doumith on 11/30/2017.
 */
public class ChangePasswordPresenter extends MvpBasePresenter<ChangePasswordMvp.View> implements ChangePasswordMvp.Presenter {

    private Context context;
    CompositeDisposable compositeDisposable;
    ProfileInteractor profileInteractor;

    public ChangePasswordPresenter(Context context) {
        this.context = context;
        compositeDisposable = new CompositeDisposable();
        this.profileInteractor = new ProfileInteractor(context);
    }


    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        if (!retainInstance) {
            RxUtils.disposeIfNotNull(compositeDisposable);
        }
    }


    @Override
    public void validatePassword(String oldPass, String newPass, String confirmPass) {

        if (TextUtils.isEmpty(oldPass) || TextUtils.isEmpty(newPass) || TextUtils.isEmpty(confirmPass)) {
            if (isViewAttached()) {
                getView().showErrorMessage(context.getString(R.string.enter_required_fields));
                return;
            }
        } else if (!newPass.equals(confirmPass)) {
            if (isViewAttached()) {
                getView().showErrorMessage(context.getString(R.string.password_dont_match));
                return;
            }
        } else if (newPass.length() < ConstantStrings.PASSWORD_LENGTH) {
            getView().showErrorMessage(context.getString(R.string.error_password_length));
            return;
        }

        changePassword(oldPass, newPass);
    }

    @Override
    public void changePassword(String oldPassword, String newPassword) {
        compositeDisposable.add(profileInteractor.changePassword(oldPassword, newPassword)
                .subscribeWith(new ServiceObserver<ServerResponse>() {
                    @Override
                    public void onStart() {
                        if (isViewAttached()) {
                            getView().showLoadingView();
                        }
                    }

                    @Override
                    public void onComplete() {

                        if (isViewAttached()) {
                            getView().HideLoadingView();
                        }
                    }

                    @Override
                    public void onNext(ServerResponse serverResponse) {

                        if (isViewAttached()) {
                            getView().setResultSuccessful(serverResponse.getStatusDescription());
                        }
                    }

                    @Override
                    public void onError(SaherException e) {

                        if (isViewAttached()) {

                            getView().showErrorMessage(e.errorMessage);
                            getView().HideLoadingView();
                        }
                    }
                }));
    }
}
