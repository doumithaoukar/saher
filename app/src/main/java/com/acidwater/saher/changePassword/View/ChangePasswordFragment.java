package com.acidwater.saher.changePassword.view;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.acidwater.saher.R;
import com.acidwater.saher.changePassword.ChangePasswordMvp;
import com.acidwater.saher.changePassword.presenter.ChangePasswordPresenter;
import com.acidwater.saher.main.MainActivityListener;
import com.acidwater.saher.profile.ProfileMvp;
import com.acidwater.saher.profile.presenter.ProfilePresenter;
import com.acidwater.saher.utils.GeneralUtils;
import com.hannesdorfmann.mosby3.mvp.MvpFragment;

/**
 * Created by Doumith on 7/6/2017.
 */
public class ChangePasswordFragment extends MvpFragment<ChangePasswordMvp.View, ChangePasswordMvp.Presenter> implements View.OnClickListener, ChangePasswordMvp.View {


    private Button btnSave;
    private EditText oldPassword;
    private EditText newPassword;
    private EditText confirmNewPassword;

    private RelativeLayout progressBar;


    private MainActivityListener mListener;

    public static ChangePasswordFragment newInstance() {
        ChangePasswordFragment changePasswordFragment = new ChangePasswordFragment();
        return changePasswordFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(
                R.layout.fragment_change_password, container, false);

        oldPassword = (EditText) mView.findViewById(R.id.oldPassword);
        newPassword = (EditText) mView.findViewById(R.id.newPassword);
        confirmNewPassword = (EditText) mView.findViewById(R.id.confirmPassword);
        btnSave = (Button) mView.findViewById(R.id.btn_continue);
        progressBar = (RelativeLayout) mView.findViewById(R.id.main_progress);
        btnSave.setOnClickListener(this);
        return mView;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_continue:
                presenter.validatePassword(oldPassword.getText().toString().trim(), newPassword.getText().toString().trim(),
                        confirmNewPassword.getText().toString().trim());
                break;

        }
    }

    @Override
    public ChangePasswordMvp.Presenter createPresenter() {
        return new ChangePasswordPresenter(getContext());
    }

    @Override
    public void showErrorMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void showLoadingView() {
        progressBar.setVisibility(View.VISIBLE);

    }

    @Override
    public void HideLoadingView() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void setResultSuccessful(String message) {

        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
        oldPassword.setText("");
        newPassword.setText("");
        confirmNewPassword.setText("");
        GeneralUtils.closeKeyboard(getContext());
        destroyThis();
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MainActivityListener) {
            mListener = (MainActivityListener) activity;
        } else {
            throw new RuntimeException("you should implemented MainActivityListener");
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void destroyThis() {
        getFragmentManager().beginTransaction().remove(this).commit();
        getFragmentManager().popBackStack();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        mListener = null;

    }


}