package com.acidwater.saher.changePassword;

import com.acidwater.saher.data.DataEntityWrapper;
import com.acidwater.saher.data.ServerResponse;
import com.acidwater.saher.profile.model.User;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;

import io.reactivex.Observable;

/**
 * Created by Doumith on 12/4/2017.
 */
public interface ChangePasswordMvp {


    interface Interactor {

        Observable<ServerResponse> changePassword();
    }

    interface Presenter extends MvpPresenter<View> {

        void validatePassword(String oldPass, String newPass, String confirmPass);

        void changePassword(String oldPassword, String newPassword);
    }

    interface View extends MvpView {

        void showErrorMessage(String message);

        void showLoadingView();

        void HideLoadingView();

        void setResultSuccessful(String message);
    }
}
